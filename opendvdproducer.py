#!/usr/bin/env python3

import os, sys, subprocess, time, random, codecs, tempfile, shutil, copy, vlc
from collections import OrderedDict

from PyQt5.QtWidgets import QApplication, QWidget, QLabel, QSpinBox, QSpinBox, QComboBox, QListWidget, QPushButton, QLineEdit, QListWidget, QCheckBox, QListView, QDial, QProgressBar, QDesktopWidget, QFileDialog, QListWidgetItem, QColorDialog
from PyQt5.QtGui import QIcon, QPainter, QPixmap, QColor, QPen, QPainterPath, QPalette, QFont, QFontDatabase
from PyQt5.QtCore import QSize, Qt, QObject, QThread, QPropertyAnimation, QEasingCurve, QRectF, QPointF, QTimer, pyqtSignal, QRect

from modules import file_io
from modules import generate_iso

path_home = os.path.expanduser("~")
path_opendvdproducer = os.path.dirname(os.path.abspath(__file__))

if sys.platform == 'darwin':
    path_config = os.path.join(path_home, 'Library', 'Application Support', 'Open DVD Producer')
    imagemagick_convert_bin = os.path.join(path_opendvdproducer, 'resources', 'convert')
    dvdauthor_bin = os.path.join(path_opendvdproducer, 'resources', 'dvdauthor')
    ffmpeg_bin = os.path.join(path_opendvdproducer, 'resources',  'ffmpeg')
    ffprobe_bin = os.path.join(path_opendvdproducer, 'resources', 'ffprobe')
    spumux_bin = os.path.join(path_opendvdproducer, 'resources', 'spumux')
    mkisofs_bin = os.path.join(path_opendvdproducer, 'resources', 'mkisofs')
    md5_bin = '/sbin/md5'
    split_bin = '/usr/bin/split'
    interface_font_size = 12

elif sys.platform == 'win32' or os.name == 'nt':
    if getattr(sys, 'frozen', False): # If the app is built to be a "portable" version (without installation)
        path_opendvdproducer = getattr(sys, '_MEIPASS', os.getcwd())
    else:
        path_opendvdproducer = os.path.dirname(os.path.abspath(__file__))
    path_config = os.path.join(os.getenv('LOCALAPPDATA'), 'Open DVD Producer')
    imagemagick_convert_bin = os.path.join(path_opendvdproducer, 'resources', 'convert.exe')
    dvdauthor_bin = os.path.join(path_opendvdproducer, 'resources', 'dvdauthor.exe')
    ffmpeg_bin = os.path.join(path_opendvdproducer, 'resources', 'ffmpeg.exe')
    ffprobe_bin = os.path.join(path_opendvdproducer, 'resources', 'ffprobe.exe')
    spumux_bin = os.path.join(path_opendvdproducer, 'resources', 'spumux.exe')
    mkisofs_bin = os.path.join(path_opendvdproducer, 'resources', 'mkisofs.exe')
    md5_bin = os.path.join(path_opendvdproducer, 'resources', 'md5.exe')
    split_bin = os.path.join(path_opendvdproducer, 'resources', 'split.exe')
    interface_font_size = 9

else:
    if subprocess.call("type ffprobe", shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE) == 0:
        ffprobe_bin = 'ffprobe'
    else:
        ffprobe_bin = 'avprobe'
    if subprocess.call("type ffmpeg", shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE) == 0:
        ffmpeg_bin = 'ffmpeg'
    else:
        ffmpeg_bin = 'avconv'
    spumux_bin = 'spumux'
    dvdauthor_bin = 'dvdauthor'
    mkisofs_bin = 'mkisofs'
    md5_bin = 'md5sum'
    split_bin = 'split'
    iso2ddp_bin = os.path.join(path_opendvdproducer, 'resources', 'iso2ddp')
    imagemagick_convert_bin = 'convert'
    interface_font_size = 10

path_graphics = os.path.join(path_opendvdproducer, 'graphics')
path_tmp = os.path.join(tempfile.gettempdir(), 'opendvdproducer-' + str(random.randint(1000,9999)))
os.mkdir(path_tmp)

class generate_dvd_thread_signal(QObject):
    sig = pyqtSignal(str)

class generate_dvd_thread(QThread):
    def __init__(self, parent = None):
        QThread.__init__(self, parent)
        self.signal = generate_dvd_thread_signal()
        self.actual_project = False
    def run(self):
        if self.actual_project:
            generate_iso.run(
                                self.actual_project,
                                self.signal,
                                split_bin=split_bin,
                                ffprobe_bin=ffprobe_bin,
                                ffmpeg_bin=ffmpeg_bin,
                                imagemagick_convert_bin=imagemagick_convert_bin,
                                spumux_bin=spumux_bin,
                                dvdauthor_bin=dvdauthor_bin,
                                mkisofs_bin=mkisofs_bin,
                                md5_bin=md5_bin
                            )

class main_window(QWidget):
    def __init__(self, parent=None):
        QWidget.__init__(self, parent)
        self.setWindowTitle('Open DVD Producer')
        self.setWindowIcon(QIcon(os.path.join(path_graphics, 'opendvdproducer.png')))

        self.video_formats = ['PAL 720x576', 'NTSC 720x480']
        self.aspect_ratios = ['16:9', '4:3']
        self.audio_formats = ['MP2 48kHz', 'AC3 48kHz']
        self.resolutions = []

        self.is_generating = False
        self.is_showing_options_panel = False

        self.main_panel = QWidget(parent=self)

        self.content_panel = QWidget(parent=self.main_panel)
        self.content_panel_animation = QPropertyAnimation(self.content_panel, b'geometry')
        self.content_panel_animation.setEasingCurve(QEasingCurve.OutCirc)

        self.content_panel_background = QLabel(parent=self.content_panel)
        self.content_panel_background.setPixmap(QPixmap(os.path.join(path_graphics, 'content_panel_background.png')))
        self.content_panel_background.setScaledContents(True)

        class preview(QWidget):
            def paintEvent(widget, paintEvent):
                painter = QPainter(widget)
                painter.setRenderHint(QPainter.Antialiasing)

                if self.selected_menu or not self.selected_menu_button_directioning == None:
                    pixmap = QPixmap(os.path.join(path_tmp, self.selected_menu + '.preview.png'))
                    painter.drawPixmap(0,0,pixmap)

                    if len(self.actual_project['menus'][self.selected_menu][1]) > 0:
                        if self.actual_project['aspect_ratio'] == 0:
                            factor_x = 1
                            if self.actual_project['video_format'] == 0:
                                factor_y = 0.703125
                            elif self.actual_project['video_format'] == 1:
                                factor_y = 0.84375
                        elif self.actual_project['aspect_ratio'] == 1:
                            factor_x = 0.88888888
                            if self.actual_project['video_format'] == 0:
                                factor_y = 0.833333333333
                            elif self.actual_project['video_format'] == 1:
                                factor_y = 1

                        for button in self.actual_project['menus'][self.selected_menu][1]:
                            button_positions = [self.actual_project['menus'][self.selected_menu][2][button][0]*factor_x, self.actual_project['menus'][self.selected_menu][2][button][1]*factor_y, self.actual_project['menus'][self.selected_menu][2][button][2]*factor_x, self.actual_project['menus'][self.selected_menu][2][button][3]*factor_y]
                            painter.setPen(QColor.fromRgb(0,0,0))
                            painter.setBrush(QColor.fromRgb(0,0,0,alpha=0))
                            rectangle_main = QRectF(button_positions[0], button_positions[1], button_positions[2], button_positions[3])
                            painter.drawRect(rectangle_main)

                            if button == self.selected_menu_button:
                                painter.setBrush(QColor.fromRgb(80,80,80,alpha=200))
                                if self.overlay_preview and self.actual_project['menus'][self.selected_menu][3]:
                                    overlay = QPixmap(get_preview_file(self, os.path.join(path_tmp, self.actual_project['menus'][self.selected_menu][3].split('/')[-1].split('\\')[-1][:-4] + '_hl.preview.png')))
                                    painter.setClipRect(rectangle_main)
                                    painter.drawPixmap(0,0,overlay)
                                    painter.setClipping(False)

                                painter.setPen(QColor.fromRgb(0,0,0))
                                painter.setBrush(QColor.fromRgb(0,0,0,alpha=0))

                                if self.directions_preview:
                                    direction_index = 0
                                    for direction in self.actual_project['menus'][self.selected_menu][2][self.selected_menu_button][5]:
                                        if direction_index == 0:
                                            orig_x, orig_y =  (button_positions[0] + (button_positions[2]/2)), button_positions[1]
                                            if direction:
                                                dest_positions = [self.actual_project['menus'][self.selected_menu][2][direction][0]*factor_x, self.actual_project['menus'][self.selected_menu][2][direction][1]*factor_y, self.actual_project['menus'][self.selected_menu][2][direction][2]*factor_x, self.actual_project['menus'][self.selected_menu][2][direction][3]*factor_y]
                                                dest_x, dest_y =  (dest_positions[0] + (dest_positions[2]/2)), dest_positions[1] + dest_positions[3]
                                            elif not self.selected_menu_button_directioning == None:
                                                dest_x, dest_y =  self.selected_menu_button_directions[direction_index][0], self.selected_menu_button_directions[direction_index][1]
                                            else:
                                                dest_x, dest_y =  orig_x, orig_y - 20

                                            pen = QPen(QColor.fromRgb(0,0,0), 4)
                                            painter.setPen(pen)
                                            arrow_path = QPainterPath(QPointF(orig_x, orig_y))
                                            arrow_path.cubicTo(QPointF(orig_x, orig_y-50), QPointF(dest_x, dest_y+50), QPointF(dest_x, dest_y + 8))
                                            painter.drawPath(arrow_path)
                                            pen = QPen(Qt.NoPen)
                                            painter.setPen(pen)
                                            arrow_path = QPainterPath(QPointF(dest_x + 5, dest_y + 8))
                                            arrow_path.lineTo(dest_x, dest_y)
                                            arrow_path.lineTo(dest_x - 5, dest_y + 8)
                                            arrow_path.lineTo(dest_x + 5, dest_y + 8)
                                            painter.fillPath(arrow_path, QColor.fromRgb(0,0,0))

                                        elif direction_index == 1:
                                            orig_x, orig_y =  (button_positions[0] + button_positions[2]), (button_positions[1] + (button_positions[3]/2))
                                            if direction:
                                                dest_positions = [self.actual_project['menus'][self.selected_menu][2][direction][0]*factor_x, self.actual_project['menus'][self.selected_menu][2][direction][1]*factor_y, self.actual_project['menus'][self.selected_menu][2][direction][2]*factor_x, self.actual_project['menus'][self.selected_menu][2][direction][3]*factor_y]
                                                dest_x, dest_y =  dest_positions[0], (dest_positions[1] + (dest_positions[3]/2))
                                            elif not self.selected_menu_button_directioning == None:
                                                dest_x, dest_y =  self.selected_menu_button_directions[direction_index][0], self.selected_menu_button_directions[direction_index][1]
                                            else:
                                                dest_x, dest_y =  orig_x + 20, orig_y
                                            pen = QPen(QColor.fromRgb(0,0,0), 4)
                                            painter.setPen(pen)
                                            arrow_path = QPainterPath(QPointF(orig_x, orig_y))
                                            arrow_path.cubicTo(QPointF(orig_x+50, orig_y), QPointF(dest_x-50, dest_y), QPointF(dest_x - 8, dest_y))
                                            painter.drawPath(arrow_path)
                                            pen = QPen(Qt.NoPen)
                                            painter.setPen(pen)
                                            arrow_path = QPainterPath(QPointF(dest_x - 8, dest_y + 5))
                                            arrow_path.lineTo(dest_x, dest_y)
                                            arrow_path.lineTo(dest_x - 8, dest_y - 5)
                                            arrow_path.lineTo(dest_x - 8, dest_y + 5)
                                            painter.fillPath(arrow_path, QColor.fromRgb(0,0,0))

                                        elif direction_index == 2:
                                            orig_x, orig_y =  button_positions[0] + (button_positions[2]/2), button_positions[1] + button_positions[3]
                                            if direction:
                                                dest_positions = [self.actual_project['menus'][self.selected_menu][2][direction][0]*factor_x, self.actual_project['menus'][self.selected_menu][2][direction][1]*factor_y, self.actual_project['menus'][self.selected_menu][2][direction][2]*factor_x, self.actual_project['menus'][self.selected_menu][2][direction][3]*factor_y]
                                                dest_x, dest_y =  (dest_positions[0] + (dest_positions[2]/2)), dest_positions[1]
                                            elif not self.selected_menu_button_directioning == None:
                                                dest_x, dest_y =  self.selected_menu_button_directions[direction_index][0], self.selected_menu_button_directions[direction_index][1]
                                            else:
                                                dest_x, dest_y =  orig_x, orig_y + 20
                                            pen = QPen(QColor.fromRgb(0,0,0), 4)
                                            painter.setPen(pen)
                                            arrow_path = QPainterPath(QPointF(orig_x, orig_y))
                                            arrow_path.cubicTo(QPointF(orig_x, orig_y+50), QPointF(dest_x, dest_y-50), QPointF(dest_x, dest_y - 8))
                                            painter.drawPath(arrow_path)
                                            pen = QPen(Qt.NoPen)
                                            painter.setPen(pen)
                                            arrow_path = QPainterPath(QPointF(dest_x + 5, dest_y - 8))
                                            arrow_path.lineTo(dest_x, dest_y)
                                            arrow_path.lineTo(dest_x - 5, dest_y - 8)
                                            arrow_path.lineTo(dest_x + 5, dest_y - 8)
                                            painter.fillPath(arrow_path, QColor.fromRgb(0,0,0))

                                        elif direction_index == 3:
                                            orig_x, orig_y =  button_positions[0], (button_positions[1] + (button_positions[3]/2))
                                            if direction:
                                                dest_positions = [self.actual_project['menus'][self.selected_menu][2][direction][0]*factor_x, self.actual_project['menus'][self.selected_menu][2][direction][1]*factor_y, self.actual_project['menus'][self.selected_menu][2][direction][2]*factor_x, self.actual_project['menus'][self.selected_menu][2][direction][3]*factor_y]
                                                dest_x, dest_y =  (dest_positions[0] + dest_positions[2]), (dest_positions[1] + (dest_positions[3]/2))
                                            elif not self.selected_menu_button_directioning == None:
                                                dest_x, dest_y =  self.selected_menu_button_directions[direction_index][0], self.selected_menu_button_directions[direction_index][1]
                                            else:
                                                dest_x, dest_y =  orig_x - 20, orig_y
                                            pen = QPen(QColor.fromRgb(0,0,0), 4)
                                            painter.setPen(pen)
                                            arrow_path = QPainterPath(QPointF(orig_x, orig_y))
                                            arrow_path.cubicTo(QPointF(orig_x-50, orig_y), QPointF(dest_x+50, dest_y), QPointF(dest_x + 8, dest_y))
                                            painter.drawPath(arrow_path)
                                            pen = QPen(Qt.NoPen)
                                            painter.setPen(pen)
                                            arrow_path = QPainterPath(QPointF(dest_x + 8, dest_y - 5))
                                            arrow_path.lineTo(dest_x, dest_y)
                                            arrow_path.lineTo(dest_x + 8, dest_y + 5)
                                            arrow_path.lineTo(dest_x + 8, dest_y - 5)
                                            painter.fillPath(arrow_path, QColor.fromRgb(0,0,0))

                                        painter.drawPath(arrow_path)
                                        direction_index += 1

                                painter.setPen(QColor.fromRgb(0,0,0))
                                painter.setBrush(QColor.fromRgb(80,80,80,alpha=200))

                            else:
                                painter.setBrush(QColor.fromRgb(0,0,0))

                            rectangle = QRectF(button_positions[0]-5, button_positions[1]-5, painter.fontMetrics().width(button) + 5,10)
                            painter.drawRoundedRect(rectangle, 5,5)
                            painter.setPen(QColor.fromRgb(255,255,255))
                            painter.setFont(QFont('Ubuntu', interface_font_size*.8))
                            rectangle = QRectF(button_positions[0]-3, button_positions[1]-6, painter.fontMetrics().width(button) + 5,10)
                            painter.drawText(rectangle, Qt.AlignLeft, button)
                            rectangle = QRectF(button_positions[2]+button_positions[0]-5, button_positions[3]+button_positions[1]-5, 10,10)
                            painter.setBrush(QColor.fromRgb(0,0,0))
                            painter.drawEllipse(rectangle)

                else:
                    pixmap = QPixmap(os.path.join(path_graphics, 'preview_' + str(self.aspect_ratios[self.actual_project['aspect_ratio']]).replace(':', '_') + '_space.png'))
                    painter.drawPixmap(0,0,pixmap)

                painter.end()

            def mousePressEvent(widget, event):
                if self.actual_project['aspect_ratio'] == 0:
                    factor_x = 1
                    if self.actual_project['video_format'] == 0:
                        factor_y = 0.703125
                    elif self.actual_project['video_format'] == 1:
                        factor_y = 0.84375
                elif self.actual_project['aspect_ratio'] == 1:
                    factor_x = 0.88888888
                    if self.actual_project['video_format'] == 0:
                        factor_y = 0.833333333333
                    elif self.actual_project['video_format'] == 1:
                        factor_y = 1

                if self.selected_menu_button and self.directions_preview:
                    button_positions = [self.actual_project['menus'][self.selected_menu][2][self.selected_menu_button][0]*factor_x, self.actual_project['menus'][self.selected_menu][2][self.selected_menu_button][1]*factor_y, self.actual_project['menus'][self.selected_menu][2][self.selected_menu_button][2]*factor_x, self.actual_project['menus'][self.selected_menu][2][self.selected_menu_button][3]*factor_y]
                    direction_index = 0
                    for direction in self.actual_project['menus'][self.selected_menu][2][self.selected_menu_button][5]:
                        if direction_index == 0:
                            if direction:
                                dest_positions = [self.actual_project['menus'][self.selected_menu][2][direction][0]*factor_x, self.actual_project['menus'][self.selected_menu][2][direction][1]*factor_y, self.actual_project['menus'][self.selected_menu][2][direction][2]*factor_x, self.actual_project['menus'][self.selected_menu][2][direction][3]*factor_y]
                                self.selected_menu_button_directions[direction_index] = [dest_positions[0] + (dest_positions[2]/2), dest_positions[1] + dest_positions[3]]
                            else:
                                orig_x, orig_y =  button_positions[0] + (button_positions[2]/2), button_positions[1]
                                self.selected_menu_button_directions[direction_index] = [orig_x, orig_y - 20]

                        elif direction_index == 1:
                            if direction:
                                dest_positions = [self.actual_project['menus'][self.selected_menu][2][direction][0]*factor_x, self.actual_project['menus'][self.selected_menu][2][direction][1]*factor_y, self.actual_project['menus'][self.selected_menu][2][direction][2]*factor_x, self.actual_project['menus'][self.selected_menu][2][direction][3]*factor_y]
                                self.selected_menu_button_directions[direction_index] = [dest_positions[0], (dest_positions[1] + (dest_positions[3]/2))]
                            else:
                                orig_x, orig_y =  button_positions[0] + button_positions[2], button_positions[1] + (button_positions[3]/2)
                                self.selected_menu_button_directions[direction_index] = [orig_x + 20, orig_y]

                        elif direction_index == 2:
                            if direction:
                                dest_positions = [self.actual_project['menus'][self.selected_menu][2][direction][0]*factor_x, self.actual_project['menus'][self.selected_menu][2][direction][1]*factor_y, self.actual_project['menus'][self.selected_menu][2][direction][2]*factor_x, self.actual_project['menus'][self.selected_menu][2][direction][3]*factor_y]
                                self.selected_menu_button_directions[direction_index] = [dest_positions[0] + (dest_positions[2]/2), dest_positions[1]]
                            else:
                                orig_x, orig_y =  button_positions[0] + (button_positions[2]/2), button_positions[1] + button_positions[3]
                                self.selected_menu_button_directions[direction_index] = [orig_x, orig_y + 20]

                        elif direction_index == 3:
                            if direction:
                                dest_positions = [self.actual_project['menus'][self.selected_menu][2][direction][0]*factor_x, self.actual_project['menus'][self.selected_menu][2][direction][1]*factor_y, self.actual_project['menus'][self.selected_menu][2][direction][2]*factor_x, self.actual_project['menus'][self.selected_menu][2][direction][3]*factor_y]
                                self.selected_menu_button_directions[direction_index] = [dest_positions[0] + dest_positions[2], (dest_positions[1] + (dest_positions[3]/2))]
                            else:
                                orig_x, orig_y =  button_positions[0], button_positions[1] + (button_positions[3]/2)
                                self.selected_menu_button_directions[direction_index] = [orig_x - 20, orig_y]

                        direction_index += 1

                    if   (((event.pos().x() > self.selected_menu_button_directions[0][0] - 6)  and (event.pos().x() < self.selected_menu_button_directions[0][0] + 6))  and ((event.pos().y() > self.selected_menu_button_directions[0][1] - 1)  and (event.pos().y() < self.selected_menu_button_directions[0][1]  + 11))):
                        self.selected_menu_button_directioning = 0
                    elif (((event.pos().x() > self.selected_menu_button_directions[1][0] - 11) and (event.pos().x() < self.selected_menu_button_directions[1][0] + 1))  and ((event.pos().y() > self.selected_menu_button_directions[1][1] - 6)  and (event.pos().y() < self.selected_menu_button_directions[1][1]  + 6))):
                        self.selected_menu_button_directioning = 1
                    elif (((event.pos().x() > self.selected_menu_button_directions[2][0] - 6)  and (event.pos().x() < self.selected_menu_button_directions[2][0] + 6))  and ((event.pos().y() > self.selected_menu_button_directions[2][1] - 11) and (event.pos().y() < self.selected_menu_button_directions[2][1]  + 1))):
                        self.selected_menu_button_directioning = 2
                    elif (((event.pos().x() > self.selected_menu_button_directions[3][0] - 1)  and (event.pos().x() < self.selected_menu_button_directions[3][0] + 11)) and ((event.pos().y() > self.selected_menu_button_directions[3][1] - 6)  and (event.pos().y() < self.selected_menu_button_directions[3][1]  + 6))):
                        self.selected_menu_button_directioning = 3
                    else:
                        self.selected_menu_button_directioning = None

                if self.selected_menu_button_directioning == None:
                    self.selected_menu_button = None

                self.selected_menu_button_resizing = False

                if self.selected_menu:
                    for button in self.actual_project['menus'][self.selected_menu][1]:
                        button_positions = [self.actual_project['menus'][self.selected_menu][2][button][0]*factor_x, self.actual_project['menus'][self.selected_menu][2][button][1]*factor_y, self.actual_project['menus'][self.selected_menu][2][button][2]*factor_x, self.actual_project['menus'][self.selected_menu][2][button][3]*factor_y]
                        if self.selected_menu_button_directioning == None:
                            if ((event.pos().x() > button_positions[0]-5) and (event.pos().x() < button_positions[0] + button_positions[2] + 5)) and ((event.pos().y() > button_positions[1]-5) and (event.pos().y() < button_positions[1] + button_positions[3] + 5)):
                                self.selected_menu_button = button
                                self.selected_menu_button_preview_difference = [event.pos().x() - button_positions[0],event.pos().y() - button_positions[1]]
                                self.options_panel_menu_buttons.setCurrentRow(self.actual_project['menus'][self.selected_menu][1].index(self.selected_menu_button))
                                menu_button_selected(self)
                                if ((event.pos().x() > button_positions[0] + button_positions[2] - 5) and (event.pos().x() < button_positions[0] + button_positions[2] + 5)) and ((event.pos().y() > button_positions[1] + button_positions[3] - 5) and (event.pos().y() < button_positions[1] + button_positions[3] + 5)):
                                    self.selected_menu_button_resizing = True

                if not self.selected_menu_button:
                    clean_buttons_selection(self)

            def mouseReleaseEvent(widget, event):
                self.selected_menu_button_preview_difference = [0,0]
                update_changes(self)

            def mouseMoveEvent(widget, event):
                if self.selected_menu_button:
                    button_positions = self.actual_project['menus'][self.selected_menu][2][self.selected_menu_button]

                    if self.actual_project['aspect_ratio'] == 0:
                        factor_x = 1
                        if self.actual_project['video_format'] == 0:
                            factor_y = 1.42222222
                        elif self.actual_project['video_format'] == 1:
                            factor_y = 1.18518519
                    elif self.actual_project['aspect_ratio'] == 1:
                        factor_x = 1.125
                        if self.actual_project['video_format'] == 0:
                            factor_y = 1.2
                        elif self.actual_project['video_format'] == 1:
                            factor_y = 1

                    if not self.selected_menu_button_directioning == None and self.directions_preview:
                        final_x = int( event.pos().x() * factor_x )
                        final_y = int( event.pos().y() * factor_y )
                        for button in self.actual_project['menus'][self.selected_menu][1]:
                            if not self.selected_menu_button == button:
                                dest_positions = [self.actual_project['menus'][self.selected_menu][2][button][0], self.actual_project['menus'][self.selected_menu][2][button][1], self.actual_project['menus'][self.selected_menu][2][button][2], self.actual_project['menus'][self.selected_menu][2][button][3]]
                                if ((final_x > dest_positions[0]-5) and (final_x < dest_positions[0] + dest_positions[2] + 5)) and ((final_y > dest_positions[1]-5) and (final_y < dest_positions[1] + dest_positions[3] + 5)):
                                    self.actual_project['menus'][self.selected_menu][2][self.selected_menu_button][5][self.selected_menu_button_directioning] = button
                                    break
                                else:
                                    self.actual_project['menus'][self.selected_menu][2][self.selected_menu_button][5][self.selected_menu_button_directioning] = None
                        self.selected_menu_button_directions[self.selected_menu_button_directioning][0] = int(event.pos().x())
                        self.selected_menu_button_directions[self.selected_menu_button_directioning][1] = int(event.pos().y())
                    else:
                        if self.selected_menu_button_resizing:
                            button_positions[2] = int( event.pos().x() * factor_x) - button_positions[0]
                            button_positions[3] = int( event.pos().y() * factor_y) - button_positions[1]
                        else:
                            button_positions[0] = int( int(event.pos().x() - self.selected_menu_button_preview_difference[0]) ) * factor_x
                            button_positions[1] = int( int(event.pos().y() - self.selected_menu_button_preview_difference[1]) ) * factor_y

                        self.actual_project['menus'][self.selected_menu][2][self.selected_menu_button] = button_positions
                    update_changes(self)

        self.preview = preview(parent=self.content_panel)

        self.no_preview_label = QLabel(parent=self.preview)
        self.no_preview_label.setAlignment(Qt.AlignCenter)
        self.no_preview_label.setForegroundRole(QPalette.Midlight)

        self.preview_video_widget = QLabel(parent=self.main_panel)

        self.video_instance = vlc.Instance('--verbose 3 --no-plugins-cache')
        print(self.video_instance)
        self.preview_video_obj = self.video_instance.media_player_new()

        if sys.platform == "linux2": # for Linux using the X Server
            self.preview_video_obj.set_xwindow(self.preview_video_widget.winId())

        elif sys.platform == "win32": # for Windows
            pycobject_hwnd = self.preview_video_widget.winId()
            import ctypes
            ctypes.pythonapi.PyCObject_AsVoidPtr.restype = ctypes.c_void_p
            ctypes.pythonapi.PyCObject_AsVoidPtr.argtypes = [ctypes.py_object]
            int_hwnd = ctypes.pythonapi.PyCObject_AsVoidPtr(pycobject_hwnd)

            self.preview_video_obj.set_hwnd(int_hwnd)

        elif sys.platform == "darwin": # for MacOS
            self.preview_video_obj.set_nsobject(self.preview_video_widget.winId())

        self.options_panel = QWidget(parent=self.content_panel)
        self.options_panel_animation = QPropertyAnimation(self.options_panel, b'geometry')
        self.options_panel_animation.setEasingCurve(QEasingCurve.OutCirc)

        self.options_panel_background = QLabel(parent=self.options_panel)
        self.options_panel_background.setStyleSheet("QLabel { background-image: url(" + os.path.join(path_graphics, "options_panel_background.png").replace('\\', '/') + "); background-position: center right; background-repeat: repeat-y; }")

        self.options_panel_dvd_panel = QWidget(parent=self.options_panel)

        class options_panel_dvd_panel_dvd_image(QWidget):
            def paintEvent(widget, paintEvent):
                estimated_size = 0.0

                if self.is_showing_options_panel and len(self.actual_project['videos']) > 0:
                    if self.actual_project['has_menus'] and len(self.actual_project['menus']) > 0:
                        for menu in self.actual_project['menus']:
                            estimated_size += float(((self.actual_project['menu_bitrate'] + int(self.actual_project['audio_datarate'].split(' ')[0]))*.00001) * self.actual_project['menus'][menu][10])
                    for video in self.actual_project['videos']:
                        final_length = self.actual_project['videos'][video][5]
                        if self.actual_project['videos'][video][6]:
                            final_length -= self.actual_project['videos'][video][6]
                        if self.actual_project['videos'][video][7]:
                            final_length -= (self.actual_project['videos'][video][5] - self.actual_project['videos'][video][7])
                        estimated_size += float(((self.actual_project['video_bitrate'] + int(self.actual_project['audio_datarate'].split(' ')[0]))*.00001) * final_length)

                painter = QPainter(widget)
                painter.setRenderHint(QPainter.Antialiasing)

                pen = QPen(Qt.NoPen)
                painter.setPen(pen)

                painter.setBrush(QColor.fromRgb(0,0,0,alpha=50))
                rectangle = QRectF(28,28,244,244)
                rectangle_inner = QRectF(106,106,88,88)

                def get_inner_point(angle):
                    circle_inner_path = QPainterPath()
                    circle_inner_path.arcTo(rectangle_inner,90, angle)
                    return circle_inner_path.currentPosition()

                if estimated_size > 360.0:
                    circle_path = QPainterPath()
                    circle_path.arcMoveTo(rectangle, 90)
                    circle_path.arcTo(rectangle, 90, 359.9)
                    circle_path.lineTo(get_inner_point(359.9))
                    circle_path.arcTo(rectangle_inner, 90 + 359.9, 359.9 * (-1))
                    circle_path.closeSubpath()
                    painter.drawPath(circle_path)
                    estimated_size -= 360

                circle_path = QPainterPath()
                circle_path.arcMoveTo(rectangle, 90)
                circle_path.arcTo(rectangle, 90, estimated_size)
                circle_path.lineTo(get_inner_point(estimated_size))
                circle_path.arcTo(rectangle_inner, 90 + estimated_size, estimated_size * (-1))
                circle_path.closeSubpath()
                painter.drawPath(circle_path)

                foreground = QPixmap(os.path.join(path_graphics, 'options_panel_dvd_image_background.png'))
                painter.drawPixmap(0,0,foreground)

        self.options_panel_dvd_panel_dvd_image = options_panel_dvd_panel_dvd_image(parent=self.options_panel_dvd_panel)
        self.options_panel_dvd_panel_dvd_image.setGeometry(35,0,300,300)

        self.options_panel_dvd_panel_size_info = QLabel(parent=self.options_panel_dvd_panel)
        self.options_panel_dvd_panel_size_info.setGeometry(35,300,300,40)
        self.options_panel_dvd_panel_size_info.setAlignment(Qt.AlignCenter)

        self.options_panel_dvd_panel_menu_encoding_label = QLabel(parent=self.options_panel_dvd_panel)
        self.options_panel_dvd_panel_menu_encoding_label.setGeometry(10, 360, 170, 20)
        self.options_panel_dvd_panel_menu_encoding_label.setText('MENU ENCODING')

        class options_panel_dvd_panel_menu_encoding(QWidget):
            def mousePressEvent(widget, event):
                options_panel_dvd_panel_menu_encoding_changed(self)

        self.options_panel_dvd_panel_menu_encoding = options_panel_dvd_panel_menu_encoding(parent=self.options_panel_dvd_panel)
        self.options_panel_dvd_panel_menu_encoding.setGeometry(10, 380, 78, 25)

        self.options_panel_dvd_panel_menu_encoding_background = QLabel(parent=self.options_panel_dvd_panel_menu_encoding)
        self.options_panel_dvd_panel_menu_encoding_background.setGeometry(0,0,self.options_panel_dvd_panel_menu_encoding.width(),self.options_panel_dvd_panel_menu_encoding.height())

        self.options_panel_dvd_panel_menu_bitrate_label = QLabel(parent=self.options_panel_dvd_panel)
        self.options_panel_dvd_panel_menu_bitrate_label.setGeometry(10, 415, 170, 20)
        self.options_panel_dvd_panel_menu_bitrate_label.setText('MENU BITRATE')

        self.options_panel_dvd_panel_menu_bitrate_field = QSpinBox(parent=self.options_panel_dvd_panel)
        self.options_panel_dvd_panel_menu_bitrate_field.setGeometry(10, 435, 100, 30)
        self.options_panel_dvd_panel_menu_bitrate_field.setMinimum(200)
        self.options_panel_dvd_panel_menu_bitrate_field.setMaximum(9600)
        self.options_panel_dvd_panel_menu_bitrate_field.editingFinished.connect(lambda:options_panel_dvd_panel_bitrates_changed(self))

        self.options_panel_dvd_panel_menu_bitrate_field_label = QLabel(parent=self.options_panel_dvd_panel)
        self.options_panel_dvd_panel_menu_bitrate_field_label.setGeometry(115, 435, 65, 30)
        self.options_panel_dvd_panel_menu_bitrate_field_label.setText('Kbp/s')

        self.options_panel_dvd_panel_menu_max_bitrate_label = QLabel(parent=self.options_panel_dvd_panel)
        self.options_panel_dvd_panel_menu_max_bitrate_label.setGeometry(10, 475, 170, 20)
        self.options_panel_dvd_panel_menu_max_bitrate_label.setText('MENU MAXIMUM BITRATE')

        self.options_panel_dvd_panel_menu_max_bitrate_field = QSpinBox(parent=self.options_panel_dvd_panel)
        self.options_panel_dvd_panel_menu_max_bitrate_field.setGeometry(10, 495, 100, 30)
        self.options_panel_dvd_panel_menu_max_bitrate_field.setMinimum(200)
        self.options_panel_dvd_panel_menu_max_bitrate_field.setMaximum(9600)
        self.options_panel_dvd_panel_menu_max_bitrate_field.editingFinished.connect(lambda:options_panel_dvd_panel_bitrates_changed(self))

        self.options_panel_dvd_panel_menu_max_bitrate_field_label = QLabel(parent=self.options_panel_dvd_panel)
        self.options_panel_dvd_panel_menu_max_bitrate_field_label.setGeometry(115, 495, 65, 30)
        self.options_panel_dvd_panel_menu_max_bitrate_field_label.setText('Kbp/s')

        self.options_panel_dvd_panel_menu_twopass_label = QLabel(parent=self.options_panel_dvd_panel)
        self.options_panel_dvd_panel_menu_twopass_label.setGeometry(10, 535, 170, 20)
        self.options_panel_dvd_panel_menu_twopass_label.setText('PASSES')

        class options_panel_dvd_panel_menu_twopass(QWidget):
            def mousePressEvent(widget, event):
                options_panel_dvd_panel_menu_twopass_changed(self)

        self.options_panel_dvd_panel_menu_twopass = options_panel_dvd_panel_menu_twopass(parent=self.options_panel_dvd_panel)
        self.options_panel_dvd_panel_menu_twopass.setGeometry(10, 555, 78,25)

        self.options_panel_dvd_panel_menu_twopass_background = QLabel(parent=self.options_panel_dvd_panel_menu_twopass)
        self.options_panel_dvd_panel_menu_twopass_background.setGeometry(0,0,self.options_panel_dvd_panel_menu_twopass.width(),self.options_panel_dvd_panel_menu_twopass.height())

        self.options_panel_dvd_panel_video_encoding_label = QLabel(parent=self.options_panel_dvd_panel)
        self.options_panel_dvd_panel_video_encoding_label.setGeometry(190, 360, 170, 20)
        self.options_panel_dvd_panel_video_encoding_label.setText('VIDEO ENCODING')

        class options_panel_dvd_panel_video_encoding(QWidget):
            def mousePressEvent(widget, event):
                options_panel_dvd_panel_video_encoding_changed(self)

        self.options_panel_dvd_panel_video_encoding = options_panel_dvd_panel_video_encoding(parent=self.options_panel_dvd_panel)
        self.options_panel_dvd_panel_video_encoding.setGeometry(190, 380, 78, 25)

        self.options_panel_dvd_panel_video_encoding_background = QLabel(parent=self.options_panel_dvd_panel_video_encoding)
        self.options_panel_dvd_panel_video_encoding_background.setGeometry(0,0,self.options_panel_dvd_panel_video_encoding.width(),self.options_panel_dvd_panel_video_encoding.height())

        self.options_panel_dvd_panel_video_bitrate_label = QLabel(parent=self.options_panel_dvd_panel)
        self.options_panel_dvd_panel_video_bitrate_label.setGeometry(190, 415, 170, 20)
        self.options_panel_dvd_panel_video_bitrate_label.setText('VIDEO BITRATE')

        self.options_panel_dvd_panel_video_bitrate_field = QSpinBox(parent=self.options_panel_dvd_panel)
        self.options_panel_dvd_panel_video_bitrate_field.setGeometry(190, 435, 100, 30)
        self.options_panel_dvd_panel_video_bitrate_field.setMinimum(200)
        self.options_panel_dvd_panel_video_bitrate_field.setMaximum(9600)
        self.options_panel_dvd_panel_video_bitrate_field.editingFinished.connect(lambda:options_panel_dvd_panel_bitrates_changed(self))

        self.options_panel_dvd_panel_video_bitrate_field_label = QLabel(parent=self.options_panel_dvd_panel)
        self.options_panel_dvd_panel_video_bitrate_field_label.setGeometry(295, 435, 65, 30)
        self.options_panel_dvd_panel_video_bitrate_field_label.setText('Kbp/s')

        self.options_panel_dvd_panel_video_max_bitrate_label = QLabel(parent=self.options_panel_dvd_panel)
        self.options_panel_dvd_panel_video_max_bitrate_label.setGeometry(190, 475, 170, 20)
        self.options_panel_dvd_panel_video_max_bitrate_label.setText('VIDEO MAXIMUM BITRATE')

        self.options_panel_dvd_panel_video_max_bitrate_field = QSpinBox(parent=self.options_panel_dvd_panel)
        self.options_panel_dvd_panel_video_max_bitrate_field.setGeometry(190, 495, 100, 30)
        self.options_panel_dvd_panel_video_max_bitrate_field.setMinimum(200)
        self.options_panel_dvd_panel_video_max_bitrate_field.setMaximum(9600)
        self.options_panel_dvd_panel_video_max_bitrate_field.editingFinished.connect(lambda:options_panel_dvd_panel_bitrates_changed(self))

        self.options_panel_dvd_panel_video_max_bitrate_field_label = QLabel(parent=self.options_panel_dvd_panel)
        self.options_panel_dvd_panel_video_max_bitrate_field_label.setGeometry(295, 495, 65, 30)
        self.options_panel_dvd_panel_video_max_bitrate_field_label.setText('Kbp/s')

        self.options_panel_dvd_panel_video_twopass_label = QLabel(parent=self.options_panel_dvd_panel)
        self.options_panel_dvd_panel_video_twopass_label.setGeometry(190, 535, 170, 20)
        self.options_panel_dvd_panel_video_twopass_label.setText('PASSES')

        class options_panel_dvd_panel_video_twopass(QWidget):
            def mousePressEvent(widget, event):
                options_panel_dvd_panel_video_twopass_changed(self)

        self.options_panel_dvd_panel_video_twopass = options_panel_dvd_panel_video_twopass(parent=self.options_panel_dvd_panel)
        self.options_panel_dvd_panel_video_twopass.setGeometry(190, 555, 78, 25)

        self.options_panel_dvd_panel_video_twopass_background = QLabel(parent=self.options_panel_dvd_panel_video_twopass)
        self.options_panel_dvd_panel_video_twopass_background.setGeometry(0,0,self.options_panel_dvd_panel_video_twopass.width(),self.options_panel_dvd_panel_video_twopass.height())

        self.options_panel_dvd_panel_gop_label = QLabel(parent=self.options_panel_dvd_panel)
        self.options_panel_dvd_panel_gop_label.setGeometry(10, 600, 170, 20)
        self.options_panel_dvd_panel_gop_label.setText('GOP SIZE')

        self.options_panel_dvd_panel_gop = QComboBox(parent=self.options_panel_dvd_panel)
        self.options_panel_dvd_panel_gop.setGeometry(10, 620, 170, 30)
        self.options_panel_dvd_panel_gop.addItems(['6','7','8','9','10','11', '12','13','14','15'])
        self.options_panel_dvd_panel_gop.activated.connect(lambda:options_panel_dvd_panel_gop_changed(self))

        self.options_panel_dvd_panel_audio_datarate_label = QLabel(parent=self.options_panel_dvd_panel)
        self.options_panel_dvd_panel_audio_datarate_label.setGeometry(190, 600, 170, 20)
        self.options_panel_dvd_panel_audio_datarate_label.setText('AUDIO DATA RATE')

        self.options_panel_dvd_panel_audio_datarate = QComboBox(parent=self.options_panel_dvd_panel)
        self.options_panel_dvd_panel_audio_datarate.setGeometry(190, 620, 170, 30)
        self.options_panel_dvd_panel_audio_datarate.addItems(['4608 kb/s (24/96)','3072 kb/s (16/96)','2304 kb/s (24/48)','1536 kb/s (DVD PCM)','512 kb/s','448 kb/s','384 kb/s','256 kb/s','224 kb/s (VCD MPA)','192 kb/s','160 kb/s','128 kb/s'])
        self.options_panel_dvd_panel_audio_datarate.activated.connect(lambda:options_panel_dvd_panel_audio_datarate_changed(self))

        self.options_panel_menu_panel = QWidget(parent=self.options_panel)

        self.options_panel_menu_buttons_label = QLabel('BUTTONS', parent=self.options_panel_menu_panel)

        self.options_panel_menu_buttons = QListWidget(parent=self.options_panel_menu_panel)
        self.options_panel_menu_buttons.clicked.connect(lambda:menu_button_selected(self))

        self.options_panel_menu_buttons_add = QPushButton(parent=self.options_panel_menu_panel)
        self.options_panel_menu_buttons_add.setGeometry(10,245,30,30)
        self.options_panel_menu_buttons_add.setIcon(QIcon(os.path.join(path_graphics, 'add.png')))
        self.options_panel_menu_buttons_add.clicked.connect(lambda:add_menu_button(self))

        self.options_panel_menu_buttons_remove = QPushButton(parent=self.options_panel_menu_panel)
        self.options_panel_menu_buttons_remove.setGeometry(45,245,30,30)
        self.options_panel_menu_buttons_remove.setIcon(QIcon(os.path.join(path_graphics, 'remove.png')))
        self.options_panel_menu_buttons_remove.clicked.connect(lambda:remove_menu_button(self))
        self.options_panel_menu_buttons_remove.setEnabled(False)

        self.options_panel_menu_buttons_edit = QPushButton(parent=self.options_panel_menu_panel)
        self.options_panel_menu_buttons_edit.setGeometry(80,245,30,30)
        self.options_panel_menu_buttons_edit.setIcon(QIcon(os.path.join(path_graphics, 'edit.png')))
        self.options_panel_menu_buttons_edit.clicked.connect(lambda:edit_menu_button(self))
        self.options_panel_menu_buttons_edit.setEnabled(False)

        self.options_panel_menu_buttons_edit_field = QLineEdit(parent=self.options_panel_menu_panel)
        self.options_panel_menu_buttons_edit_field.textEdited.connect(lambda:edit_field_menu_changed(self))
        self.options_panel_menu_buttons_edit_field.setVisible(False)

        self.options_panel_menu_buttons_edit_confirm = QPushButton(parent=self.options_panel_menu_panel)
        self.options_panel_menu_buttons_edit_confirm.setIcon(QIcon(os.path.join(path_graphics, 'confirm.png')))
        self.options_panel_menu_buttons_edit_confirm.clicked.connect(lambda:edit_confirm_menu_button(self))
        self.options_panel_menu_buttons_edit_confirm.setVisible(False)

        self.options_panel_menu_buttons_edit_cancel = QPushButton(parent=self.options_panel_menu_panel)
        self.options_panel_menu_buttons_edit_cancel.setIcon(QIcon(os.path.join(path_graphics, 'cancel.png')))
        self.options_panel_menu_buttons_edit_cancel.clicked.connect(lambda:edit_cancel_menu_button(self))
        self.options_panel_menu_buttons_edit_cancel.setVisible(False)

        self.options_panel_menu_buttons_position_box = QWidget(parent=self.options_panel_menu_panel)

        self.options_panel_menu_buttons_x_position_label = QLabel('<small>X POSITION</small>', parent=self.options_panel_menu_buttons_position_box)

        self.options_panel_menu_buttons_x_position = QSpinBox(parent=self.options_panel_menu_buttons_position_box)
        self.options_panel_menu_buttons_x_position.editingFinished.connect(lambda:menu_buttons_set_geometry(self))

        self.options_panel_menu_buttons_y_position_label = QLabel('<small>Y POSITION</small>', parent=self.options_panel_menu_buttons_position_box)

        self.options_panel_menu_buttons_y_position = QSpinBox(parent=self.options_panel_menu_buttons_position_box)
        self.options_panel_menu_buttons_y_position.editingFinished.connect(lambda:menu_buttons_set_geometry(self))

        self.options_panel_menu_buttons_width_label = QLabel('<small>WIDTH</small>', parent=self.options_panel_menu_buttons_position_box)

        self.options_panel_menu_buttons_width = QSpinBox(parent=self.options_panel_menu_buttons_position_box)
        self.options_panel_menu_buttons_width.editingFinished.connect(lambda:menu_buttons_set_geometry(self))

        self.options_panel_menu_buttons_height_label = QLabel('<small>HEIGHT</small>', parent=self.options_panel_menu_buttons_position_box)

        self.options_panel_menu_buttons_height = QSpinBox(parent=self.options_panel_menu_buttons_position_box)
        self.options_panel_menu_buttons_height.editingFinished.connect(lambda:menu_buttons_set_geometry(self))

        self.options_panel_menu_buttons_jumpto_label = QLabel('JUMP TO', parent=self.options_panel_menu_panel)
        self.options_panel_menu_buttons_jumpto_label.setGeometry(10,285,360,15)

        self.options_panel_menu_buttons_jumpto = QComboBox(parent=self.options_panel_menu_panel)
        self.options_panel_menu_buttons_jumpto.setGeometry(10,300,350,25)
        self.options_panel_menu_buttons_jumpto.activated.connect(lambda:button_jumpto_selected(self))

        self.options_panel_menu_buttons_directions_image = QLabel(parent=self.options_panel_menu_panel)
        self.options_panel_menu_buttons_directions_image.setGeometry(155,375,60,60)
        self.options_panel_menu_buttons_directions_image.setPixmap(QPixmap(os.path.join(path_graphics, 'options_panel_menu_positions.png')))

        self.options_panel_menu_buttons_directions_top_label = QLabel('TOP DIRECTION', parent=self.options_panel_menu_panel)
        self.options_panel_menu_buttons_directions_top_label.setGeometry(112,335,145,15)

        self.options_panel_menu_buttons_directions_top = QComboBox(parent=self.options_panel_menu_panel)
        self.options_panel_menu_buttons_directions_top.setGeometry(112,350,145,25)
        self.options_panel_menu_buttons_directions_top.activated.connect(lambda:button_directions_selected(self))

        self.options_panel_menu_buttons_directions_right_label = QLabel('RIGHT DIRECTION', parent=self.options_panel_menu_panel)
        self.options_panel_menu_buttons_directions_right_label.setGeometry(215,385,145,15)

        self.options_panel_menu_buttons_directions_right = QComboBox(parent=self.options_panel_menu_panel)
        self.options_panel_menu_buttons_directions_right.setGeometry(215,400,145,25)
        self.options_panel_menu_buttons_directions_right.activated.connect(lambda:button_directions_selected(self))

        self.options_panel_menu_buttons_directions_bottom_label = QLabel('BOTTOM DIRECTION', parent=self.options_panel_menu_panel)
        self.options_panel_menu_buttons_directions_bottom_label.setGeometry(112,435,145,15)

        self.options_panel_menu_buttons_directions_bottom = QComboBox(parent=self.options_panel_menu_panel)
        self.options_panel_menu_buttons_directions_bottom.setGeometry(112,450,145,25)
        self.options_panel_menu_buttons_directions_bottom.activated.connect(lambda:button_directions_selected(self))

        self.options_panel_menu_buttons_directions_left_label = QLabel('LEFT DIRECTION', parent=self.options_panel_menu_panel)
        self.options_panel_menu_buttons_directions_left_label.setGeometry(10,385,145,15)

        self.options_panel_menu_buttons_directions_left = QComboBox(parent=self.options_panel_menu_panel)
        self.options_panel_menu_buttons_directions_left.setGeometry(10,400,145,25)
        self.options_panel_menu_buttons_directions_left.activated.connect(lambda:button_directions_selected(self))

        self.options_panel_video_panel = QWidget(parent=self.options_panel)

        self.options_panel_video_intro_video_checkbox = QCheckBox('This is the intro video', parent=self.options_panel_video_panel)
        self.options_panel_video_intro_video_checkbox.clicked.connect(lambda:set_intro_video(self))

        self.options_panel_video_reencode_video_checkbox = QCheckBox('Reencode this video', parent=self.options_panel_video_panel)
        self.options_panel_video_reencode_video_checkbox.clicked.connect(lambda:set_reencode_video(self))

        self.options_panel_video_resolution_combo = QComboBox(parent=self.options_panel_video_panel)
        self.options_panel_video_resolution_combo.activated.connect(lambda:video_resolution_combo_selected(self))

        self.options_panel_video_jumpto_label = QLabel('JUMP TO', parent=self.options_panel_video_panel)

        self.options_panel_video_jumpto = QComboBox(parent=self.options_panel_video_panel)
        self.options_panel_video_jumpto.activated.connect(lambda:video_jumpto_selected(self))

        self.options_panel_video_chapters_list = QListWidget(parent=self.options_panel_video_panel)
        self.options_panel_video_chapters_list.clicked.connect(lambda:chapter_selected(self))

        self.options_panel_video_chapters_name_label = QLabel('<small>CHAPTER NAME</small>', parent=self.options_panel_video_panel)
        self.options_panel_video_chapters_name_label.setVisible(False)

        self.options_panel_video_chapters_name = QLineEdit(parent=self.options_panel_video_panel)
        self.options_panel_video_chapters_name.textEdited.connect(lambda:check_chapter_name(self))
        self.options_panel_video_chapters_name.setVisible(False)

        self.options_panel_video_chapters_timecode_label = QLabel('<small>TIMECODE</small>', parent=self.options_panel_video_panel)
        self.options_panel_video_chapters_timecode_label.setVisible(False)

        self.options_panel_video_chapters_timecode = QLineEdit(parent=self.options_panel_video_panel)
        self.options_panel_video_chapters_timecode.textEdited.connect(lambda:check_chapter_name(self))
        self.options_panel_video_chapters_timecode.setVisible(False)

        self.options_panel_video_chapters_edit_confirm = QPushButton(parent=self.options_panel_video_panel)
        self.options_panel_video_chapters_edit_confirm.clicked.connect(lambda:confirm_edit_chapter(self))
        self.options_panel_video_chapters_edit_confirm.setIcon(QIcon(os.path.join(path_graphics, 'confirm.png')))
        self.options_panel_video_chapters_edit_confirm.setVisible(False)

        self.options_panel_video_chapters_edit_cancel = QPushButton(parent=self.options_panel_video_panel)
        self.options_panel_video_chapters_edit_cancel.clicked.connect(lambda:hide_edit_chapter(self))
        self.options_panel_video_chapters_edit_cancel.setIcon(QIcon(os.path.join(path_graphics, 'cancel.png')))
        self.options_panel_video_chapters_edit_cancel.setVisible(False)

        self.options_panel_video_chapters_add = QPushButton(parent=self.options_panel_video_panel)
        self.options_panel_video_chapters_add.setIcon(QIcon(os.path.join(path_graphics, 'add.png')))
        self.options_panel_video_chapters_add.clicked.connect(lambda:add_chapter(self))

        self.options_panel_video_chapters_remove = QPushButton(parent=self.options_panel_video_panel)
        self.options_panel_video_chapters_remove.clicked.connect(lambda:remove_chapter(self))
        self.options_panel_video_chapters_remove.setIcon(QIcon(os.path.join(path_graphics, 'remove.png')))
        self.options_panel_video_chapters_remove.setEnabled(False)

        self.options_panel_video_chapters_edit = QPushButton(parent=self.options_panel_video_panel)
        self.options_panel_video_chapters_edit.clicked.connect(lambda:edit_chapter(self))
        self.options_panel_video_chapters_edit.setIcon(QIcon(os.path.join(path_graphics, 'edit.png')))
        self.options_panel_video_chapters_edit.setEnabled(False)

        self.options_panel_video_chapters_import = QPushButton(parent=self.options_panel_video_panel)
        self.options_panel_video_chapters_import.setIcon(QIcon(os.path.join(path_graphics, 'import.png')))
        self.options_panel_video_chapters_import.clicked.connect(lambda:import_chapters(self))

        self.nowediting_panel = QWidget(parent=self.main_panel)

        self.nowediting_panel_animation = QPropertyAnimation(self.nowediting_panel, b'geometry')
        self.nowediting_panel_animation.setEasingCurve(QEasingCurve.OutCirc)

        self.nowediting_dvd_panel = QWidget(parent=self.nowediting_panel)
        self.nowediting_dvd_panel_animation = QPropertyAnimation(self.nowediting_dvd_panel, b'geometry')
        self.nowediting_dvd_panel_animation.setEasingCurve(QEasingCurve.OutCirc)

        self.nowediting_dvd_panel_background = QLabel(parent=self.nowediting_dvd_panel)
        self.nowediting_dvd_panel_background.setStyleSheet("QLabel {background-image: url(" + os.path.join(path_graphics, "nowediting_panel_background.png").replace('\\', '/') + ");background-position: top left; background-repeat: repeat-x;}")
        self.nowediting_dvd_panel_background.setAlignment(Qt.AlignVCenter | Qt.AlignLeft)

        self.nowediting_dvd_panel_project_name_label = QLabel(parent=self.nowediting_dvd_panel)
        self.nowediting_dvd_panel_project_name_label.setForegroundRole(QPalette.Midlight)
        self.nowediting_dvd_panel_project_name_label.setText('PROJECT NAME')

        self.nowediting_dvd_panel_project_name = QLineEdit(parent=self.nowediting_dvd_panel)
        self.nowediting_dvd_panel_project_name.setGeometry(10,30,470,30)
        self.nowediting_dvd_panel_project_name.editingFinished.connect(lambda:update_changes(self))

        self.nowediting_dvd_panel_new_project_file_button = QPushButton(parent=self.nowediting_dvd_panel)
        self.nowediting_dvd_panel_new_project_file_button.setGeometry(10, 70, 150, 30)
        self.nowediting_dvd_panel_new_project_file_button.setIcon(QIcon(os.path.join(path_graphics, 'new.png')))
        self.nowediting_dvd_panel_new_project_file_button.setText('NEW PROJECT')
        self.nowediting_dvd_panel_new_project_file_button.clicked.connect(lambda:new_project_file(self))

        self.nowediting_dvd_panel_open_project_file_button = QPushButton(parent=self.nowediting_dvd_panel)
        self.nowediting_dvd_panel_open_project_file_button.setGeometry(170, 70, 150, 30)
        self.nowediting_dvd_panel_open_project_file_button.setIcon(QIcon(os.path.join(path_graphics, 'open.png')))
        self.nowediting_dvd_panel_open_project_file_button.setText('OPEN PROJECT')
        self.nowediting_dvd_panel_open_project_file_button.clicked.connect(lambda:open_project_file(self))

        self.nowediting_dvd_panel_save_project_file_button = QPushButton(parent=self.nowediting_dvd_panel)
        self.nowediting_dvd_panel_save_project_file_button.setGeometry(330, 70, 150, 30)
        self.nowediting_dvd_panel_save_project_file_button.setIcon(QIcon(os.path.join(path_graphics, 'save.png')))
        self.nowediting_dvd_panel_save_project_file_button.setText('SAVE PROJECT')
        self.nowediting_dvd_panel_save_project_file_button.clicked.connect(lambda:save_project_file(self))

        self.nowediting_dvd_panel_video_format_label = QLabel(parent=self.nowediting_dvd_panel)
        self.nowediting_dvd_panel_video_format_label.setGeometry(490,10,145,20)
        self.nowediting_dvd_panel_video_format_label.setForegroundRole(QPalette.Midlight)
        self.nowediting_dvd_panel_video_format_label.setText('VIDEO FORMAT')

        class nowediting_dvd_panel_video_format(QWidget):
            def mousePressEvent(widget, event):
                nowediting_dvd_panel_video_format_changed(self)

        self.nowediting_dvd_panel_video_format = nowediting_dvd_panel_video_format(parent=self.nowediting_dvd_panel)
        self.nowediting_dvd_panel_video_format.setGeometry(490, 30, 145, 25)

        self.nowediting_dvd_panel_video_format_background = QLabel(parent=self.nowediting_dvd_panel_video_format)
        self.nowediting_dvd_panel_video_format_background.setGeometry(0,0,self.nowediting_dvd_panel_video_format.width(),self.nowediting_dvd_panel_video_format.height())

        self.nowediting_dvd_panel_audio_format_label = QLabel(parent=self.nowediting_dvd_panel)
        self.nowediting_dvd_panel_audio_format_label.setGeometry(490,60,145,20)
        self.nowediting_dvd_panel_audio_format_label.setForegroundRole(QPalette.Midlight)
        self.nowediting_dvd_panel_audio_format_label.setText('AUDIO FORMAT')

        class nowediting_dvd_panel_audio_format(QWidget):
            def mousePressEvent(widget, event):
                nowediting_dvd_panel_audio_format_changed(self)

        self.nowediting_dvd_panel_audio_format = nowediting_dvd_panel_audio_format(parent=self.nowediting_dvd_panel)
        self.nowediting_dvd_panel_audio_format.setGeometry(490, 80, 145, 25)

        self.nowediting_dvd_panel_audio_format_background = QLabel(parent=self.nowediting_dvd_panel_audio_format)
        self.nowediting_dvd_panel_audio_format_background.setGeometry(0,0,self.nowediting_dvd_panel_audio_format.width(),self.nowediting_dvd_panel_audio_format.height())

        self.nowediting_dvd_panel_aspect_ratio_label = QLabel(parent=self.nowediting_dvd_panel)
        self.nowediting_dvd_panel_aspect_ratio_label.setGeometry(645,10,100,20)
        self.nowediting_dvd_panel_aspect_ratio_label.setForegroundRole(QPalette.Midlight)
        self.nowediting_dvd_panel_aspect_ratio_label.setText('ASPECT RATIO')

        class nowediting_dvd_panel_aspect_ratio(QWidget):
            def mousePressEvent(widget, event):
                nowediting_dvd_panel_aspect_ratio_changed(self)

        self.nowediting_dvd_panel_aspect_ratio = nowediting_dvd_panel_aspect_ratio(parent=self.nowediting_dvd_panel)
        self.nowediting_dvd_panel_aspect_ratio.setGeometry(645, 30, 100, 61)

        self.nowediting_dvd_panel_aspect_ratio_background = QLabel(parent=self.nowediting_dvd_panel_aspect_ratio)
        self.nowediting_dvd_panel_aspect_ratio_background.setGeometry(0,0,self.nowediting_dvd_panel_aspect_ratio.width(),self.nowediting_dvd_panel_aspect_ratio.height())

        self.nowediting_dvd_panel_has_menus_label = QLabel(parent=self.nowediting_dvd_panel)
        self.nowediting_dvd_panel_has_menus_label.setGeometry(755,10,145,20)
        self.nowediting_dvd_panel_has_menus_label.setForegroundRole(QPalette.Midlight)
        self.nowediting_dvd_panel_has_menus_label.setText('USE MENUS')

        class nowediting_dvd_panel_has_menus(QWidget):
            def mousePressEvent(widget, event):
                nowediting_dvd_panel_has_menus_changed(self)

        self.nowediting_dvd_panel_has_menus = nowediting_dvd_panel_has_menus(parent=self.nowediting_dvd_panel)
        self.nowediting_dvd_panel_has_menus.setGeometry(755, 30, 145, 25)

        self.nowediting_dvd_panel_has_menus_background = QLabel(parent=self.nowediting_dvd_panel_has_menus)
        self.nowediting_dvd_panel_has_menus_background.setGeometry(0,0,self.nowediting_dvd_panel_has_menus.width(),self.nowediting_dvd_panel_has_menus.height())

        self.nowediting_menus_panel = QWidget(parent=self.nowediting_panel)
        self.nowediting_menus_panel_animation = QPropertyAnimation(self.nowediting_menus_panel, b'geometry')
        self.nowediting_menus_panel_animation.setEasingCurve(QEasingCurve.OutCirc)

        self.nowediting_menus_panel_background = QLabel(parent=self.nowediting_menus_panel)
        self.nowediting_menus_panel_background.setStyleSheet("QLabel {background-image: url(" + os.path.join(path_graphics, "nowediting_panel_background.png").replace('\\', '/') + ");background-position: top left; background-repeat: repeat-x;}")
        self.nowediting_menus_panel_background.setAlignment(Qt.AlignVCenter | Qt.AlignLeft)

        self.nowediting_menus_panel_list = QListWidget(parent=self.nowediting_menus_panel)
        self.nowediting_menus_panel_list.setViewMode(QListView.IconMode)
        self.nowediting_menus_panel_list.clicked.connect(lambda:menu_selected(self))

        self.nowediting_menus_panel_duplicate = QPushButton(parent=self.nowediting_menus_panel)
        self.nowediting_menus_panel_duplicate.setIcon(QIcon(os.path.join(path_graphics, 'duplicate.png')))
        self.nowediting_menus_panel_duplicate.clicked.connect(lambda:duplicate_menu(self))
        self.nowediting_menus_panel_duplicate.setEnabled(False)

        self.nowediting_menus_panel_add = QPushButton(parent=self.nowediting_menus_panel)
        self.nowediting_menus_panel_add.setIcon(QIcon(os.path.join(path_graphics, 'add.png')))
        self.nowediting_menus_panel_add.clicked.connect(lambda:add_menu(self))

        self.nowediting_menus_panel_remove = QPushButton(parent=self.nowediting_menus_panel)
        self.nowediting_menus_panel_remove.clicked.connect(lambda:remove_menu(self))
        self.nowediting_menus_panel_remove.setIcon(QIcon(os.path.join(path_graphics, 'remove.png')))
        self.nowediting_menus_panel_remove.setEnabled(False)

        self.nowediting_videos_panel = QWidget(parent=self.nowediting_panel)
        self.nowediting_videos_panel_animation = QPropertyAnimation(self.nowediting_videos_panel, b'geometry')
        self.nowediting_videos_panel_animation.setEasingCurve(QEasingCurve.OutCirc)

        self.nowediting_videos_panel_background = QLabel(parent=self.nowediting_videos_panel)
        self.nowediting_videos_panel_background.setStyleSheet("QLabel {background-image: url(" + os.path.join(path_graphics, "nowediting_panel_background.png").replace('\\', '/') + ");background-position: top left; background-repeat: repeat-x;}")
        self.nowediting_videos_panel_background.setAlignment(Qt.AlignVCenter | Qt.AlignLeft)

        self.nowediting_videos_panel_list = QListWidget(parent=self.nowediting_videos_panel)
        self.nowediting_videos_panel_list.setViewMode(QListView.IconMode)
        self.nowediting_videos_panel_list.clicked.connect(lambda:video_selected(self))

        self.nowediting_videos_panel_add = QPushButton(parent=self.nowediting_videos_panel)
        self.nowediting_videos_panel_add.setIcon(QIcon(os.path.join(path_graphics, 'add.png')))
        self.nowediting_videos_panel_add.clicked.connect(lambda:add_video(self))

        self.nowediting_videos_panel_remove = QPushButton(parent=self.nowediting_videos_panel)
        self.nowediting_videos_panel_remove.clicked.connect(lambda:remove_video(self))
        self.nowediting_videos_panel_remove.setIcon(QIcon(os.path.join(path_graphics, 'remove.png')))
        self.nowediting_videos_panel_remove.setEnabled(False)

        self.top_panel = QWidget(parent=self.main_panel)

        self.top_panel_background = QLabel(parent=self.top_panel)
        self.top_panel_background.setStyleSheet("QLabel {background-image: url(" + os.path.join(path_graphics, "top_panel_background.png").replace('\\', '/') + ");background-position: top left; background-repeat: repeat-x;}")

        self.top_panel_project_name_label = QLabel(parent=self.main_panel)
        self.top_panel_project_name_label.setAlignment(Qt.AlignVCenter)
        self.top_panel_project_name_label.setForegroundRole(QPalette.Light)

        class nowediting_panel_dvd_button(QWidget):
            def enterEvent(widget, event):
                self.nowediting_panel_dvd_button_icon.setPixmap(QPixmap(os.path.join(path_graphics, 'nowediting_dvd_button_icon_over.png')))
            def leaveEvent(widget, event):
                self.nowediting_panel_dvd_button_icon.setPixmap(QPixmap(os.path.join(path_graphics, 'nowediting_dvd_button_icon.png')))
            def mousePressEvent(widget, event):
                nowediting_panel_button_changed(self, 'dvd')

        self.nowediting_panel_dvd_button = nowediting_panel_dvd_button(parent=self.nowediting_panel)
        self.nowediting_panel_dvd_button.setGeometry(-30,110,170,60)

        self.nowediting_panel_dvd_button_background = QLabel(parent=self.nowediting_panel_dvd_button)
        self.nowediting_panel_dvd_button_background.setGeometry(0,0,self.nowediting_panel_dvd_button.width(),self.nowediting_panel_dvd_button.height())
        self.nowediting_panel_dvd_button_background.setStyleSheet("QLabel {background-image: url(" + os.path.join(path_graphics, "nowediting_panel_background_tab.png").replace('\\', '/') + ");background-position: top left; background-repeat: repeat-x;}")

        self.nowediting_panel_dvd_button_icon = QLabel(parent=self.nowediting_panel_dvd_button)
        self.nowediting_panel_dvd_button_icon.setGeometry(0,0,self.nowediting_panel_dvd_button.width(),self.nowediting_panel_dvd_button.height())
        self.nowediting_panel_dvd_button_icon.setPixmap(QPixmap(os.path.join(path_graphics, 'nowediting_dvd_button_icon.png')))

        class nowediting_panel_menus_button(QWidget):
            def enterEvent(widget, event):
                self.nowediting_panel_menus_button_icon.setPixmap(QPixmap(os.path.join(path_graphics, 'nowediting_menus_button_icon_over.png')))
            def leaveEvent(widget, event):
                self.nowediting_panel_menus_button_icon.setPixmap(QPixmap(os.path.join(path_graphics, 'nowediting_menus_button_icon.png')))
            def mousePressEvent(widget, event):
                nowediting_panel_button_changed(self, 'menus')

        self.nowediting_panel_menus_button = nowediting_panel_menus_button(parent=self.nowediting_panel)
        self.nowediting_panel_menus_button.setGeometry(110,110,170,60)

        self.nowediting_panel_menus_button_background = QLabel(parent=self.nowediting_panel_menus_button)
        self.nowediting_panel_menus_button_background.setGeometry(0,0,self.nowediting_panel_menus_button.width(),self.nowediting_panel_menus_button.height())
        self.nowediting_panel_menus_button_background.setStyleSheet("QLabel {background-image: url(" + os.path.join(path_graphics, "nowediting_panel_background_tab.png").replace('\\', '/') + ");background-position: top left; background-repeat: repeat-x;}")

        self.nowediting_panel_menus_button_icon = QLabel(parent=self.nowediting_panel_menus_button)
        self.nowediting_panel_menus_button_icon.setGeometry(0,0,self.nowediting_panel_menus_button.width(),self.nowediting_panel_menus_button.height())
        self.nowediting_panel_menus_button_icon.setPixmap(QPixmap(os.path.join(path_graphics, 'nowediting_menus_button_icon.png')))

        class nowediting_panel_videos_button(QWidget):
            def enterEvent(widget, event):
                self.nowediting_panel_videos_button_icon.setPixmap(QPixmap(os.path.join(path_graphics, 'nowediting_videos_button_icon_over.png')))
            def leaveEvent(widget, event):
                self.nowediting_panel_videos_button_icon.setPixmap(QPixmap(os.path.join(path_graphics, 'nowediting_videos_button_icon.png')))
            def mousePressEvent(widget, event):
                nowediting_panel_button_changed(self, 'videos')

        self.nowediting_panel_videos_button = nowediting_panel_videos_button(parent=self.nowediting_panel)
        self.nowediting_panel_videos_button.setGeometry(250,110,170,60)

        self.nowediting_panel_videos_button_background = QLabel(parent=self.nowediting_panel_videos_button)
        self.nowediting_panel_videos_button_background.setGeometry(0,0,self.nowediting_panel_videos_button.width(),self.nowediting_panel_videos_button.height())
        self.nowediting_panel_videos_button_background.setStyleSheet("QLabel {background-image: url(" + os.path.join(path_graphics, "nowediting_panel_background_tab.png").replace('\\', '/') + ");background-position: top left; background-repeat: repeat-x;}")

        self.nowediting_panel_videos_button_icon = QLabel(parent=self.nowediting_panel_videos_button)
        self.nowediting_panel_videos_button_icon.setGeometry(0,0,self.nowediting_panel_videos_button.width(),self.nowediting_panel_videos_button.height())
        self.nowediting_panel_videos_button_icon.setPixmap(QPixmap(os.path.join(path_graphics, 'nowediting_videos_button_icon.png')))

        self.videos_player_panel = QWidget(parent=self.main_panel)
        self.videos_player_panel_animation = QPropertyAnimation(self.videos_player_panel, b'geometry')
        self.videos_player_panel_animation.setEasingCurve(QEasingCurve.OutCirc)

        class videos_player_timeline(QWidget):
            is_editing_trim_start = False
            is_editing_trim_end = False
            def paintEvent(widget, paintEvent):
                painter = QPainter(widget)

                if self.selected_video:
                    painter.setRenderHint(QPainter.Antialiasing)
                    pixmap_seek = QPixmap(os.path.join(path_graphics, 'videos_player_timeline_seek.png'))
                    pixmap_start = QPixmap(os.path.join(path_graphics, 'videos_player_timeline_start.png'))
                    pixmap_end = QPixmap(os.path.join(path_graphics, 'videos_player_timeline_end.png'))

                    rect_s = QRectF(0, 0, 1,widget.height())
                    rect_t = QRectF(0, 0, widget.width(),widget.height())
                    pixmap = QPixmap(os.path.join(path_graphics, 'videos_player_timeline_background.png'))
                    painter.drawPixmap(rect_s,pixmap,rect_t)
                    if self.nowediting == 'videos':
                        if self.actual_project['videos'][self.selected_video][6]:
                            painter.setBrush(QColor.fromRgb(0,0,0,alpha=100))
                            painter.setPen(QPen(Qt.NoPen))
                            rectangle = QRectF(0, 2, self.actual_project['videos'][self.selected_video][6] / ((self.actual_project['videos'][self.selected_video][5])/widget.width()),widget.height()-2)
                            painter.drawRect(rectangle)
                            painter.drawPixmap((self.actual_project['videos'][self.selected_video][6] / ((self.actual_project['videos'][self.selected_video][5])/widget.width()))-15,2,pixmap_start)
                            if self.videos_player_timeline.is_editing_trim_start:
                                painter.setPen(QColor.fromRgb(255,255,255,alpha=100))
                                painter.setFont(QFont("Ubuntu", 8))
                                rectangle = QRectF(0, 20, (self.actual_project['videos'][self.selected_video][6] / ((self.actual_project['videos'][self.selected_video][5])/widget.width()))-20,widget.height()-20)
                                painter.drawText(rectangle, Qt.AlignVCenter | Qt.AlignRight , 'To remove this\ncut mark, drag\noutside the\ntimeline.')
                        if self.actual_project['videos'][self.selected_video][7]:
                            painter.setBrush(QColor.fromRgb(0,0,0,alpha=100))
                            painter.setPen(QPen(Qt.NoPen))
                            rectangle = QRectF(self.actual_project['videos'][self.selected_video][7] / ((self.actual_project['videos'][self.selected_video][5])/widget.width()), 2, (widget.width() - (self.actual_project['videos'][self.selected_video][7] / ((self.actual_project['videos'][self.selected_video][5])/widget.width()))),widget.height()-2)
                            painter.drawRect(rectangle)
                            painter.drawPixmap((self.actual_project['videos'][self.selected_video][7] / ((self.actual_project['videos'][self.selected_video][5])/widget.width()))-3,2,pixmap_end)
                            if self.videos_player_timeline.is_editing_trim_end:
                                painter.setPen(QColor.fromRgb(255,255,255,alpha=100))
                                painter.setFont(QFont("Ubuntu", 8))
                                rectangle = QRectF((self.actual_project['videos'][self.selected_video][7] / ((self.actual_project['videos'][self.selected_video][5])/widget.width()))+20, 20, (widget.width() - (self.actual_project['videos'][self.selected_video][7] / ((self.actual_project['videos'][self.selected_video][5])/widget.width())))-20,widget.height()-20)
                                painter.drawText(rectangle, Qt.AlignVCenter | Qt.AlignLeft , 'To remove this\ncut mark, drag\noutside the\ntimeline.')
                        for chapter in self.actual_project['videos'][self.selected_video][1]:
                            text_size = painter.fontMetrics().width(chapter)
                            mark = convert_timecode_to_seconds(self.actual_project['videos'][self.selected_video][2][chapter]) / ((self.actual_project['videos'][self.selected_video][5])/widget.width())
                            if chapter == self.selected_video_chapter:
                                painter.setBrush(QColor.fromRgb(80,80,80,alpha=200))
                            else:
                                painter.setBrush(QColor.fromRgb(0,0,0,alpha=200))
                            painter.setPen(QColor.fromRgb(255,255,255))
                            polygon = QPolygonF()
                            polygon.append(QPointF(mark * 1.0, 0.0))
                            polygon.append(QPointF(mark * 1.0, 40.0))
                            polygon.append(QPointF((mark * 1.0) + text_size + 10, 40.0))
                            polygon.append(QPointF((mark * 1.0) + text_size + 10, 30.0))
                            polygon.append(QPointF((mark * 1.0) + 5.0, 30.0))
                            polygon.append(QPointF(mark * 1.0, 25.0))
                            painter.drawPolygon(polygon)
                            painter.setFont(QFont("Ubuntu", 8))
                            rectangle = QRectF((mark * 1.0) + 5.0, 30.0, text_size + 10.0,20.0)
                            painter.drawText(rectangle, Qt.AlignLeft, chapter)
                    painter.drawPixmap((self.preview_video_obj.get_position() * widget.width())-10,0,pixmap_seek)
                painter.end()

            def mousePressEvent(widget, event):
                for chapter in self.actual_project['videos'][self.selected_video][1]:
                    text_size = QFontMetrics(QFont("Ubuntu", 8)).width(chapter)
                    mark = convert_timecode_to_seconds(self.actual_project['videos'][self.selected_video][2][chapter]) / ((self.actual_project['videos'][self.selected_video][5])/widget.width())

                    if event.pos().y() > 29.0 and event.pos().y() < 41.0 and event.pos().x() > mark and event.pos().x() < (mark + text_size + 10.0):
                        self.selected_video_chapter = chapter
                        chapter_seek_in_timeline(self)
                        break
                    else:
                        self.selected_video_chapter = None

                if self.selected_video_chapter == None:
                    if self.actual_project['videos'][self.selected_video][6] and event.pos().x() > ((self.actual_project['videos'][self.selected_video][6] / ((self.actual_project['videos'][self.selected_video][5])/widget.width()))-15) and event.pos().x() < ((self.actual_project['videos'][self.selected_video][6] / ((self.actual_project['videos'][self.selected_video][5])/widget.width()))+3):
                        widget.is_editing_trim_start = (self.actual_project['videos'][self.selected_video][6] / ((self.actual_project['videos'][self.selected_video][5])/widget.width())) - event.pos().x()
                    elif self.actual_project['videos'][self.selected_video][7] and event.pos().x() > ((self.actual_project['videos'][self.selected_video][7] / ((self.actual_project['videos'][self.selected_video][5])/widget.width()))-3) and event.pos().x() < ((self.actual_project['videos'][self.selected_video][7] / ((self.actual_project['videos'][self.selected_video][5])/widget.width()))+15):
                        widget.is_editing_trim_end = event.pos().x() - (self.actual_project['videos'][self.selected_video][7] / ((self.actual_project['videos'][self.selected_video][5])/widget.width()))

                if (not widget.is_editing_trim_start or not widget.is_editing_trim_end or self.selected_video_chapter == None) and (event.pos().x() > 0 and event.pos().x() < widget.width()):
                    self.preview_video_obj.set_position(float(event.pos().x()) / float(widget.width()))
                update_timeline(self)

            def mouseReleaseEvent(widget, event):
                widget.is_editing_trim_start = False
                widget.is_editing_trim_end = False
                if widget.is_editing_trim_start and (self.actual_project['videos'][self.selected_video][6] < 0.0 or self.actual_project['videos'][self.selected_video][6] > self.actual_project['videos'][self.selected_video][6]):
                    self.actual_project['videos'][self.selected_video][6] = False

                elif widget.is_editing_trim_end and (self.actual_project['videos'][self.selected_video][7] < 0.0 or self.actual_project['videos'][self.selected_video][7] > self.actual_project['videos'][self.selected_video][6]):
                    self.actual_project['videos'][self.selected_video][7] = False

                update_timeline(self)

            def mouseMoveEvent(widget, event):
                if widget.is_editing_trim_start and not self.actual_project['videos'][self.selected_video][6] > self.actual_project['videos'][self.selected_video][7]:
                    self.actual_project['videos'][self.selected_video][6] = (event.pos().x() + widget.is_editing_trim_start) * ((self.actual_project['videos'][self.selected_video][5])/widget.width())
                    if event.pos().x() > 0 and event.pos().x() < widget.width():
                        self.preview_video_obj.set_position(self.actual_project['videos'][self.selected_video][6]/self.actual_project['videos'][self.selected_video][5])

                elif widget.is_editing_trim_end and not self.actual_project['videos'][self.selected_video][7] < self.actual_project['videos'][self.selected_video][6]:
                    self.actual_project['videos'][self.selected_video][7] = (event.pos().x() - widget.is_editing_trim_end) * ((self.actual_project['videos'][self.selected_video][5])/widget.width())
                    if event.pos().x() > 0 and event.pos().x() < widget.width():
                        self.preview_video_obj.set_position(self.actual_project['videos'][self.selected_video][7]/self.actual_project['videos'][self.selected_video][5])

                elif event.pos().x() > 0 and event.pos().x() < widget.width():
                    self.preview_video_obj.set_position(float(event.pos().x())  / float(widget.width()))
                update_timeline(self)

        self.videos_player_timeline = videos_player_timeline(parent=self.videos_player_panel)

        self.videos_player_controls_panel = QWidget(parent=self.videos_player_panel)
        self.videos_player_controls_panel.setGeometry(100,0,560,50)

        self.videos_player_controls_panel_background = QLabel(parent=self.videos_player_controls_panel)
        self.videos_player_controls_panel_background.setGeometry(0,0,self.videos_player_controls_panel.width(),self.videos_player_controls_panel.height())
        self.videos_player_controls_panel_background.setPixmap(QPixmap(os.path.join(path_graphics, 'videos_player_controls_panel_background.png')))

        class videos_player_controls_panel_trimstart(QWidget):
            def mousePressEvent(widget, event):
                video_set_trim_start(self)
                self.videos_player_controls_panel_trimstart_background.setPixmap(QPixmap(os.path.join(path_graphics, 'videos_player_controls_panel_trimstart_press.png')))
            def enterEvent(widget, event):
                self.videos_player_controls_panel_trimstart_background.setPixmap(QPixmap(os.path.join(path_graphics, 'videos_player_controls_panel_trimstart_over.png')))
            def mouseReleaseEvent(widget, event):
                self.videos_player_controls_panel_trimstart_background.setPixmap(QPixmap(os.path.join(path_graphics, 'videos_player_controls_panel_trimstart_background.png')))
            def leaveEvent(widget, event):
                self.videos_player_controls_panel_trimstart_background.setPixmap(QPixmap(os.path.join(path_graphics, 'videos_player_controls_panel_trimstart_background.png')))

        self.videos_player_controls_panel_trimstart = videos_player_controls_panel_trimstart(parent=self.videos_player_controls_panel)
        self.videos_player_controls_panel_trimstart.setGeometry(0,10,32,30)

        self.videos_player_controls_panel_trimstart_background = QLabel(parent=self.videos_player_controls_panel_trimstart)
        self.videos_player_controls_panel_trimstart_background.setGeometry(0,0,self.videos_player_controls_panel_trimstart.width(),self.videos_player_controls_panel_trimstart.height())
        self.videos_player_controls_panel_trimstart_background.setPixmap(QPixmap(os.path.join(path_graphics, 'videos_player_controls_panel_trimstart_background.png')))

        class videos_player_controls_panel_trimend(QWidget):
            def mousePressEvent(widget, event):
                video_set_trim_end(self)
                self.videos_player_controls_panel_trimend_background.setPixmap(QPixmap(os.path.join(path_graphics, 'videos_player_controls_panel_trimend_press.png')))
            def enterEvent(widget, event):
                self.videos_player_controls_panel_trimend_background.setPixmap(QPixmap(os.path.join(path_graphics, 'videos_player_controls_panel_trimend_over.png')))
            def mouseReleaseEvent(widget, event):
                self.videos_player_controls_panel_trimend_background.setPixmap(QPixmap(os.path.join(path_graphics, 'videos_player_controls_panel_trimend_background.png')))
            def leaveEvent(widget, event):
                self.videos_player_controls_panel_trimend_background.setPixmap(QPixmap(os.path.join(path_graphics, 'videos_player_controls_panel_trimend_background.png')))

        self.videos_player_controls_panel_trimend = videos_player_controls_panel_trimend(parent=self.videos_player_controls_panel)
        self.videos_player_controls_panel_trimend.setGeometry(32,10,33,30)

        self.videos_player_controls_panel_trimend_background = QLabel(parent=self.videos_player_controls_panel_trimend)
        self.videos_player_controls_panel_trimend_background.setGeometry(0,0,self.videos_player_controls_panel_trimend.width(),self.videos_player_controls_panel_trimend.height())
        self.videos_player_controls_panel_trimend_background.setPixmap(QPixmap(os.path.join(path_graphics, 'videos_player_controls_panel_trimend_background.png')))

        class videos_player_controls_panel_frameback(QWidget):
            def mousePressEvent(widget, event):
                video_seek_back_frame(self, 1.0 / (float(self.actual_project['videos'][self.selected_video][10]*self.actual_project['videos'][self.selected_video][5])))
                self.videos_player_controls_panel_frameback_background.setPixmap(QPixmap(os.path.join(path_graphics, 'videos_player_controls_panel_frameback_press.png')))
            def enterEvent(widget, event):
                self.videos_player_controls_panel_frameback_background.setPixmap(QPixmap(os.path.join(path_graphics, 'videos_player_controls_panel_frameback_over.png')))
            def mouseReleaseEvent(widget, event):
                self.videos_player_controls_panel_frameback_background.setPixmap(QPixmap(os.path.join(path_graphics, 'videos_player_controls_panel_frameback_background.png')))
            def leaveEvent(widget, event):
                self.videos_player_controls_panel_frameback_background.setPixmap(QPixmap(os.path.join(path_graphics, 'videos_player_controls_panel_frameback_background.png')))

        self.videos_player_controls_panel_frameback = videos_player_controls_panel_frameback(parent=self.videos_player_controls_panel)
        self.videos_player_controls_panel_frameback.setGeometry(60,0,73,50)

        self.videos_player_controls_panel_frameback_background = QLabel(parent=self.videos_player_controls_panel_frameback)
        self.videos_player_controls_panel_frameback_background.setGeometry(0,0,self.videos_player_controls_panel_frameback.width(),self.videos_player_controls_panel_frameback.height())
        self.videos_player_controls_panel_frameback_background.setPixmap(QPixmap(os.path.join(path_graphics, 'videos_player_controls_panel_frameback_background.png')))

        class videos_player_controls_panel_stop(QWidget):
            def mousePressEvent(widget, event):
                video_stop(self)
                self.videos_player_controls_panel_stop_background.setPixmap(QPixmap(os.path.join(path_graphics, 'videos_player_controls_panel_stop_press.png')))
                self.videos_player_controls_panel_play_background.setPixmap(QPixmap(os.path.join(path_graphics, 'videos_player_controls_panel_play_background.png')))
                self.videos_player_controls_panel_pause_background.setPixmap(QPixmap(os.path.join(path_graphics, 'videos_player_controls_panel_pause_background.png')))

            def enterEvent(widget, event):
                self.videos_player_controls_panel_stop_background.setPixmap(QPixmap(os.path.join(path_graphics, 'videos_player_controls_panel_stop_over.png')))
            def mouseReleaseEvent(widget, event):
                self.videos_player_controls_panel_stop_background.setPixmap(QPixmap(os.path.join(path_graphics, 'videos_player_controls_panel_stop_background.png')))
            def leaveEvent(widget, event):
                self.videos_player_controls_panel_stop_background.setPixmap(QPixmap(os.path.join(path_graphics, 'videos_player_controls_panel_stop_background.png')))

        self.videos_player_controls_panel_stop = videos_player_controls_panel_stop(parent=self.videos_player_controls_panel)
        self.videos_player_controls_panel_stop.setGeometry(132,0,48,50)

        self.videos_player_controls_panel_stop_background = QLabel(parent=self.videos_player_controls_panel_stop)
        self.videos_player_controls_panel_stop_background.setGeometry(0,0,self.videos_player_controls_panel_stop.width(),self.videos_player_controls_panel_stop.height())
        self.videos_player_controls_panel_stop_background.setPixmap(QPixmap(os.path.join(path_graphics, 'videos_player_controls_panel_stop_background.png')))

        class videos_player_controls_panel_play(QWidget):
            def mousePressEvent(widget, event):
                if not self.preview_video_obj.get_state() == vlc.State(3):
                    video_play(self)
                    self.videos_player_controls_panel_play_background.setPixmap(QPixmap(os.path.join(path_graphics, 'videos_player_controls_panel_play_press.png')))

            def enterEvent(widget, event):
                if not self.preview_video_obj.get_state() in [vlc.State(3), vlc.State(4)]:
                    self.videos_player_controls_panel_play_background.setPixmap(QPixmap(os.path.join(path_graphics, 'videos_player_controls_panel_play_over.png')))
            def leaveEvent(widget, event):
                if not self.preview_video_obj.get_state() in [vlc.State(3), vlc.State(4)]:
                    self.videos_player_controls_panel_play_background.setPixmap(QPixmap(os.path.join(path_graphics, 'videos_player_controls_panel_play_background.png')))

        self.videos_player_controls_panel_play = videos_player_controls_panel_play(parent=self.videos_player_controls_panel)
        self.videos_player_controls_panel_play.setGeometry(181,0,48,50)

        self.videos_player_controls_panel_play_background = QLabel(parent=self.videos_player_controls_panel_play)
        self.videos_player_controls_panel_play_background.setGeometry(0,0,self.videos_player_controls_panel_play.width(),self.videos_player_controls_panel_play.height())
        self.videos_player_controls_panel_play_background.setPixmap(QPixmap(os.path.join(path_graphics, 'videos_player_controls_panel_play_background.png')))

        class videos_player_controls_panel_pause(QWidget):
            def mousePressEvent(widget, event):
                if self.preview_video_obj.is_playing():
                    video_pause(self)
                else:
                    video_play(self)
            def enterEvent(widget, event):
                if self.preview_video_obj.get_state() == vlc.State(4):
                    self.videos_player_controls_panel_pause_background.setPixmap(QPixmap(os.path.join(path_graphics, 'videos_player_controls_panel_pause_press.png')))
                else:
                    self.videos_player_controls_panel_pause_background.setPixmap(QPixmap(os.path.join(path_graphics, 'videos_player_controls_panel_pause_over.png')))
            def mouseReleaseEvent(widget, event):
                if self.preview_video_obj.get_state() == vlc.State(4):
                    self.videos_player_controls_panel_pause_background.setPixmap(QPixmap(os.path.join(path_graphics, 'videos_player_controls_panel_pause_press.png')))
                else:
                    self.videos_player_controls_panel_pause_background.setPixmap(QPixmap(os.path.join(path_graphics, 'videos_player_controls_panel_pause_background.png')))

            def leaveEvent(widget, event):
                if self.preview_video_obj.get_state() == vlc.State(4):
                    self.videos_player_controls_panel_pause_background.setPixmap(QPixmap(os.path.join(path_graphics, 'videos_player_controls_panel_pause_press.png')))
                else:
                    self.videos_player_controls_panel_pause_background.setPixmap(QPixmap(os.path.join(path_graphics, 'videos_player_controls_panel_pause_background.png')))

        self.videos_player_controls_panel_pause = videos_player_controls_panel_pause(parent=self.videos_player_controls_panel)
        self.videos_player_controls_panel_pause.setGeometry(229,0,48,50)

        self.videos_player_controls_panel_pause_background = QLabel(parent=self.videos_player_controls_panel_pause)
        self.videos_player_controls_panel_pause_background.setGeometry(0,0,self.videos_player_controls_panel_pause.width(),self.videos_player_controls_panel_pause.height())
        self.videos_player_controls_panel_pause_background.setPixmap(QPixmap(os.path.join(path_graphics, 'videos_player_controls_panel_pause_background.png')))

        class videos_player_controls_panel_mark(QWidget):
            def mousePressEvent(widget, event):
                video_add_this_mark_frame(self)
                self.videos_player_controls_panel_mark_background.setPixmap(QPixmap(os.path.join(path_graphics, 'videos_player_controls_panel_mark_press.png')))
            def enterEvent(widget, event):
                self.videos_player_controls_panel_mark_background.setPixmap(QPixmap(os.path.join(path_graphics, 'videos_player_controls_panel_mark_over.png')))
            def mouseReleaseEvent(widget, event):
                self.videos_player_controls_panel_mark_background.setPixmap(QPixmap(os.path.join(path_graphics, 'videos_player_controls_panel_mark_background.png')))
            def leaveEvent(widget, event):
                self.videos_player_controls_panel_mark_background.setPixmap(QPixmap(os.path.join(path_graphics, 'videos_player_controls_panel_mark_background.png')))

        self.videos_player_controls_panel_mark = videos_player_controls_panel_mark(parent=self.videos_player_controls_panel)
        self.videos_player_controls_panel_mark.setGeometry(277,0,48,50)

        self.videos_player_controls_panel_mark_background = QLabel(parent=self.videos_player_controls_panel_mark)
        self.videos_player_controls_panel_mark_background.setGeometry(0,0,self.videos_player_controls_panel_mark.width(),self.videos_player_controls_panel_mark.height())
        self.videos_player_controls_panel_mark_background.setPixmap(QPixmap(os.path.join(path_graphics, 'videos_player_controls_panel_mark_background.png')))

        class videos_player_controls_panel_frameforward(QWidget):
            def mousePressEvent(widget, event):
                video_seek_next_frame(self, 1.0 / (float(self.actual_project['videos'][self.selected_video][10]*self.actual_project['videos'][self.selected_video][5])))
                self.videos_player_controls_panel_frameforward_background.setPixmap(QPixmap(os.path.join(path_graphics, 'videos_player_controls_panel_frameforward_press.png')))
            def enterEvent(widget, event):
                self.videos_player_controls_panel_frameforward_background.setPixmap(QPixmap(os.path.join(path_graphics, 'videos_player_controls_panel_frameforward_over.png')))
            def mouseReleaseEvent(widget, event):
                self.videos_player_controls_panel_frameforward_background.setPixmap(QPixmap(os.path.join(path_graphics, 'videos_player_controls_panel_frameforward_background.png')))
            def leaveEvent(widget, event):
                self.videos_player_controls_panel_frameforward_background.setPixmap(QPixmap(os.path.join(path_graphics, 'videos_player_controls_panel_frameforward_background.png')))

        self.videos_player_controls_panel_frameforward = videos_player_controls_panel_frameforward(parent=self.videos_player_controls_panel)
        self.videos_player_controls_panel_frameforward.setGeometry(325,0,74,50)

        self.videos_player_controls_panel_frameforward_background = QLabel(parent=self.videos_player_controls_panel_frameforward)
        self.videos_player_controls_panel_frameforward_background.setGeometry(0,0,self.videos_player_controls_panel_frameforward.width(),self.videos_player_controls_panel_frameforward.height())
        self.videos_player_controls_panel_frameforward_background.setPixmap(QPixmap(os.path.join(path_graphics, 'videos_player_controls_panel_frameforward_background.png')))

        self.videos_player_controls_panel_current_time = QLabel(parent=self.videos_player_controls_panel)
        self.videos_player_controls_panel_current_time.setGeometry(404,10,146,30)
        self.videos_player_controls_panel_current_time.setAlignment(Qt.AlignCenter | Qt.AlignVCenter)
        self.videos_player_controls_panel_current_time.setStyleSheet('font-family: "Ubuntu Mono"; font-size:22px; color:#9AC3CF')

        self.menus_properties_panel = QWidget(parent=self.main_panel)
        self.menus_properties_panel_animation = QPropertyAnimation(self.menus_properties_panel, b'geometry')
        self.menus_properties_panel_animation.setEasingCurve(QEasingCurve.OutCirc)

        self.menus_properties_panel_background_left = QLabel(parent=self.menus_properties_panel)
        self.menus_properties_panel_background_left.setStyleSheet("QLabel { background-image: url(" + os.path.join(path_graphics, "menus_properties_panel_background.png").replace('\\', '/') + "); background-position: top left; background-repeat: repeat-x; }")

        self.menus_properties_panel_background_center = QLabel('<font style="color:white;">PROPERTIES</font>', parent=self.menus_properties_panel)
        self.menus_properties_panel_background_center.setStyleSheet("QLabel { padding-top:3px; background-image: url(" + os.path.join(path_graphics, "menus_properties_panel_background_center.png").replace('\\', '/') + "); background-position: top middle; background-repeat: no-repeat; }")
        self.menus_properties_panel_background_center.setAlignment(Qt.AlignTop | Qt.AlignHCenter)

        self.menus_properties_panel_background_right = QLabel(parent=self.menus_properties_panel)
        self.menus_properties_panel_background_right.setStyleSheet("QLabel { background-image: url(" + os.path.join(path_graphics, "menus_properties_panel_background.png").replace('\\', '/') + "); background-position: top right; background-repeat: repeat-x; }")

        self.menus_properties_panel_background_file_label = QLabel(parent=self.menus_properties_panel)
        self.menus_properties_panel_background_file_label.setGeometry(10,30,90,20)
        self.menus_properties_panel_background_file_label.setText('BACKGROUND')

        class menus_properties_panel_background_file_preview(QWidget):
            def enterEvent(widget, event):
                self.menus_properties_panel_background_file_preview_open_button.setVisible(True)
                self.menus_properties_panel_background_file_preview_remove_button.setVisible(True)
            def leaveEvent(widget, event):
                self.menus_properties_panel_background_file_preview_open_button.setVisible(False)
                self.menus_properties_panel_background_file_preview_remove_button.setVisible(False)

        self.menus_properties_panel_background_file_preview = menus_properties_panel_background_file_preview(parent=self.menus_properties_panel)

        self.menus_properties_panel_background_file_preview_background = QLabel(parent=self.menus_properties_panel_background_file_preview)
        self.menus_properties_panel_background_file_preview_background.setScaledContents(True)
        self.menus_properties_panel_background_file_preview_foreground = QLabel(parent=self.menus_properties_panel_background_file_preview)

        self.menus_properties_panel_background_file_preview_open_button = QPushButton(QIcon(os.path.join(path_graphics, 'open.png')), '', parent=self.menus_properties_panel_background_file_preview)
        self.menus_properties_panel_background_file_preview_open_button.setGeometry(5,15,30,30)
        self.menus_properties_panel_background_file_preview_open_button.setVisible(False)
        self.menus_properties_panel_background_file_preview_open_button.clicked.connect(lambda:select_menu_file(self))

        self.menus_properties_panel_background_file_preview_remove_button = QPushButton(QIcon(os.path.join(path_graphics, 'remove.png')), '', parent=self.menus_properties_panel_background_file_preview)
        self.menus_properties_panel_background_file_preview_remove_button.setGeometry(40,15,30,30)
        self.menus_properties_panel_background_file_preview_remove_button.setVisible(False)
        self.menus_properties_panel_background_file_preview_remove_button.clicked.connect(lambda:remove_menu_file(self))

        self.menus_properties_panel_overlay_file_label = QLabel(parent=self.menus_properties_panel)
        self.menus_properties_panel_overlay_file_label.setGeometry(110,30,90,20)
        self.menus_properties_panel_overlay_file_label.setText('OVERLAY')

        class menus_properties_panel_overlay_file_preview(QWidget):
            def enterEvent(widget, event):
                self.menus_properties_panel_overlay_file_preview_open_button.setVisible(True)
                self.menus_properties_panel_overlay_file_preview_remove_button.setVisible(True)
            def leaveEvent(widget, event):
                self.menus_properties_panel_overlay_file_preview_open_button.setVisible(False)
                self.menus_properties_panel_overlay_file_preview_remove_button.setVisible(False)

        self.menus_properties_panel_overlay_file_preview = menus_properties_panel_overlay_file_preview(parent=self.menus_properties_panel)

        self.menus_properties_panel_overlay_file_preview_background = QLabel(parent=self.menus_properties_panel_overlay_file_preview)
        self.menus_properties_panel_overlay_file_preview_background.setScaledContents(True)
        self.menus_properties_panel_overlay_file_preview_foreground = QLabel(parent=self.menus_properties_panel_overlay_file_preview)

        self.menus_properties_panel_overlay_file_preview_open_button = QPushButton(QIcon(os.path.join(path_graphics, 'open.png')), '', parent=self.menus_properties_panel_overlay_file_preview)
        self.menus_properties_panel_overlay_file_preview_open_button.setGeometry(5,15,30,30)
        self.menus_properties_panel_overlay_file_preview_open_button.setVisible(False)
        self.menus_properties_panel_overlay_file_preview_open_button.clicked.connect(lambda:select_overlay_file(self))

        self.menus_properties_panel_overlay_file_preview_remove_button = QPushButton(QIcon(os.path.join(path_graphics, 'remove.png')), '', parent=self.menus_properties_panel_overlay_file_preview)
        self.menus_properties_panel_overlay_file_preview_remove_button.setGeometry(40,15,30,30)
        self.menus_properties_panel_overlay_file_preview_remove_button.setVisible(False)
        self.menus_properties_panel_overlay_file_preview_remove_button.clicked.connect(lambda:remove_overlay_file(self))

        self.menus_properties_panel_color_label = QLabel(parent=self.menus_properties_panel)
        self.menus_properties_panel_color_label.setGeometry(210,30,50,20)
        self.menus_properties_panel_color_label.setText('COLOR')

        self.options_panel_menu_choose_color_button = QPushButton(parent=self.menus_properties_panel)
        self.options_panel_menu_choose_color_button.setGeometry(210, 50, 50,50)
        self.options_panel_menu_choose_color_button.clicked.connect(lambda:choose_color(self))

        self.menus_properties_panel_transparency_slider_label = QLabel('OPACITY', parent=self.menus_properties_panel)
        self.menus_properties_panel_transparency_slider_label.setGeometry(270,30,100,20)

        self.menus_properties_panel_transparency_slider = QDial(parent=self.menus_properties_panel)
        self.menus_properties_panel_transparency_slider.setGeometry(270,50,50,50)
        self.menus_properties_panel_transparency_slider.setMaximum(100)
        self.menus_properties_panel_transparency_slider.setMinimum(0)
        self.menus_properties_panel_transparency_slider.valueChanged.connect(lambda:transparency_slider_changing(self))
        self.menus_properties_panel_transparency_slider.sliderReleased.connect(lambda:transparency_slider_changed(self))

        self.menus_properties_panel_transparency_slider_value = QLabel(parent=self.menus_properties_panel)
        self.menus_properties_panel_transparency_slider_value.setAlignment(Qt.AlignVCenter)
        self.menus_properties_panel_transparency_slider_value.setGeometry(320,50,50,50)

        self.menus_properties_panel_border_slider_label = QLabel('BORDER', parent=self.menus_properties_panel)
        self.menus_properties_panel_border_slider_label.setGeometry(370,30,100,20)

        self.menus_properties_panel_border_slider = QDial(parent=self.menus_properties_panel)
        self.menus_properties_panel_border_slider.setGeometry(370,50,50,50)
        self.menus_properties_panel_border_slider.setMaximum(100)
        self.menus_properties_panel_border_slider.setMinimum(0)
        self.menus_properties_panel_border_slider.valueChanged.connect(lambda:border_slider_changing(self))
        self.menus_properties_panel_border_slider.sliderReleased.connect(lambda:border_slider_changed(self))

        self.menus_properties_panel_border_slider_value = QLabel(parent=self.menus_properties_panel)
        self.menus_properties_panel_border_slider_value.setAlignment(Qt.AlignVCenter)
        self.menus_properties_panel_border_slider_value.setGeometry(420,50,50,50)

        self.menus_properties_panel_sound_box = QLabel('SOUND', parent=self.menus_properties_panel)
        self.menus_properties_panel_sound_box.setGeometry(470,30,100,20)

        self.menus_properties_panel_sound_label = QLabel(parent=self.menus_properties_panel)
        self.menus_properties_panel_sound_label.setGeometry(470,50,100,30)
        self.menus_properties_panel_sound_label.setAlignment(Qt.AlignVCenter | Qt.AlignLeft)

        self.menus_properties_panel_sound_open_button = QPushButton(QIcon(os.path.join(path_graphics, 'open.png')), 'OPEN', parent=self.menus_properties_panel)
        self.menus_properties_panel_sound_open_button.setGeometry(470, 80, 100,20)
        self.menus_properties_panel_sound_open_button.clicked.connect(lambda:select_menu_sound_file(self))

        class menus_properties_panel_overlay_preview(QWidget):
            def mousePressEvent(widget, event):
                preview_overlay_clicked(self)

        self.menus_properties_panel_overlay_preview = menus_properties_panel_overlay_preview(parent=self.menus_properties_panel)
        self.menus_properties_panel_overlay_preview.setGeometry(630,30,180,25)

        self.menus_properties_panel_overlay_preview_background = QLabel(parent=self.menus_properties_panel_overlay_preview)
        self.menus_properties_panel_overlay_preview_background.setGeometry(0,0,self.menus_properties_panel_overlay_preview.width(),self.menus_properties_panel_overlay_preview.height())

        self.menus_properties_panel_overlay_preview_label = QLabel('PREVIEW OVERLAY', parent=self.menus_properties_panel_overlay_preview)
        self.menus_properties_panel_overlay_preview_label.setGeometry(25,0,self.menus_properties_panel_overlay_preview.width()-25,self.menus_properties_panel_overlay_preview.height())
        self.menus_properties_panel_overlay_preview_label.setForegroundRole(QPalette.Light)

        class menus_properties_panel_directions_preview(QWidget):
            def mousePressEvent(widget, event):
                preview_directions(self)

        self.menus_properties_panel_directions_preview = menus_properties_panel_directions_preview(parent=self.menus_properties_panel)
        self.menus_properties_panel_directions_preview.setGeometry(630,55,180,25)

        self.menus_properties_panel_directions_preview_background = QLabel(parent=self.menus_properties_panel_directions_preview)
        self.menus_properties_panel_directions_preview_background.setGeometry(0,0,self.menus_properties_panel_directions_preview.width(),self.menus_properties_panel_directions_preview.height())

        self.menus_properties_panel_directions_preview_label = QLabel('PREVIEW DIRECTIONS', parent=self.menus_properties_panel_directions_preview)
        self.menus_properties_panel_directions_preview_label.setGeometry(25,0,self.menus_properties_panel_directions_preview.width()-25,self.menus_properties_panel_directions_preview.height())
        self.menus_properties_panel_directions_preview_label.setForegroundRole(QPalette.Light)

        class menus_properties_panel_main_menu_checkbox(QWidget):
            def mousePressEvent(widget, event):
                set_main_menu(self)

        self.menus_properties_panel_main_menu_checkbox = menus_properties_panel_main_menu_checkbox(parent=self.menus_properties_panel)
        self.menus_properties_panel_main_menu_checkbox.setGeometry(626,85,78,25)

        self.menus_properties_panel_main_menu_checkbox_background = QLabel(parent=self.menus_properties_panel_main_menu_checkbox)
        self.menus_properties_panel_main_menu_checkbox_background.setGeometry(0,0,self.menus_properties_panel_main_menu_checkbox.width(),self.menus_properties_panel_main_menu_checkbox.height())

        self.menus_properties_panel_jumpto_label = QLabel('AT THE END, JUMP TO', parent=self.menus_properties_panel)
        self.menus_properties_panel_jumpto_label.setGeometry(820,30,150,15)

        self.menus_properties_panel_jumpto = QComboBox(parent=self.menus_properties_panel)
        self.menus_properties_panel_jumpto.setGeometry(820,45,150,25)
        self.menus_properties_panel_jumpto.activated.connect(lambda:menus_properties_panel_jumpto_selected(self))

        self.lock_finalize_panel = QWidget(parent=self.main_panel)
        self.lock_finalize_panel_animation = QPropertyAnimation(self.lock_finalize_panel, b'geometry')
        self.lock_finalize_panel_animation.setEasingCurve(QEasingCurve.OutCirc)

        self.lock_finalize_panel_background = QLabel(parent=self.lock_finalize_panel)
        self.lock_finalize_panel_background.setPixmap(QPixmap(os.path.join(path_graphics, 'lock_finalize_panel_background.png')))
        self.lock_finalize_panel_background.setStyleSheet("QLabel { background-image: url(" + os.path.join(path_graphics, "lock_finalize_panel_background.png").replace('\\', '/') + "); background-position: top; background-repeat: repeat-x; }")

        self.lock_finalize_dvd_image = QLabel(parent=self.lock_finalize_panel)
        self.lock_finalize_dvd_image.setGeometry(150,245,300,300)
        self.lock_finalize_dvd_image.setPixmap(QPixmap(os.path.join(path_graphics, 'finalize_dvd_image.png')))

        self.lock_finalize_panel_progress_bar_label = QLabel('PROGRESS', parent=self.lock_finalize_panel)
        self.lock_finalize_panel_progress_bar_label.setGeometry(460, 345, 400, 30)
        self.lock_finalize_panel_progress_bar_label.setForegroundRole(QPalette.Light)

        self.lock_finalize_panel_progress_bar = QProgressBar(parent=self.lock_finalize_panel)
        self.lock_finalize_panel_progress_bar.setGeometry(460, 375, 400, 40)

        self.lock_finalize_panel_progress_bar_description = QLabel(parent=self.lock_finalize_panel)
        self.lock_finalize_panel_progress_bar_description.setGeometry(460, 415, 400, 30)
        self.lock_finalize_panel_progress_bar_description.setForegroundRole(QPalette.Light)

        from modules import finalize_panel
        self.finalize_panel = finalize_panel
        self.finalize_panel.load(self, path_graphics)

        self.generate_dvd_thread_thread = generate_dvd_thread()
        self.generate_dvd_thread_thread.signal.sig.connect(self.generate_dvd_thread_thread_completed)

        self.setGeometry(QDesktopWidget().screenGeometry().width()*.1, QDesktopWidget().screenGeometry().height()*.1, QDesktopWidget().screenGeometry().width()*.8, QDesktopWidget().screenGeometry().height()*.8)

    def generate_dvd_thread_thread_completed(self, data):
        if data.startswith('START'):
            self.generate_effect(self.lock_finalize_panel_animation, 'geometry', 1000, [self.lock_finalize_panel.x(),self.lock_finalize_panel.y(),self.lock_finalize_panel.width(),self.lock_finalize_panel.height()], [0,0,self.lock_finalize_panel.width(),self.lock_finalize_panel.height()])
            self.is_generating = False
            self.finalize_panel.generate_button.setVisible(False)

            self.generate_effect(self.finalize_panel.animation, 'geometry', 500, [self.finalize_panel.x(),self.finalize_panel.y(),self.finalize_panel.width(),self.finalize_panel.height()], [self.main_panel.width(),self.finalize_panel.y(),self.finalize_panel.width(),self.finalize_panel.height()])

            self.lock_finalize_panel_progress_bar.setMaximum(int(data.split(',')[2]))
            self.lock_finalize_panel_progress_bar.setValue(0)
            self.lock_finalize_panel_progress_bar_description.setText("PROCESSING INTRO VIDEO")
        elif data.startswith('FINISH'):
            self.generate_effect(self.lock_finalize_panel_animation, 'geometry', 1000, [self.lock_finalize_panel.x(),self.lock_finalize_panel.y(),self.lock_finalize_panel.width(),self.lock_finalize_panel.height()], [0,self.main_panel.height(),self.lock_finalize_panel.width(),self.lock_finalize_panel.height()])
            self.is_generating = False
            self.finalize_panel.generate_button.setVisible(True)
            self.generate_effect(self.finalize_panel.animation, 'geometry', 500, [self.finalize_panel.x(),self.finalize_panel.y(),self.finalize_panel.width(),self.finalize_panel.height()], [self.main_panel.width() - 260,self.finalize_panel.y(),self.finalize_panel.width(),self.finalize_panel.height()])
        else:
            self.lock_finalize_panel_progress_bar.setValue(int(data.split(',')[1]))
            self.lock_finalize_panel_progress_bar_description.setText(data.split(',')[0])

    def closeEvent(self, event):
        shutil.rmtree(path_tmp, ignore_errors=True)

    def generate_effect(self, widget, effect_type, duration, startValue, endValue):
        widget.setDuration(duration)
        if effect_type == 'geometry':
            widget.setStartValue(QRect(startValue[0],startValue[1],startValue[2],startValue[3]))
            widget.setEndValue(QRect(endValue[0],endValue[1],endValue[2],endValue[3]))
        elif effect_type == 'opacity':
            widget.setStartValue(startValue)
            widget.setEndValue(endValue)
        widget.start()

    def resizeEvent(self, event):
        self.main_panel.setGeometry(0, 0, self.width(), self.height())
        self.top_panel.setGeometry(0,0,self.main_panel.width(),90)
        self.content_panel.setGeometry(0,-40,self.main_panel.width(),self.main_panel.height()+120)
        self.content_panel_background.setGeometry(0,0,self.content_panel.width(), self.content_panel.height())
        self.options_panel.setGeometry(self.main_panel.width()-380,120,380,self.main_panel.height()-80)
        self.options_panel_background.setGeometry(0,0,self.options_panel.width(), self.options_panel.height())
        if self.is_generating:
            self.lock_finalize_panel.setGeometry(0,self.main_panel.height(),self.main_panel.width(),self.main_panel.height())
        else:
            self.lock_finalize_panel.setGeometry(0,self.main_panel.height(),self.main_panel.width(),self.main_panel.height())
        self.lock_finalize_panel_background.setGeometry(0,0,self.lock_finalize_panel.width(),self.lock_finalize_panel.height())
        self.options_panel_dvd_panel.setGeometry(10,0,self.options_panel.width()-10,self.options_panel.height())
        self.options_panel_menu_panel.setGeometry(10,0,self.options_panel.width()-10,self.options_panel.height())
        self.options_panel_menu_buttons_label.setGeometry(10,35,self.options_panel_menu_panel.width()-20,15)
        self.options_panel_menu_buttons.setGeometry(10,50,self.options_panel_menu_panel.width()-80,190)
        self.options_panel_menu_buttons_edit_field.setGeometry(10,245,self.options_panel_menu_panel.width()-90,30)
        self.options_panel_menu_buttons_edit_confirm.setGeometry(self.options_panel_menu_panel.width()-75,245,30,30)
        self.options_panel_menu_buttons_edit_cancel.setGeometry(self.options_panel_menu_panel.width()-40,245,30,30)
        self.options_panel_menu_buttons_position_box.setGeometry(self.options_panel_menu_panel.width()-60,50,50,215)
        self.options_panel_menu_buttons_x_position_label.setGeometry(0,0,self.options_panel_menu_buttons_position_box.width(),15)
        self.options_panel_menu_buttons_x_position_label.setGeometry(0,0,self.options_panel_menu_buttons_position_box.width(),15)
        self.options_panel_menu_buttons_x_position.setGeometry(0, 15, self.options_panel_menu_buttons_position_box.width(), 20)
        self.options_panel_menu_buttons_y_position_label.setGeometry(0,40,self.options_panel_menu_buttons_position_box.width(),15)
        self.options_panel_menu_buttons_y_position.setGeometry(0, 55, self.options_panel_menu_buttons_position_box.width(), 20)
        self.options_panel_menu_buttons_width_label.setGeometry(0,80,self.options_panel_menu_buttons_position_box.width(),15)
        self.options_panel_menu_buttons_width.setGeometry(0, 95, self.options_panel_menu_buttons_position_box.width(), 20)
        self.options_panel_menu_buttons_height_label.setGeometry(0,120,self.options_panel_menu_buttons_position_box.width(),15)
        self.options_panel_menu_buttons_height.setGeometry(0, 135, self.options_panel_menu_buttons_position_box.width(), 20)
        self.options_panel_video_panel.setGeometry(10,50,self.options_panel.width()-10,self.options_panel.height()-50)
        self.options_panel_video_intro_video_checkbox.setGeometry(10,0,self.options_panel_video_panel.width()-20,25)
        self.options_panel_video_reencode_video_checkbox.setGeometry(10,35,self.options_panel_video_panel.width()-20,25)
        self.options_panel_video_resolution_combo.setGeometry(10,70,self.options_panel_video_panel.width()-20,25)
        self.options_panel_video_jumpto_label.setGeometry(10,105,self.options_panel_video_panel.width()-20,15)
        self.options_panel_video_jumpto.setGeometry(10,120,self.options_panel_video_panel.width()-20,25)
        self.options_panel_video_chapters_list.setGeometry(10,155,self.options_panel_video_panel.width()-20,self.options_panel_video_panel.height()-205)
        self.options_panel_video_chapters_name_label.setGeometry(10,self.options_panel_video_panel.height()-55,((self.options_panel_video_panel.width()-90)*.5)-5,15)
        self.options_panel_video_chapters_name.setGeometry(10,self.options_panel_video_panel.height()-40,((self.options_panel_video_panel.width()-90)*.5)-5,30)
        self.options_panel_video_chapters_timecode_label.setGeometry(((self.options_panel_video_panel.width()-90)*.5)+10,self.options_panel_video_panel.height()-55,((self.options_panel_video_panel.width()-90)*.5)-5,15)
        self.options_panel_video_chapters_timecode.setGeometry(((self.options_panel_video_panel.width()-90)*.5)+10,self.options_panel_video_panel.height()-40,((self.options_panel_video_panel.width()-90)*.5)-5,30)
        self.options_panel_video_chapters_edit_confirm.setGeometry(self.options_panel_video_panel.width()-75,self.options_panel_video_panel.height()-40,30,30)
        self.options_panel_video_chapters_edit_cancel.setGeometry(self.options_panel_video_panel.width()-40,self.options_panel_video_panel.height()-40,30,30)
        self.options_panel_video_chapters_add.setGeometry(10,self.options_panel_video_panel.height()-40,30,30)
        self.options_panel_video_chapters_remove.setGeometry(45,self.options_panel_video_panel.height()-40,30,30)
        self.options_panel_video_chapters_edit.setGeometry(80,self.options_panel_video_panel.height()-40,30,30)
        self.options_panel_video_chapters_import.setGeometry(self.options_panel_video_chapters_list.width() - 20,self.options_panel_video_panel.height()-40,30,30)

        if 'nowediting_is_open' in dir(self) and not self.nowediting_is_open:
            self.nowediting_panel.setGeometry(0,-40,self.main_panel.width(),170)
            if self.nowediting == 'menus' and self.selected_menu:
                self.menus_properties_panel.setGeometry(0,self.main_panel.height()-125,self.main_panel.width() - self.options_panel.width() + 10,125)
            elif self.nowediting == 'videos' and self.selected_video:
                self.videos_player_panel.setGeometry(0,self.main_panel.height()-125,self.main_panel.width() - self.options_panel.width() + 10,125)
        else:
            self.nowediting_panel.setGeometry(0,80,self.main_panel.width(),170)
            self.menus_properties_panel.setGeometry(0,self.main_panel.height(),self.main_panel.width() - self.options_panel.width() + 10,125)
            self.videos_player_panel.setGeometry(0,self.main_panel.height(),self.main_panel.width() - self.options_panel.width() + 10,125)

        self.videos_player_timeline.setGeometry(0,25,self.videos_player_panel.width(),100)
        self.menus_properties_panel_background_left.setGeometry(0,0,(self.menus_properties_panel.width()/2)-78,self.menus_properties_panel.height())
        self.menus_properties_panel_background_center.setGeometry((self.menus_properties_panel.width()/2)-78,0,156,self.menus_properties_panel.height())
        self.menus_properties_panel_background_right.setGeometry((self.menus_properties_panel.width()/2)+78,0,(self.menus_properties_panel.width()/2)-78,self.menus_properties_panel.height())
        self.nowediting_dvd_panel.setGeometry(0,0,self.nowediting_panel.width(),self.nowediting_panel.height())
        self.nowediting_dvd_panel_background.setGeometry(0,0,self.nowediting_dvd_panel.width(),self.nowediting_dvd_panel.height())
        self.nowediting_dvd_panel_project_name_label.setGeometry(10,10,(self.nowediting_dvd_panel.width()*.5)-10,20)
        self.nowediting_menus_panel.setGeometry(0,0,self.nowediting_panel.width(),self.nowediting_panel.height())
        self.nowediting_menus_panel_background.setGeometry(0,0,self.nowediting_menus_panel.width(),self.nowediting_menus_panel.height())
        self.nowediting_menus_panel_list.setGeometry(10,10,self.nowediting_menus_panel.width()-60,self.nowediting_menus_panel.height()-70)
        self.nowediting_menus_panel_duplicate.setGeometry(self.nowediting_menus_panel.width()-40,10,30,30)
        self.nowediting_menus_panel_add.setGeometry(self.nowediting_menus_panel.width()-40,40,30,30)
        self.nowediting_menus_panel_remove.setGeometry(self.nowediting_menus_panel.width()-40,80,30,30)
        self.nowediting_videos_panel.setGeometry(0,0,self.nowediting_panel.width(),self.nowediting_panel.height())
        self.nowediting_videos_panel_background.setGeometry(0,0,self.nowediting_videos_panel.width(),self.nowediting_videos_panel.height())
        self.nowediting_videos_panel_list.setGeometry(10,10,self.nowediting_videos_panel.width()-60,self.nowediting_videos_panel.height()-70)
        self.nowediting_videos_panel_add.setGeometry(self.nowediting_videos_panel.width()-40,40,30,30)
        self.nowediting_videos_panel_remove.setGeometry(self.nowediting_videos_panel.width()-40,80,30,30)
        self.top_panel_background.setGeometry(0,0,self.top_panel.width(),self.top_panel.height())
        self.top_panel_project_name_label.setGeometry(20, 0, self.top_panel.width() , 80)

        if 'has_menus' in dir(self) and ((not self.actual_project['has_menus']) or (self.actual_project['has_menus'] and len(self.actual_project['menus']) > 0)) and len(self.actual_project['videos']) > 0:
            self.finalize_panel.setGeometry(self.main_panel.width()-260,0,500,100)
        else:
            self.finalize_panel.setGeometry(self.main_panel.width(),0,500,100)

        self.timer = QTimer(self)
        self.timer.setInterval(60)
        self.timer.timeout.connect(lambda:update_timeline(self))
        self.timer.start()

###################################################################################################
########################################################################################### PROJETO
###################################################################################################

def nowediting_dvd_panel_aspect_ratio_changed(self):
    if self.actual_project['aspect_ratio'] == 0:
        self.actual_project['aspect_ratio'] = 1
    else:
        self.actual_project['aspect_ratio'] = 0

    change_aspect_ratio(self)
    nowediting_dvd_panel_aspect_ratio_update(self)

def nowediting_dvd_panel_aspect_ratio_update(self):
    if self.actual_project['aspect_ratio'] == 0:
        self.nowediting_dvd_panel_aspect_ratio_background.setPixmap(QPixmap(os.path.join(path_graphics, 'nowediting_dvd_panel_aspect_ratio_16_9.png')))
    else:
        self.nowediting_dvd_panel_aspect_ratio_background.setPixmap(QPixmap(os.path.join(path_graphics, 'nowediting_dvd_panel_aspect_ratio_4_3.png')))

def nowediting_dvd_panel_video_format_changed(self):
    if self.actual_project['video_format'] == 0:
        self.actual_project['video_format'] = 1
    else:
        self.actual_project['video_format'] = 0

    nowediting_dvd_panel_video_format_update(self)

def nowediting_dvd_panel_video_format_update(self):
    if self.actual_project['video_format'] == 0:
        self.resolutions = ['720x576','704x576','352x576','352x288']
        self.nowediting_dvd_panel_video_format_background.setPixmap(QPixmap(os.path.join(path_graphics, 'nowediting_dvd_panel_video_format_pal.png')))
    else:
        self.resolutions = ['720x480','704x480','352x480','352x240']
        self.nowediting_dvd_panel_video_format_background.setPixmap(QPixmap(os.path.join(path_graphics, 'nowediting_dvd_panel_video_format_ntsc.png')))
    self.options_panel_video_resolution_combo.clear()
    self.options_panel_video_resolution_combo.addItems(self.resolutions)

def nowediting_dvd_panel_audio_format_changed(self):
    if self.actual_project['audio_format'] == 0:
        self.actual_project['audio_format'] = 1
    else:
        self.actual_project['audio_format'] = 0

    nowediting_dvd_panel_audio_format_update(self)

def nowediting_dvd_panel_audio_format_update(self):
    if self.actual_project['audio_format'] == 0:
        self.nowediting_dvd_panel_audio_format_background.setPixmap(QPixmap(os.path.join(path_graphics, 'nowediting_dvd_panel_audio_format_mp2.png')))
    else:
        self.nowediting_dvd_panel_audio_format_background.setPixmap(QPixmap(os.path.join(path_graphics, 'nowediting_dvd_panel_audio_format_ac3.png')))


def nowediting_dvd_panel_has_menus_changed(self):
    self.actual_project['has_menus'] = not self.actual_project['has_menus']

    nowediting_dvd_panel_has_menus_update(self)

def nowediting_dvd_panel_has_menus_update(self):
    if self.actual_project['has_menus']:
        self.nowediting_dvd_panel_has_menus_background.setPixmap(QPixmap(os.path.join(path_graphics, 'nowediting_dvd_panel_has_menus_yes.png')))
    else:
        self.nowediting_dvd_panel_has_menus_background.setPixmap(QPixmap(os.path.join(path_graphics, 'nowediting_dvd_panel_has_menus_no.png')))
    self.nowediting_menus_panel.setEnabled(self.actual_project['has_menus'])

def nowediting_panel_button_changed(self, nowediting):
    if not nowediting == 'menus':
        clean_menus_list_selection(self)

    if not nowediting == 'videos':
        clean_videos_list_selection(self)

    if not self.nowediting_is_open:
        self.nowediting_is_open = True
        self.generate_effect(self.nowediting_panel_animation, 'geometry', 500, [self.nowediting_panel.x(),self.nowediting_panel.y(),self.nowediting_panel.width(),self.nowediting_panel.height()], [0,80,self.main_panel.width(),170])
        self.generate_effect(self.content_panel_animation, 'geometry', 500, [self.content_panel.x(),self.content_panel.y(),self.content_panel.width(),self.content_panel.height()], [0,80,self.content_panel.width(),self.content_panel.height()])
        self.generate_effect(self.options_panel_animation, 'geometry', 500, [self.options_panel.x(),self.options_panel.y(),self.options_panel.width(),self.options_panel.height()], [self.main_panel.width(),self.options_panel.y(),self.options_panel.width(),self.options_panel.height()])
        self.generate_effect(self.videos_player_panel_animation, 'geometry', 500, [self.videos_player_panel.x(),self.videos_player_panel.y(),self.videos_player_panel.width(),self.videos_player_panel.height()], [self.videos_player_panel.x(),self.main_panel.height(),self.videos_player_panel.width(),self.videos_player_panel.height()])
        self.is_showing_options_panel = False
        #if self.preview_video_obj.state() in [Phonon.PlayingState]:
        if self.preview_video_obj.is_playing():
            video_pause(self)
        self.preview_video_widget.setVisible(False)
        self.preview.setVisible(True)

    else:
        if nowediting == self.nowediting:
            self.nowediting_is_open = False
            self.generate_effect(self.nowediting_panel_animation, 'geometry', 500, [self.nowediting_panel.x(),self.nowediting_panel.y(),self.nowediting_panel.width(),self.nowediting_panel.height()], [0,-40,self.main_panel.width(),170])
            self.generate_effect(self.content_panel_animation, 'geometry', 500, [self.content_panel.x(),self.content_panel.y(),self.content_panel.width(),self.content_panel.height()], [0,-40,self.content_panel.width(),self.content_panel.height()])
            if not self.options_panel.x() == self.main_panel.width() - 380 and (self.nowediting == 'dvd' or (self.nowediting == 'menus' and self.selected_menu) or (self.nowediting == 'videos' and self.selected_video) ):
                self.is_showing_options_panel = True
                self.generate_effect(self.options_panel_animation, 'geometry', 500, [self.options_panel.x(),self.options_panel.y(),self.options_panel.width(),self.options_panel.height()], [self.main_panel.width()-380,self.options_panel.y(),self.options_panel.width(),self.options_panel.height()])

        elif nowediting == 'dvd':
            None

        elif nowediting == 'menus':
            if self.selected_menu:
                self.generate_effect(self.menus_properties_panel_animation, 'geometry', 500, [self.menus_properties_panel.x(),self.menus_properties_panel.y(),self.menus_properties_panel.width(),self.menus_properties_panel.height()], [self.menus_properties_panel.x(),self.main_panel.height() - 125,self.menus_properties_panel.width(),self.menus_properties_panel.height()])
                if not self.options_panel.x() == self.main_panel.width() - 380:
                    self.is_showing_options_panel = True
                    self.generate_effect(self.options_panel_animation, 'geometry', 500, [self.options_panel.x(),self.options_panel.y(),self.options_panel.width(),self.options_panel.height()], [self.main_panel.width()-380,self.options_panel.y(),self.options_panel.width(),self.options_panel.height()])

        elif nowediting == 'videos':
            if self.selected_video:
                self.preview_video_widget.setVisible(True)
                self.preview.setVisible(False)

                if not self.options_panel.x() == self.main_panel.width() - 380:
                    self.is_showing_options_panel = True
                    self.generate_effect(self.options_panel_animation, 'geometry', 500, [self.options_panel.x(),self.options_panel.y(),self.options_panel.width(),self.options_panel.height()], [self.main_panel.width()-380,self.options_panel.y(),self.options_panel.width(),self.options_panel.height()])

    self.nowediting = nowediting

    if self.nowediting == 'dvd':
        self.options_panel_dvd_panel.setVisible(True)
        self.options_panel_menu_panel.setVisible(False)
        self.options_panel_video_panel.setVisible(False)
        self.nowediting_dvd_panel.setVisible(True)
        self.nowediting_panel_dvd_button_background.setVisible(True)
        self.nowediting_menus_panel.setVisible(False)
        self.nowediting_panel_menus_button_background.setVisible(False)
        self.nowediting_videos_panel.setVisible(False)
        self.nowediting_panel_videos_button_background.setVisible(False)
        self.generate_effect(self.videos_player_panel_animation, 'geometry', 500, [self.videos_player_panel.x(),self.videos_player_panel.y(),self.videos_player_panel.width(),self.videos_player_panel.height()], [self.videos_player_panel.x(),self.main_panel.height(),self.videos_player_panel.width(),self.videos_player_panel.height()])
        self.generate_effect(self.menus_properties_panel_animation, 'geometry', 500, [self.menus_properties_panel.x(),self.menus_properties_panel.y(),self.menus_properties_panel.width(),self.menus_properties_panel.height()], [self.menus_properties_panel.x(),self.main_panel.height(),self.menus_properties_panel.width(),self.menus_properties_panel.height()])

    elif self.nowediting == 'menus':
        self.options_panel_dvd_panel.setVisible(False)
        self.options_panel_menu_panel.setVisible(True)
        self.options_panel_video_panel.setVisible(False)
        self.nowediting_dvd_panel.setVisible(False)
        self.nowediting_panel_dvd_button_background.setVisible(False)
        self.nowediting_menus_panel.setVisible(True)
        self.nowediting_panel_menus_button_background.setVisible(True)
        self.nowediting_videos_panel.setVisible(False)
        self.nowediting_panel_videos_button_background.setVisible(False)
        self.generate_effect(self.videos_player_panel_animation, 'geometry', 500, [self.videos_player_panel.x(),self.videos_player_panel.y(),self.videos_player_panel.width(),self.videos_player_panel.height()], [self.videos_player_panel.x(),self.main_panel.height(),self.videos_player_panel.width(),self.videos_player_panel.height()])

    elif self.nowediting == 'videos':
        self.options_panel_dvd_panel.setVisible(False)
        self.options_panel_menu_panel.setVisible(False)
        self.options_panel_video_panel.setVisible(True)
        self.nowediting_dvd_panel.setVisible(False)
        self.nowediting_panel_dvd_button_background.setVisible(False)
        self.nowediting_menus_panel.setVisible(False)
        self.nowediting_panel_menus_button_background.setVisible(False)
        self.nowediting_videos_panel.setVisible(True)
        self.nowediting_panel_videos_button_background.setVisible(True)
        self.generate_effect(self.menus_properties_panel_animation, 'geometry', 500, [self.menus_properties_panel.x(),self.menus_properties_panel.y(),self.menus_properties_panel.width(),self.menus_properties_panel.height()], [self.menus_properties_panel.x(),self.main_panel.height(),self.menus_properties_panel.width(),self.menus_properties_panel.height()])

    update_changes(self)

def clean_changes(self):
    self.selected_menu = None
    self.selected_menu_button = None
    self.selected_menu_button_resizing = False
    self.selected_menu_button_directioning = None
    self.selected_menu_button_directions = [None,None,None,None]
    self.selected_menu_button_preview_difference = [0,0]
    self.selected_video = False
    self.selected_video_chapter = False

    self.actual_project = {}
    self.actual_project['file'] = False
    self.actual_project['name'] = 'Untitled DVD project'
    self.actual_project['menus'] = OrderedDict()
    self.actual_project['videos'] = OrderedDict()
    self.actual_project['video_format'] = 0
    self.actual_project['aspect_ratio'] = 0
    self.actual_project['audio_format'] = 0
    self.actual_project['menu_encoding'] = 'CBR'
    self.actual_project['menu_twopass'] = True
    self.actual_project['menu_bitrate'] = 7500
    self.actual_project['menu_max_bitrate'] = 9000
    self.actual_project['video_encoding'] = 'CBR'
    self.actual_project['video_twopass'] = True
    self.actual_project['video_bitrate'] = 3500
    self.actual_project['video_max_bitrate'] = 6000
    self.actual_project['gop_size'] = 12
    self.actual_project['audio_datarate'] = '384 kb/s'
    self.actual_project['has_menus'] = True

    self.overlay_preview = False
    self.directions_preview = False
    self.nowediting_is_open = True

    nowediting_dvd_panel_video_format_update(self)
    nowediting_dvd_panel_audio_format_update(self)
    nowediting_dvd_panel_aspect_ratio_update(self)
    options_panel_dvd_panel_menu_encoding_update(self)
    options_panel_dvd_panel_video_encoding_update(self)
    options_panel_dvd_panel_menu_twopass_update(self)
    options_panel_dvd_panel_video_twopass_update(self)
    nowediting_dvd_panel_has_menus_update(self)

    self.no_preview_label.setVisible(True)
    self.no_preview_label.setText('<font style="font-size:32px;">Select a menu<br>or a video</font>')

    self.options_panel_menu_buttons_position_box.setEnabled(False)

    self.finalize_panel.generate_button_md5_checkbox.setChecked(False)
    self.finalize_panel.generate_button_ddp_checkbox.setChecked(False)

    self.preview_video_widget.setVisible(False)

    self.nowediting_dvd_panel_project_name.setText(self.actual_project['name'])

    self.nowediting = 'dvd'
    self.nowediting_panel_dvd_button_background.setVisible(False)
    nowediting_panel_button_changed(self, self.nowediting)

    self.options_panel_menu_buttons.clear()
    self.options_panel_video_chapters_list.clear()
    change_aspect_ratio(self)
    main_tabs_changed(self)
    update_changes(self)
    self.really_clean_project = True

def open_project_file(self):
    canceled = False
    if not self.really_clean_project:
        save_message = QMessageBox()
        save_message.setText("Would you like to save the project?.")
        save_message.setInformativeText("You nave made some changes in your project.")
        save_message.setStandardButtons(QMessageBox.Save | QMessageBox.Discard | QMessageBox.Cancel)
        save_message.setDefaultButton(QMessageBox.Save)
        ret = save_message.exec_()
        if ret == QMessageBox.Save:
            save_project_file(self)
        if ret == QMessageBox.Discard or ret == QMessageBox.Save:
            clean_changes(self)
        elif ret == QMessageBox.Cancel:
            canceled = True

    if not canceled:
        filepath = QFileDialog.getOpenFileName(self, 'Select the Open DVD Producer project file', path_home, 'Open DVD Producer files (*.odvdp)')[0]#.toUtf8()
        if filepath:
            self.actual_project = file_io.read_project_file(filepath)

            self.nowediting_dvd_panel_project_name.setText(self.actual_project['name'])

            change_aspect_ratio(self)
            populate_menus_list(self)
            populate_videos_list(self)
            nowediting_dvd_panel_video_format_update(self)
            nowediting_dvd_panel_audio_format_update(self)
            nowediting_dvd_panel_aspect_ratio_update(self)
            options_panel_dvd_panel_menu_encoding_update(self)
            options_panel_dvd_panel_video_encoding_update(self)
            options_panel_dvd_panel_menu_twopass_update(self)
            options_panel_dvd_panel_video_twopass_update(self)
            nowediting_dvd_panel_has_menus_update(self)
            update_changes(self)

            self.really_clean_project = True

def save_project_file(self):
    if not self.actual_project['file']:
        self.actual_project['file'] = QFileDialog.getSaveFileName(self, 'Select a filename to save', path_home, 'Open DVD Producer files (*.odvdp)')[0]

    if self.actual_project['file']:
        if not self.actual_project['file'].endswith('.odvdp'):
            self.actual_project['file'] = self.actual_project['file'] + '.odvdp'
        codecs.open(os.path.join(self.actual_project['file']), 'w', 'utf-8').write(file_io.write_project_file(self.actual_project))

    update_changes(self)

    self.really_clean_project = True

def new_project_file(self):
    clean_changes(self)

def get_preview_file(self, path):
    final_path = path
    if not os.path.isfile(path):
        if path.split('.')[-1] in ['mov', 'm4v', 'mpg', 'm2v', 'mp4', 'mkv']:
            final_path = os.path.join(path_graphics, 'file_not_found.mkv')
        else:
            final_path = os.path.join(path_graphics, 'file_not_found.png')
    return final_path

def options_panel_dvd_panel_bitrates_changed(self):
    self.actual_project['menu_bitrate'] = self.options_panel_dvd_panel_menu_bitrate_field.value()
    self.actual_project['menu_max_bitrate'] = self.options_panel_dvd_panel_menu_max_bitrate_field.value()
    self.actual_project['video_bitrate'] = self.options_panel_dvd_panel_video_bitrate_field.value()
    self.actual_project['video_max_bitrate'] = self.options_panel_dvd_panel_video_max_bitrate_field.value()
    update_changes(self)

def options_panel_dvd_panel_gop_changed(self):
    self.really_clean_project = False
    self.actual_project['gop_size'] = int(self.options_panel_dvd_panel_gop.currentText())
    update_changes(self)

def options_panel_dvd_panel_audio_datarate_changed(self):
    self.really_clean_project = False
    self.actual_project['audio_datarate'] = self.options_panel_dvd_panel_audio_datarate.currentText()
    update_changes(self)

def options_panel_dvd_panel_menu_encoding_changed(self):
    if self.actual_project['menu_encoding'] == 'CBR':
        self.actual_project['menu_encoding'] = 'VBR'
    else:
        self.actual_project['menu_encoding'] = 'CBR'
    options_panel_dvd_panel_menu_encoding_update(self)

def options_panel_dvd_panel_menu_encoding_update(self):
    if self.actual_project['menu_encoding'] == 'CBR':
        self.options_panel_dvd_panel_menu_encoding_background.setPixmap(QPixmap(os.path.join(path_graphics, 'options_panel_dvd_encoding_cbr.png')))
        self.options_panel_dvd_panel_menu_twopass_label.setVisible(False)
        self.options_panel_dvd_panel_menu_twopass.setVisible(False)
        self.options_panel_dvd_panel_menu_max_bitrate_label.setVisible(False)
        self.options_panel_dvd_panel_menu_max_bitrate_field.setVisible(False)
        self.options_panel_dvd_panel_menu_max_bitrate_field_label.setVisible(False)
    else:
        self.options_panel_dvd_panel_menu_encoding_background.setPixmap(QPixmap(os.path.join(path_graphics, 'options_panel_dvd_encoding_vbr.png')))
        self.options_panel_dvd_panel_menu_twopass_label.setVisible(True)
        self.options_panel_dvd_panel_menu_twopass.setVisible(True)
        self.options_panel_dvd_panel_menu_max_bitrate_label.setVisible(True)
        self.options_panel_dvd_panel_menu_max_bitrate_field.setVisible(True)
        self.options_panel_dvd_panel_menu_max_bitrate_field_label.setVisible(True)

def options_panel_dvd_panel_video_encoding_changed(self):
    if self.actual_project['video_encoding'] == 'CBR':
        self.actual_project['video_encoding'] = 'VBR'
    else:
        self.actual_project['video_encoding'] = 'CBR'
    options_panel_dvd_panel_video_encoding_update(self)

def options_panel_dvd_panel_video_encoding_update(self):
    if self.actual_project['video_encoding'] == 'CBR':
        self.options_panel_dvd_panel_video_encoding_background.setPixmap(QPixmap(os.path.join(path_graphics, 'options_panel_dvd_encoding_cbr.png')))
        self.options_panel_dvd_panel_video_twopass_label.setVisible(False)
        self.options_panel_dvd_panel_video_twopass.setVisible(False)
        self.options_panel_dvd_panel_video_max_bitrate_label.setVisible(False)
        self.options_panel_dvd_panel_video_max_bitrate_field.setVisible(False)
        self.options_panel_dvd_panel_video_max_bitrate_field_label.setVisible(False)
    else:
        self.options_panel_dvd_panel_video_encoding_background.setPixmap(QPixmap(os.path.join(path_graphics, 'options_panel_dvd_encoding_vbr.png')))
        self.options_panel_dvd_panel_video_twopass_label.setVisible(True)
        self.options_panel_dvd_panel_video_twopass.setVisible(True)
        self.options_panel_dvd_panel_video_max_bitrate_label.setVisible(True)
        self.options_panel_dvd_panel_video_max_bitrate_field.setVisible(True)
        self.options_panel_dvd_panel_video_max_bitrate_field_label.setVisible(True)

def options_panel_dvd_panel_menu_twopass_changed(self):
    self.actual_project['menu_twopass'] = not self.actual_project['menu_twopass']
    options_panel_dvd_panel_menu_twopass_update(self)

def options_panel_dvd_panel_menu_twopass_update(self):
    if self.actual_project['menu_twopass']:
        self.options_panel_dvd_panel_menu_twopass_background.setPixmap(QPixmap(os.path.join(path_graphics, 'options_panel_dvd_passes_two.png')))
    else:
        self.options_panel_dvd_panel_menu_twopass_background.setPixmap(QPixmap(os.path.join(path_graphics, 'options_panel_dvd_passes_one.png')))

def options_panel_dvd_panel_video_twopass_changed(self):
    self.actual_project['video_twopass'] = not self.actual_project['video_twopass']
    options_panel_dvd_panel_video_twopass_update(self)

def options_panel_dvd_panel_video_twopass_update(self):
    if self.actual_project['video_twopass']:
        self.options_panel_dvd_panel_video_twopass_background.setPixmap(QPixmap(os.path.join(path_graphics, 'options_panel_dvd_passes_two.png')))
    else:
        self.options_panel_dvd_panel_video_twopass_background.setPixmap(QPixmap(os.path.join(path_graphics, 'options_panel_dvd_passes_one.png')))

def update_changes(self):
    if self.options_panel_menu_buttons_edit_field.isVisible():
        edit_cancel_menu_button(self)

    self.actual_project['name'] = self.nowediting_dvd_panel_project_name.text()

    if self.actual_project['file']:
            self.top_panel_project_name_label.setText('<font style="font-size:18px; font-weight:200;">' + self.actual_project['name'] + '</font><br><font style="font-size:14px;">' + self.actual_project['file'] + '</font>')
    else:
        self.top_panel_project_name_label.setText('<font style="font-size:18px; font-weight:200;">' + self.actual_project['name'] + '</font><br><font style="font-size:14px;">Not saved</font>')

    if self.selected_menu:
        self.no_preview_label.setVisible(False)

        self.menus_properties_panel_transparency_slider_value.setText(str(int(self.actual_project['menus'][self.selected_menu][8]*100)) + '%')
        self.menus_properties_panel_border_slider_value.setText(str(int(self.actual_project['menus'][self.selected_menu][9]*100)) + '%')

        self.menus_properties_panel_transparency_slider.setValue(int(self.actual_project['menus'][self.selected_menu][8]*100))
        self.menus_properties_panel_border_slider.setValue(int(self.actual_project['menus'][self.selected_menu][9]*100))

        self.menus_properties_panel_background_file_preview_background.setPixmap(QPixmap(os.path.join(path_tmp, self.selected_menu + '.preview.png')))

        if self.actual_project['menus'][self.selected_menu][3]:
            self.menus_properties_panel_overlay_file_preview_background.setPixmap(QPixmap(self.actual_project['menus'][self.selected_menu][3]))
        else:
            self.menus_properties_panel_overlay_file_preview_background.setPixmap(QPixmap(None))

        if self.actual_project['menus'][self.selected_menu][4]:
            self.options_panel_menu_choose_color_button.setStyleSheet('background-color:' + self.actual_project['menus'][self.selected_menu][4])
        else:
            self.options_panel_menu_choose_color_button.setStyleSheet('background-color:white')

        if self.actual_project['menus'][self.selected_menu][0].split('.')[-1] == 'png':
            self.menus_properties_panel_sound_box.setVisible(True)
            self.menus_properties_panel_sound_label.setVisible(True)
            self.menus_properties_panel_sound_open_button.setVisible(True)

            if self.actual_project['menus'][self.selected_menu][5]:
                self.menus_properties_panel_sound_label.setText('<small>' + self.actual_project['menus'][self.selected_menu][5].split('/')[-1].split('\\')[-1] + '</small>')
            else:
                self.menus_properties_panel_sound_label.setText('<small>' + 'No selected audio' + '</small>')
        else:
            self.menus_properties_panel_sound_box.setVisible(False)
            self.menus_properties_panel_sound_label.setVisible(False)
            self.menus_properties_panel_sound_open_button.setVisible(False)

        if self.actual_project['menus'][self.selected_menu][6]:
            self.menus_properties_panel_main_menu_checkbox_background.setPixmap(QPixmap(os.path.join(path_graphics, 'menus_properties_panel_main_menu_checkbox_checked.png')))
        else:
            self.menus_properties_panel_main_menu_checkbox_background.setPixmap(QPixmap(os.path.join(path_graphics, 'menus_properties_panel_main_menu_checkbox_unchecked.png')))

        if self.overlay_preview:
            self.menus_properties_panel_overlay_preview_background.setPixmap(QPixmap(os.path.join(path_graphics, 'menus_properties_panel_preview_checked.png')))
        else:
            self.menus_properties_panel_overlay_preview_background.setPixmap(QPixmap(os.path.join(path_graphics, 'menus_properties_panel_preview_unchecked.png')))

        if self.directions_preview:
            self.menus_properties_panel_directions_preview_background.setPixmap(QPixmap(os.path.join(path_graphics, 'menus_properties_panel_preview_checked.png')))
        else:
            self.menus_properties_panel_directions_preview_background.setPixmap(QPixmap(os.path.join(path_graphics, 'menus_properties_panel_preview_unchecked.png')))

        if self.selected_menu_button:
            self.options_panel_menu_buttons_x_position.setValue(self.actual_project['menus'][self.selected_menu][2][self.selected_menu_button][0])
            self.options_panel_menu_buttons_y_position.setValue(self.actual_project['menus'][self.selected_menu][2][self.selected_menu_button][1])
            self.options_panel_menu_buttons_width.setValue(self.actual_project['menus'][self.selected_menu][2][self.selected_menu_button][2])
            self.options_panel_menu_buttons_height.setValue(self.actual_project['menus'][self.selected_menu][2][self.selected_menu_button][3])

            populate_jumpto(self)

            if self.actual_project['menus'][self.selected_menu][2][self.selected_menu_button][4]:
                self.options_panel_menu_buttons_jumpto.setCurrentIndex(self.options_panel_menu_buttons_jumpto.findText(self.actual_project['menus'][self.selected_menu][2][self.selected_menu_button][4]))
            else:
                self.options_panel_menu_buttons_jumpto.setCurrentIndex(-1)

            populate_button_directions_list(self)

            if self.actual_project['menus'][self.selected_menu][2][self.selected_menu_button][5][0]:
                self.options_panel_menu_buttons_directions_top.setCurrentIndex(self.options_panel_menu_buttons_directions_top.findText(self.actual_project['menus'][self.selected_menu][2][self.selected_menu_button][5][0]))
            else:
                self.options_panel_menu_buttons_directions_top.setCurrentIndex(0)

            if self.actual_project['menus'][self.selected_menu][2][self.selected_menu_button][5][1]:
                self.options_panel_menu_buttons_directions_right.setCurrentIndex(self.options_panel_menu_buttons_directions_right.findText(self.actual_project['menus'][self.selected_menu][2][self.selected_menu_button][5][1]))
            else:
                self.options_panel_menu_buttons_directions_right.setCurrentIndex(0)

            if self.actual_project['menus'][self.selected_menu][2][self.selected_menu_button][5][2]:
                self.options_panel_menu_buttons_directions_bottom.setCurrentIndex(self.options_panel_menu_buttons_directions_bottom.findText(self.actual_project['menus'][self.selected_menu][2][self.selected_menu_button][5][2]))
            else:
                self.options_panel_menu_buttons_directions_bottom.setCurrentIndex(0)

            if self.actual_project['menus'][self.selected_menu][2][self.selected_menu_button][5][3]:
                self.options_panel_menu_buttons_directions_left.setCurrentIndex(self.options_panel_menu_buttons_directions_left.findText(self.actual_project['menus'][self.selected_menu][2][self.selected_menu_button][5][3]))
            else:
                self.options_panel_menu_buttons_directions_left.setCurrentIndex(0)

        if self.nowediting == 'menus':
            if self.nowediting_is_open:
                self.generate_effect(self.menus_properties_panel_animation, 'geometry', 500, [self.menus_properties_panel.x(),self.menus_properties_panel.y(),self.menus_properties_panel.width(),self.menus_properties_panel.height()], [self.menus_properties_panel.x(),self.main_panel.height(),self.menus_properties_panel.width(),self.menus_properties_panel.height()])
            else:
                self.is_showing_options_panel = True
                self.generate_effect(self.menus_properties_panel_animation, 'geometry', 500, [self.menus_properties_panel.x(),self.menus_properties_panel.y(),self.menus_properties_panel.width(),self.menus_properties_panel.height()], [self.menus_properties_panel.x(),self.main_panel.height()-125,self.menus_properties_panel.width(),self.menus_properties_panel.height()])
                self.generate_effect(self.options_panel_animation, 'geometry', 500, [self.options_panel.x(),self.options_panel.y(),self.options_panel.width(),self.options_panel.height()], [self.main_panel.width()-380,self.options_panel.y(),self.options_panel.width(),self.options_panel.height()])

        if len(self.actual_project['menus']) == 1:
            self.menus_properties_panel_main_menu_checkbox.setVisible(False)
        else:
            self.menus_properties_panel_main_menu_checkbox.setVisible(True)

    if self.selected_video:
        populate_chapters_list(self)
        if not self.selected_video_chapter:
            self.options_panel_video_chapters_remove.setEnabled(False)
            self.options_panel_video_chapters_edit.setEnabled(False)

        if self.nowediting == 'videos' and self.videos_player_panel.y() == self.main_panel.height():
            self.generate_effect(self.videos_player_panel_animation, 'geometry', 500, [self.videos_player_panel.x(),self.videos_player_panel.y(),self.videos_player_panel.width(),self.videos_player_panel.height()], [self.videos_player_panel.x(),self.main_panel.height()-125,self.videos_player_panel.width(),self.videos_player_panel.height()])
        populate_jumpto_list(self)
        if self.actual_project['videos'][self.selected_video][8]:
            self.options_panel_video_jumpto.setCurrentIndex(self.options_panel_video_jumpto.findText(self.actual_project['videos'][self.selected_video][8]))
        else:
            self.options_panel_video_jumpto.setCurrentIndex(self.options_panel_video_jumpto.findText('Main menu'))

        self.options_panel_video_intro_video_checkbox.setChecked(self.actual_project['videos'][self.selected_video][3])
        self.options_panel_video_reencode_video_checkbox.setChecked(self.actual_project['videos'][self.selected_video][4])
        self.options_panel_video_resolution_combo.setEnabled(self.actual_project['videos'][self.selected_video][4])
        self.options_panel_video_resolution_combo.setCurrentIndex(self.actual_project['videos'][self.selected_video][9])

        update_timeline(self)

    if ((not self.actual_project['has_menus']) or (self.actual_project['has_menus'] and len(self.actual_project['menus']) > 0)) and len(self.actual_project['videos']) > 0:
        self.generate_effect(self.finalize_panel.animation, 'geometry', 500, [self.finalize_panel.x(),self.finalize_panel.y(),self.finalize_panel.width(),self.finalize_panel.height()], [self.main_panel.width() - 260,self.finalize_panel.y(),self.finalize_panel.width(),self.finalize_panel.height()])
    else:
        self.generate_effect(self.finalize_panel.animation, 'geometry', 500, [self.finalize_panel.x(),self.finalize_panel.y(),self.finalize_panel.width(),self.finalize_panel.height()], [self.main_panel.width(),self.finalize_panel.y(),self.finalize_panel.width(),self.finalize_panel.height()])

    self.options_panel_dvd_panel_dvd_image.update()

    estimated_size = 0.0
    estimated_proportion = 0

    if len(self.actual_project['videos']) > 0:
        if self.actual_project['has_menus'] and len(self.actual_project['menus']) > 0:
            for menu in self.actual_project['menus']:
                estimated_size += float(((self.actual_project['menu_bitrate'] + int(self.actual_project['audio_datarate'].split(' ')[0]))*.001) * self.actual_project['menus'][menu][10])
        for video in self.actual_project['videos']:
            final_length = self.actual_project['videos'][video][5]
            if self.actual_project['videos'][video][6]:
                final_length -= self.actual_project['videos'][video][6]
            if self.actual_project['videos'][video][7]:
                final_length -= (self.actual_project['videos'][video][5] - self.actual_project['videos'][video][7])
            estimated_size += float(((self.actual_project['video_bitrate'] + int(self.actual_project['audio_datarate'].split(' ')[0]))*.001) * final_length)
        estimated_proportion = int(estimated_size / 360)

    final_text = '<font style="font-size:12px;"><b>ESTIMATED SIZE:</b></font><br><font style="font-size:18px;"><b>'
    if estimated_size*.000125 < 1.0:
        final_text += str("%.2f" % round(estimated_size*.125,2)) + ' MB'
    else:
        final_text += str("%.2f" % round(estimated_size/8590.0,2)) + ' GB'
    final_text += ' (' + str(estimated_proportion) + '%)</b></font>'

    self.options_panel_dvd_panel_size_info.setText(final_text)

    self.options_panel_dvd_panel_menu_bitrate_field.setValue(self.actual_project['menu_bitrate'])
    self.options_panel_dvd_panel_menu_max_bitrate_field.setValue(self.actual_project['menu_max_bitrate'])
    self.options_panel_dvd_panel_video_bitrate_field.setValue(self.actual_project['video_bitrate'])
    self.options_panel_dvd_panel_video_max_bitrate_field.setValue(self.actual_project['video_max_bitrate'])
    self.options_panel_dvd_panel_gop.setCurrentIndex(self.options_panel_dvd_panel_gop.findText(str(self.actual_project['gop_size'])))
    self.options_panel_dvd_panel_audio_datarate.setCurrentIndex(self.options_panel_dvd_panel_audio_datarate.findText(self.actual_project['audio_datarate']))

    self.preview.update()

def main_tabs_changed(self):
    if self.nowediting == 'menus':
        self.options_panel_menu_panel_0.setVisible(False)
        self.options_panel_menu_buttons_panel_0.setVisible(False)
        self.options_panel_video_panel.setVisible(True)
        self.options_panel_video_chapters_panel_0.setVisible(True)
        self.preview_video_widget.setVisible(False)
        self.preview_video_play_button.setVisible(False)
        self.preview_video_pause_button.setVisible(False)
        self.preview_video_stop_button.setVisible(False)
        self.preview_video_seek_back_frame_button.setVisible(False)
        self.preview_video_seek_next_frame_button.setVisible(False)
        self.preview_video_add_this_mark_button.setVisible(False)
        self.menus_properties_panel_overlay_preview_button.setVisible(True)
        clean_menus_list_selection(self)
        self.selected_video = False
    elif self.nowediting == 'videos':
        self.options_panel_menu_panel_0.setVisible(True)
        self.options_panel_menu_buttons_panel_0.setVisible(True)
        self.options_panel_video_panel.setVisible(False)
        self.options_panel_video_chapters_panel_0.setVisible(False)
        self.preview_video_play_button.setVisible(True)
        self.preview_video_pause_button.setVisible(True)
        self.preview_video_stop_button.setVisible(True)
        self.preview_video_seek_back_frame_button.setVisible(True)
        self.preview_video_seek_next_frame_button.setVisible(True)
        self.preview_video_add_this_mark_button.setVisible(True)
        self.menus_properties_panel_overlay_preview_button.setVisible(False)
        clean_videos_list_selection(self)
        if self.selected_video:
            #if self.preview_video_obj.state() == Phonon.PlayingState:
            if self.preview_video_obj.is_playing():
                self.preview_video_obj.stop()
            self.preview_video_widget.setVisible(True)
        self.selected_menu = None

def change_aspect_ratio(self):
    for menu in self.actual_project['menus']:
        generate_preview_image(self, menu, self.actual_project['menus'])

    for video in self.actual_project['videos']:
        generate_preview_image(self, video, self.actual_project['videos'])

    if self.actual_project['aspect_ratio'] == 0:
        self.preview.setGeometry(50, 220, 720, 405)
        self.preview_video_widget.setGeometry(50, 180, 720, 405)
        self.nowediting_menus_panel_list.setIconSize(QSize(100, 56))
        self.nowediting_videos_panel_list.setIconSize(QSize(100, 56))
        self.options_panel_menu_buttons_x_position.setMaximum(720)
        self.options_panel_menu_buttons_width.setMaximum(720)
        self.menus_properties_panel_background_file_preview.setGeometry(10,50,89,50)
        self.menus_properties_panel_background_file_preview_background.setGeometry(0,0,self.menus_properties_panel_background_file_preview.width(),self.menus_properties_panel_background_file_preview.height())
        self.menus_properties_panel_background_file_preview_foreground.setGeometry(0,0,self.menus_properties_panel_background_file_preview.width(),self.menus_properties_panel_background_file_preview.height())
        self.menus_properties_panel_background_file_preview_foreground.setPixmap(QPixmap(os.path.join(path_graphics, 'menus_properties_panel_background_file_preview_foreground_16_9.png')))
        self.menus_properties_panel_overlay_file_preview.setGeometry(110,50,89,50)
        self.menus_properties_panel_overlay_file_preview_background.setGeometry(0,0,self.menus_properties_panel_overlay_file_preview.width(),self.menus_properties_panel_overlay_file_preview.height())
        self.menus_properties_panel_overlay_file_preview_foreground.setGeometry(0,0,self.menus_properties_panel_overlay_file_preview.width(),self.menus_properties_panel_overlay_file_preview.height())
        self.menus_properties_panel_overlay_file_preview_foreground.setPixmap(QPixmap(os.path.join(path_graphics, 'menus_properties_panel_background_file_preview_foreground_16_9.png')))
    elif self.actual_project['aspect_ratio'] == 1:
        self.preview.setGeometry(90, 220, 640, 480)
        self.preview_video_widget.setGeometry(90, 180, 640, 480)
        self.nowediting_menus_panel_list.setIconSize(QSize(88, 66))
        self.nowediting_videos_panel_list.setIconSize(QSize(88, 66))
        self.options_panel_menu_buttons_x_position.setMaximum(640)
        self.options_panel_menu_buttons_width.setMaximum(640)
        self.menus_properties_panel_background_file_preview.setGeometry(10,50,67,50)
        self.menus_properties_panel_background_file_preview_background.setGeometry(0,0,self.menus_properties_panel_background_file_preview.width(),self.menus_properties_panel_background_file_preview.height())
        self.menus_properties_panel_background_file_preview_foreground.setGeometry(0,0,self.menus_properties_panel_background_file_preview.width(),self.menus_properties_panel_background_file_preview.height())
        self.menus_properties_panel_background_file_preview_foreground.setPixmap(QPixmap(os.path.join(path_graphics, 'menus_properties_panel_background_file_preview_foreground_4_3.png')))
        self.menus_properties_panel_overlay_file_preview.setGeometry(110,50,67,50)
        self.menus_properties_panel_overlay_file_preview_background.setGeometry(0,0,self.menus_properties_panel_overlay_file_preview.width(),self.menus_properties_panel_overlay_file_preview.height())
        self.menus_properties_panel_overlay_file_preview_foreground.setGeometry(0,0,self.menus_properties_panel_overlay_file_preview.width(),self.menus_properties_panel_overlay_file_preview.height())
        self.menus_properties_panel_overlay_file_preview_foreground.setPixmap(QPixmap(os.path.join(path_graphics, 'menus_properties_panel_background_file_preview_foreground_4_3.png')))

    self.options_panel_menu_buttons_y_position.setMaximum(int(self.video_formats[self.actual_project['video_format']].split(' ')[1].split('x')[1]))
    self.options_panel_menu_buttons_height.setMaximum(int(self.video_formats[self.actual_project['video_format']].split(' ')[1].split('x')[1]))

    self.no_preview_label.setGeometry(0,0,self.preview.width(),self.preview.height())

    if self.selected_menu:
        update_overlay_image_preview(self)

    populate_menus_list(self)
    populate_videos_list(self)

    update_changes(self)

def convert_to_timecode(value):#, framerate):
    #value = value.replace('s', '')
    #framerate = framerate.replace('s','')

    #if '/' in framerate:
    #    framerate = float(framerate.split('/')[1])/float(framerate.split('/')[0])
    #else:
    #    framerate = float(framerate)

    #if '/' in value:
    #    value = float(value.split('/')[0])/float(value.split('/')[1])
    #else:
    #    value = float(value)
    value = float(value.replace('s', ''))

    #value = value/framerate

    fr = int(str('%03d' % int(str(value).split('.')[1]))[:3])
    mm, ss = divmod(value, 60)
    hh, mm = divmod(mm, 60)

    return '%02d:%02d:%02d.%03d' % (hh, mm, ss, fr)

def convert_timecode_to_seconds(timecode):
    final_value = 0.0
    final_value += float(timecode.split(':')[2].split('.')[0])
    final_value += float(timecode.split(':')[1])*60
    final_value += float(timecode.split(':')[0])*3600
    final_value += float('0.' + timecode.split('.')[-1])
    return final_value

def sort_list_of_chapters(dict_of_chapters):

    dict_of_chapters_for_order = {}
    for name in dict_of_chapters.keys():
        dict_of_chapters_for_order[dict_of_chapters[name]] = name
    list_of_chapters_in_order = sorted(dict_of_chapters_for_order.keys())

    new_list = []
    for timecode in list_of_chapters_in_order:
        new_list.append(dict_of_chapters_for_order[timecode])

    return new_list


###################################################################################################
############################################################################################# MENUS
###################################################################################################

def set_main_menu(self):
    self.actual_project['menus'][self.selected_menu][6] = not self.actual_project['menus'][self.selected_menu][6]

    for menu in self.actual_project['menus']:
        if not menu == self.selected_menu:
            self.actual_project['menus'][menu][6] = False

    update_changes(self)

def choose_color(self):
    color = QColorDialog().getColor()
    if color.isValid():
        self.actual_project['menus'][self.selected_menu][4] = color.name()
    preview_overlay(self)
    update_changes(self)


def generate_preview_image(self, image_item, image_dict):
    if image_dict[image_item][0].split('.')[-1] in ['mov', 'm4v', 'mpg', 'm2v', 'mp4', 'mkv']:
        subprocess.call([ffmpeg_bin, '-loglevel', 'error', '-y', '-ss', '00:03.00', '-i', get_preview_file(self, image_dict[image_item][0]), '-frames:v', '1', os.path.join(path_tmp, image_item + '.preview_.png')])
        image_path = os.path.join(path_tmp, image_item + '.preview_.png')
    else:
        image_path = get_preview_file(self, image_dict[image_item][0])

    if self.actual_project['aspect_ratio'] == 0:
        size = '720x405!'
    elif self.actual_project['aspect_ratio'] == 1:
        size = '640x480!'
    subprocess.call([imagemagick_convert_bin, get_preview_file(self, image_path), '-resize', size, os.path.join(path_tmp, image_item + '.preview.png')])

def menu_selected(self):
    if self.nowediting_menus_panel_list.currentItem():
        self.nowediting_menus_panel_remove.setEnabled(True)
        self.nowediting_menus_panel_duplicate.setEnabled(True)
        self.selected_menu = self.nowediting_menus_panel_list.currentItem().text().split('\n')[0]
    else:
        self.nowediting_menus_panel_remove.setEnabled(False)
        self.nowediting_menus_panel_duplicate.setEnabled(False)

    self.selected_menu_button = None
    self.options_panel_menu_buttons_position_box.setEnabled(False)
    self.options_panel_menu_buttons.setCurrentItem(None)

    self.preview_video_widget.setVisible(False)

    nowediting_panel_button_changed(self, self.nowediting)
    populate_menu_buttons_list(self)
    clean_buttons_selection(self)
    populate_jumpto_menus(self)
    update_changes(self)

def duplicate_menu(self):
    new_menu_name = check_name(self, self.actual_project['menus'], self.selected_menu)
    new_menu = copy.deepcopy(self.actual_project['menus'][self.selected_menu])

    self.actual_project['menus'][new_menu_name] = new_menu

    generate_preview_image(self, new_menu_name, self.actual_project['menus'])

    self.selected_menu_button = None
    clean_menus_list_selection(self)

    self.selected_menu = list(self.actual_project['menus'])[-1]
    update_changes(self)
    populate_menus_list(self)

    nowediting_panel_button_changed(self, self.nowediting)

def add_menu(self):
    image_path_list = QFileDialog.getOpenFileNames(self, 'Select the image or video for menu', path_home, 'PNG, JPEG images or MPEG videos (*.jpg *.png *.m4v *.m2v *.mpg *.mp4 *.mov *.mkv)')[0]#.toUtf8()

    for image_path_file in image_path_list:
        image_path = os.path.abspath(image_path_file)
        if not image_path == '':
            menu_name = check_name(self, self.actual_project['menus'], image_path.split('/')[-1].split('\\')[-1].split('.')[0])

            if len(self.actual_project['menus']) > 0:
                mainmenu = False
            else:
                mainmenu = True
            length = 60.0
            if image_path.endswith('.jpg') or image_path.endswith('.png') or image_path.endswith('.jpeg'):
                length = 60.0
            else:
                length_xml = subprocess.Popen([ffprobe_bin, '-loglevel', 'error',  '-show_format', '-print_format', 'xml', image_path], stdin=subprocess.PIPE, stdout=subprocess.PIPE, stderr=subprocess.PIPE).stdout.read().decode()
                length = float(length_xml.split(' duration="')[1].split('"')[0])

            self.actual_project['menus'][menu_name] = [   image_path,                             # [0] Arquivo do fundo, imagem ou video
                                                [],                                     # [1] Lista de botoes
                                                {},                                     # [2] Dicionario de botoes
                                                None,                                   # [3] Arquivo de overlay (str)
                                                None,                                   # [4] Cor do Overlay (str)
                                                None,                                   # [5] Arquivo de som (str)
                                                mainmenu,                               # [6] Se é o menu principal
                                                False,                                  # [7] If jump to at end
                                                .5,                                     # [8] Transparencia
                                                .5,                                     # [9] Borda dura
                                                length                                  # [10] Duracao em segundos
                                            ]

            generate_preview_image(self, menu_name, self.actual_project['menus'])

    self.selected_menu_button = None
    clean_menus_list_selection(self)

    self.selected_menu = list(self.actual_project['menus'])[-1]
    update_changes(self)
    populate_menus_list(self)

    nowediting_panel_button_changed(self, self.nowediting)

def check_name(self, names_list, name):
    final_name = name
    counter = 1
    while final_name in names_list:
         final_name = name + '_' + str(counter)
         counter += 1
    else:
        return final_name

def populate_menus_list(self):
    self.nowediting_menus_panel_list.clear()
    for menu in self.actual_project['menus']:
        icon = QIcon(os.path.join(path_tmp, menu + '.preview.png').replace('\\', '/'))
        self.nowediting_menus_panel_list.addItem(QListWidgetItem(icon, menu))

def select_menu_file(self):
    image_path = QFileDialog.getOpenFileName(self, 'Select the image or video for menu', path_home, 'PNG, JPEG images or MPEG videos (*.jpg *.png *.m4v *.m2v *.mpg *.mp4 *.mov *.mkv)')[0]#.toUtf8()
    if not image_path == '':
        self.actual_project['menus'][self.selected_menu][0] = image_path

    generate_preview_image(self, self.selected_menu, self.actual_project['menus'])
    populate_menus_list(self)
    update_changes(self)

def remove_menu_file(self):
    self.actual_project['menus'][self.selected_menu][0] = ''
    generate_preview_image(self, self.selected_menu, self.actual_project['menus'])
    update_changes(self)

def select_overlay_file(self):
    overlay_image_path = QFileDialog.getOpenFileName(self, "Select an image for the overlay", path_home, "Images (*.jpg *.png)")[0]#.toUtf8()
    if not overlay_image_path == '':
        self.actual_project['menus'][self.selected_menu][3] = overlay_image_path
    update_overlay_image_preview(self)

def remove_overlay_file(self):
    self.actual_project['menus'][self.selected_menu][3] = None
    update_overlay_image_preview(self)

def update_overlay_image_preview(self):
    preview_overlay(self)
    update_changes(self)

def preview_overlay_clicked(self):
    self.overlay_preview = not self.overlay_preview
    preview_overlay(self)

def preview_overlay(self):
    if self.actual_project['aspect_ratio'] == 0:
        size = '720x405!'
    elif self.actual_project['aspect_ratio'] == 1:
        size = '640x480!'

    for menu in self.actual_project['menus']:
        if self.actual_project['menus'][menu][3]:
            menu_color = '#FFFFFF'
            if self.actual_project['menus'][menu][4]:
                menu_color = self.actual_project['menus'][menu][4]
            subprocess.call([imagemagick_convert_bin, get_preview_file(self, self.actual_project['menus'][menu][3]), '-resize', size, '+antialias', '-threshold', str(int(self.actual_project['menus'][menu][9]*100)) + '%', '-flatten', os.path.join(path_tmp, self.actual_project['menus'][menu][3].split('/')[-1].split('\\')[-1][:-4] + '_hl.preview.png')])
            subprocess.call([imagemagick_convert_bin, os.path.join(path_tmp, self.actual_project['menus'][menu][3].split('/')[-1].split('\\')[-1][:-4] + '_hl.preview.png'), '-threshold', str(int(self.actual_project['menus'][menu][9]*100)) + '%', '-transparent', 'white', '-channel', 'RGBA', '-fill', menu_color + str('%02x' % int(self.actual_project['menus'][menu][8]*255)), '-opaque', 'black', os.path.join(path_tmp, self.actual_project['menus'][menu][3].split('/')[-1].split('\\')[-1][:-4] + '_hl.preview.png')])

    update_changes(self)

def preview_directions(self):
    self.directions_preview = not self.directions_preview
    update_changes(self)

def select_menu_sound_file(self):
    menu_sound_path = QFileDialog.getOpenFileName(self, "Select an audio file", path_home, "Audio file (*.ac3 *.flac)")[0]
    if not menu_sound_path == '':
        self.actual_project['menus'][self.selected_menu][5] = menu_sound_path
        self.menus_properties_panel_sound_label.setText(menu_sound_path.split('/')[-1].split('\\')[-1])

def remove_menu(self):
    del self.actual_project['menus'][self.selected_menu]
    if len(self.actual_project['menus']) == 1:
        self.actual_project['menus'][self.actual_project['menus'][0]][6] = True

    for menu in self.actual_project['menus']:
        for button in self.actual_project['menus'][menu][1]:
            if self.actual_project['menus'][menu][2][button][4] == self.selected_menu:
                self.actual_project['menus'][menu][2][button][4] = None
    self.selected_menu = False
    clean_menus_list_selection(self)
    populate_menus_list(self)
    update_changes(self)

def clean_menus_list_selection(self):
    self.selected_menu = None
    self.no_preview_label.setVisible(True)
    self.nowediting_menus_panel_list.setCurrentItem(None)
    self.nowediting_menus_panel_duplicate.setEnabled(False)
    self.nowediting_menus_panel_remove.setEnabled(False)
    clean_buttons_selection(self)

def transparency_slider_changing(self):
    self.menus_properties_panel_transparency_slider_value.setText(str(int(self.menus_properties_panel_transparency_slider.value())) + '%')

def transparency_slider_changed(self):
    self.actual_project['menus'][self.selected_menu][8] = self.menus_properties_panel_transparency_slider.value()/100.0
    preview_overlay(self)

def border_slider_changing(self):
    self.menus_properties_panel_border_slider_value.setText(str(int(self.menus_properties_panel_border_slider.value())) + '%')

def border_slider_changed(self):
    self.actual_project['menus'][self.selected_menu][9] = self.menus_properties_panel_border_slider.value()/100.0
    preview_overlay(self)

###################################################################################################
########################################################################################### BUTTONS
###################################################################################################

def add_menu_button(self):
    new_button_name = check_name(self, self.actual_project['menus'][self.selected_menu][1], 'button')
    self.actual_project['menus'][self.selected_menu][1].append(new_button_name)
    self.actual_project['menus'][self.selected_menu][2][new_button_name] = [  10,             # posição X
                                                                    10,             # posição Y
                                                                    50,             # largura
                                                                    50,             # altura
                                                                    None,           # jump to
                                                                    [   None,       # top direction
                                                                        None,       # right direction
                                                                        None,       # bottom direction
                                                                        None        # left direction
                                                                    ]
                                                                ]
    populate_menu_buttons_list(self)
    update_jumpto_list(self)
    update_changes(self)

def menu_button_selected(self):
    self.options_panel_menu_buttons_position_box.setEnabled(True)
    self.selected_menu_button = self.options_panel_menu_buttons.currentItem().text()
    self.options_panel_menu_buttons_remove.setEnabled(True)
    self.options_panel_menu_buttons_edit.setEnabled(True)
    update_changes(self)

def menu_buttons_set_geometry(self):
    final_list = [self.options_panel_menu_buttons_x_position.value(), self.options_panel_menu_buttons_y_position.value(), self.options_panel_menu_buttons_width.value(), self.options_panel_menu_buttons_height.value(), self.actual_project['menus'][self.selected_menu][2][self.selected_menu_button][4], self.actual_project['menus'][self.selected_menu][2][self.selected_menu_button][5]]
    self.actual_project['menus'][self.selected_menu][2][self.selected_menu_button] = final_list
    update_changes(self)

def populate_menu_buttons_list(self):
    self.options_panel_menu_buttons.clear()
    self.options_panel_menu_buttons_edit.setText('')
    for button in self.actual_project['menus'][self.selected_menu][1]:
        self.options_panel_menu_buttons.addItem(button)

def populate_button_directions_list(self):
    final_list = ["Auto"]
    for button in self.actual_project['menus'][self.selected_menu][1]:
        if not button == self.selected_menu_button:
            final_list.append(button)
    self.options_panel_menu_buttons_directions_top.clear()
    self.options_panel_menu_buttons_directions_top.addItems(final_list)
    self.options_panel_menu_buttons_directions_right.clear()
    self.options_panel_menu_buttons_directions_right.addItems(final_list)
    self.options_panel_menu_buttons_directions_bottom.clear()
    self.options_panel_menu_buttons_directions_bottom.addItems(final_list)
    self.options_panel_menu_buttons_directions_left.clear()
    self.options_panel_menu_buttons_directions_left.addItems(final_list)

def remove_menu_button(self):
    del self.actual_project['menus'][self.selected_menu][2][self.selected_menu_button]
    self.actual_project['menus'][self.selected_menu][1].remove(self.selected_menu_button)

    for menu in self.actual_project['menus']:
        for button in self.actual_project['menus'][menu][1]:
            directions_list = self.actual_project['menus'][menu][2][button][5]
            for i in range(len(directions_list)):
                if directions_list[i] == self.selected_menu_button:
                    directions_list[i] = None
            self.actual_project['menus'][menu][2][button][5] = directions_list

    populate_menu_buttons_list(self)
    clean_buttons_selection(self)

def edit_menu_button(self):
    self.options_panel_menu_buttons.setEnabled(False)
    self.options_panel_menu_buttons_remove.setEnabled(False)
    self.options_panel_menu_buttons_add.setEnabled(False)
    self.options_panel_menu_buttons_position_box.setEnabled(False)
    self.options_panel_menu_buttons_edit_field.setVisible(True)
    self.options_panel_menu_buttons_edit_confirm.setVisible(True)
    self.options_panel_menu_buttons_edit_cancel.setVisible(True)
    self.options_panel_menu_buttons_edit.setText(self.selected_menu_button)
    #self.options_panel_menu_buttons_edit_field

def edit_field_menu_changed(self):
    if self.options_panel_menu_buttons_edit_field.text() in self.actual_project['menus'][self.selected_menu][1]:
        self.options_panel_menu_buttons_edit_confirm.setEnabled(False)
    else:
        self.options_panel_menu_buttons_edit_confirm.setEnabled(True)

def edit_confirm_menu_button(self):
    old_menu_button_name = self.selected_menu_button
    old_menu_button = self.actual_project['menus'][self.selected_menu][2][self.selected_menu_button]
    self.actual_project['menus'][self.selected_menu][2][self.options_panel_menu_buttons_edit_field.text()] = old_menu_button
    self.actual_project['menus'][self.selected_menu][1].append(self.options_panel_menu_buttons_edit_field.text())

    del self.actual_project['menus'][self.selected_menu][2][old_menu_button_name]
    self.actual_project['menus'][self.selected_menu][1].remove(old_menu_button_name)

    self.actual_project['menus'][self.selected_menu][1]

    for button in self.actual_project['menus'][self.selected_menu][1]:
        direction_index = 0
        for direction in self.actual_project['menus'][self.selected_menu][2][button][5]:
            if direction == old_menu_button_name:
                self.actual_project['menus'][self.selected_menu][2][button][5][direction_index] = self.options_panel_menu_buttons_edit_field.text()
            direction_index += 1

    populate_menu_buttons_list(self)
    clean_buttons_selection(self)

def edit_cancel_menu_button(self):
    self.options_panel_menu_buttons_edit_field.setVisible(False)
    self.options_panel_menu_buttons_edit_confirm.setVisible(False)
    self.options_panel_menu_buttons_edit_cancel.setVisible(False)
    populate_menu_buttons_list(self)
    clean_buttons_selection(self)

def clean_buttons_selection(self):
    self.options_panel_menu_buttons.setEnabled(True)
    self.options_panel_menu_buttons_add.setEnabled(True)
    self.options_panel_menu_buttons_edit_field.setText('')
    self.selected_menu_button = None
    self.options_panel_menu_buttons.setCurrentItem(None)
    self.options_panel_menu_buttons_remove.setEnabled(False)
    self.options_panel_menu_buttons_position_box.setEnabled(False)
    self.options_panel_menu_buttons_edit.setEnabled(False)
    self.options_panel_menu_buttons_x_position.setValue(0)
    self.options_panel_menu_buttons_y_position.setValue(0)
    self.options_panel_menu_buttons_width.setValue(0)
    self.options_panel_menu_buttons_height.setValue(0)
    update_changes(self)

def button_directions_selected(self):
    directions_list = [None,None,None,None]
    if not self.options_panel_menu_buttons_directions_top.currentText() == 'Auto':
        directions_list[0] = self.options_panel_menu_buttons_directions_top.currentText()
    if not self.options_panel_menu_buttons_directions_right.currentText() == 'Auto':
        directions_list[1] = self.options_panel_menu_buttons_directions_right.currentText()
    if not self.options_panel_menu_buttons_directions_bottom.currentText() == 'Auto':
        directions_list[2] = self.options_panel_menu_buttons_directions_bottom.currentText()
    if not self.options_panel_menu_buttons_directions_left.currentText() == 'Auto':
        directions_list[3] = self.options_panel_menu_buttons_directions_left.currentText()
    self.actual_project['menus'][self.selected_menu][2][self.selected_menu_button][5] = directions_list
    update_changes(self)

###################################################################################################
############################################################################################ VIDEOS
###################################################################################################

def update_timeline(self):
    if self.selected_video:
        self.videos_player_controls_panel_current_time.setText(convert_to_timecode(str(self.preview_video_obj.get_position() * self.actual_project['videos'][self.selected_video][5])))
        self.videos_player_timeline.update()

def add_video(self):
    video_path_list = QFileDialog.getOpenFileNames(self, "Selecione um video", path_home, "Video files (*.m4v *.m2v *.mpg *.mp4 *.mov *.mkv)")[0]

    for video_path_file in video_path_list:
        video_path = os.path.abspath(video_path_file)
        if not video_path == '':
            video_name = video_path.split('/')[-1].split('\\')[-1].split('.')[0]

            chapters_list, chapters_dict = get_video_chapters(self, video_path)

            length_xml = subprocess.Popen([ffprobe_bin, '-loglevel', 'error', '-show_format', '-show_streams', '-print_format', 'xml', video_path], stdin=subprocess.DEVNULL, stdout=subprocess.PIPE, stderr=subprocess.STDOUT).stdout.read().decode()
            length = float(length_xml.split(' duration="')[1].split('"')[0])
            framerate = float(length_xml.split(' codec_type="video"')[1].split(' r_frame_rate="')[1].split('"')[0].split('/')[0]) / float(length_xml.split(' codec_type="video"')[1].split(' r_frame_rate="')[1].split('"')[0].split('/')[1])

            start = False
            end = False

            self.actual_project['videos'][video_name] =   [   video_path,                   # [0] Video path
                                                    chapters_list,                # [1] Chapters list
                                                    chapters_dict,                # [2] Chapters dict
                                                    False,                        # [3] if it is intro video
                                                    True,                         # [4] if it is to be converted
                                                    length,                       # [5] length
                                                    start,                        # [6] Start
                                                    end,                          # [7] End
                                                    None,                         # [8] Post
                                                    0,                            # [9] Resolution
                                                    framerate                     # [10] Framerate
                                                ]

            generate_preview_image(self, video_name, self.actual_project['videos'])

    populate_videos_list(self)

def populate_videos_list(self):
    self.nowediting_videos_panel_list.clear()
    for video in self.actual_project['videos']:
        icon = QIcon(os.path.join(path_tmp, video + '.preview.png'))
        self.nowediting_videos_panel_list.addItem(QListWidgetItem(icon, video))

def update_jumpto_list(self):
    self.list_of_jumpto = []
    for video in [*self.actual_project['videos']]:
        self.list_of_jumpto.append(video)
    for menu in [*self.actual_project['menus']]:
        self.list_of_jumpto.append(menu)

def remove_video(self):
    del self.actual_project['videos'][self.selected_video]
    for menu in self.actual_project['menus']:
        for button in self.actual_project['menus'][menu][1]:
            if self.actual_project['menus'][menu][2][button][4] == self.selected_video:
                self.actual_project['menus'][menu][2][button][4] = None
    self.selected_video = False
    clean_videos_list_selection(self)
    populate_videos_list(self)

def clean_videos_list_selection(self):
    self.selected_video = False
    self.no_preview_label.setVisible(True)
    self.nowediting_videos_panel_list.setCurrentItem(None)

def video_selected(self):
    if self.nowediting_videos_panel_list.currentItem():
        self.nowediting_videos_panel_remove.setEnabled(True)
        self.selected_video = self.nowediting_videos_panel_list.currentItem().text()
    else:
        self.nowediting_videos_panel_remove.setEnabled(False)

    self.preview_video_widget.setVisible(True)
    media = self.video_instance.media_new(get_preview_file(self, self.actual_project['videos'][self.selected_video][0]))
    self.preview_video_obj.set_media(media)

    #nowediting_panel_button_changed(self, self.nowediting)

    update_changes(self)

def button_jumpto_selected(self):
    self.actual_project['menus'][self.selected_menu][2][self.selected_menu_button][4] = self.options_panel_menu_buttons_jumpto.currentText()

def menus_properties_panel_jumpto_selected(self):
    if self.menus_properties_panel_jumpto.currentIndex() == 0:
        self.actual_project['menus'][self.selected_menu][7] = False
    else:
        self.actual_project['menus'][self.selected_menu][7] = self.menus_properties_panel_jumpto.currentText()

def video_play(self):
    self.preview_video_obj.play()

def video_pause(self):
    self.videos_player_controls_panel_pause_background.setPixmap(QPixmap(os.path.join(path_graphics, 'videos_player_controls_panel_pause_press.png')))
    self.videos_player_controls_panel_play_background.setPixmap(QPixmap(os.path.join(path_graphics, 'videos_player_controls_panel_play_press.png')))
    self.preview_video_obj.pause()

def video_stop(self):
    self.preview_video_obj.stop()
    update_timeline(self)

def video_seek_next_frame(self, position):
    #self.preview_video_obj.seek(self.preview_video_obj.currentTime() + position)
    self.preview_video_obj.set_position(self.preview_video_obj.get_position() + position)
    update_timeline(self)

def video_seek_back_frame(self, position):
    #self.preview_video_obj.seek(self.preview_video_obj.currentTime() - position)
    self.preview_video_obj.set_position(self.preview_video_obj.get_position() - position)
    update_timeline(self)

def video_add_this_mark_frame(self):
    self.selected_video_chapter = None
    append_chapter(self, check_name(self, self.actual_project['videos'][self.selected_video][1], 'mark'), convert_to_timecode(str(self.preview_video_obj.get_position() * self.actual_project['videos'][self.selected_video][5])))
    update_changes(self)

def video_set_trim_start(self):
    self.actual_project['videos'][self.selected_video][6] = self.preview_video_obj.get_position() * self.actual_project['videos'][self.selected_video][5]
    update_timeline(self)

def video_set_trim_end(self):
    self.actual_project['videos'][self.selected_video][7] = self.preview_video_obj.get_position() * self.actual_project['videos'][self.selected_video][5]
    update_timeline(self)

def populate_jumpto(self):
    self.options_panel_menu_buttons_jumpto.clear()
    final_list = []
    for menu in self.actual_project['menus']:
        final_list.append(menu)
    for video in self.actual_project['videos']:
        final_list.append(video)
        for chapter in sort_list_of_chapters(self.actual_project['videos'][video][2]):
            final_list.append(video + ' > ' + str(chapter) + ' (' + self.actual_project['videos'][video][2][chapter] + ')')
        list_of_chapter_groups = []
        for chapter in sort_list_of_chapters(self.actual_project['videos'][video][2]):
            if not str(chapter).split(' ')[0] in list_of_chapter_groups:
                list_of_chapter_groups.append(str(chapter).split(' ')[0])
                final_list.append(video + ' > ' + str(chapter).split(' ')[0])
    self.options_panel_menu_buttons_jumpto.addItems(final_list)

def populate_jumpto_menus(self):
    self.menus_properties_panel_jumpto.clear()
    final_list = ['Play again (loop)']
    for menu in self.actual_project['menus']:
        if not menu == self.selected_menu:
            final_list.append(menu)
    for video in self.actual_project['videos']:
        final_list.append(video)
    self.menus_properties_panel_jumpto.addItems(final_list)

    if self.actual_project['menus'][self.selected_menu][7]:
        self.menus_properties_panel_jumpto.setCurrentIndex(self.menus_properties_panel_jumpto.findText(self.actual_project['menus'][self.selected_menu][7]))
    else:
        self.menus_properties_panel_jumpto.setCurrentIndex(0)

def set_intro_video(self):
    self.actual_project['videos'][self.selected_video][3] = self.options_panel_video_intro_video_checkbox.isChecked()

    for video in self.actual_project['videos']:
        if not video == self.selected_video:
            self.actual_project['videos'][video][3] = False

def set_reencode_video(self):
    self.actual_project['videos'][self.selected_video][4] = self.options_panel_video_reencode_video_checkbox.isChecked()
    update_changes(self)

def video_resolution_combo_selected(self):
    self.actual_project['videos'][self.selected_video][9] = self.options_panel_video_resolution_combo.currentIndex()

def video_jumpto_selected(self):
    if self.options_panel_video_jumpto.currentText() == 'Main menu':
        self.actual_project['videos'][self.selected_video][8] = None
    else:
        self.actual_project['videos'][self.selected_video][8] = self.options_panel_video_jumpto.currentText()

def populate_jumpto_list(self):
    final_list = ['Main menu']
    final_list += self.actual_project['menus']
    final_list += self.actual_project['videos']
    final_list.remove(self.selected_video)
    self.options_panel_video_jumpto.clear()
    self.options_panel_video_jumpto.addItems(final_list)

###################################################################################################
########################################################################################## CHAPTERS
###################################################################################################

def import_chapters(self):
    filepath = QFileDialog.getOpenFileName(self, "Select a video or XML", path_home, "Video files (*.fcpxml *.m4v *.m2v *.mpg *.mp4 *.mov *.mkv)")[0]#.toUtf8()
    if not filepath == '':
        chapters_list, chapters_dict = get_video_chapters(self, filepath)

        self.actual_project['videos'][self.selected_video][1] = self.actual_project['videos'][self.selected_video][1] + chapters_list
        for chapter in chapters_dict.keys():
            self.actual_project['videos'][self.selected_video][2][chapter] = chapters_dict[chapter]

        populate_chapters_list(self)

def get_video_chapters(self, filepath):
    chapters_list = []
    chapters_dict = {}
    if filepath.split('.')[-1] == 'fcpxml':
        chapters_xml = codecs.open(filepath, 'r', 'utf-8').read()
        for chapter_line in chapters_xml.split('<chapter-marker ')[1:]:
            if '/>' in chapter_line and ('start="' in chapter_line.split('/>')[0] and 'duration="' in chapter_line.split('/>')[0]):
                mark = convert_to_timecode(chapter_line.split('start="')[1].split('"')[0])#, chapter_line.split('duration="')[1].split('"')[0])
                name = check_name(self, chapters_list, chapter_line.split('value="')[1].split('"')[0])
                chapters_list.append(name)
                chapters_dict[name] = mark
    else:
        chapters_xml = subprocess.Popen([ffprobe_bin, '-loglevel', 'error', '-show_chapters', '-print_format', 'xml', filepath], stdin=subprocess.PIPE, stdout=subprocess.PIPE, stderr=subprocess.PIPE).stdout.read().decode()
        for chapter_line in chapters_xml.split('<chapter '):
            if '</chapter>' in chapter_line:
                mark = convert_to_timecode(chapter_line.split('start="')[1].split('"')[0])#, chapter_line.split('time_base="')[1].split('"')[0])
                name = check_name(self, chapters_list, chapter_line.split('value="')[1].split('"')[0])
                chapters_list.append(name)
                chapters_dict[name] = mark

    return chapters_list, chapters_dict

def chapter_selected(self):
    self.selected_video_chapter = self.options_panel_video_chapters_list.currentItem().text().split('\n')[0]
    self.options_panel_video_chapters_remove.setEnabled(True)
    self.options_panel_video_chapters_edit.setEnabled(True)
    chapter_seek_in_timeline(self)

def chapter_seek_in_timeline(self):
    self.preview_video_obj.set_position(convert_timecode_to_seconds(self.actual_project['videos'][self.selected_video][2][self.selected_video_chapter])/self.actual_project['videos'][self.selected_video][5])

def add_chapter(self):
    self.selected_video_chapter = None
    self.options_panel_video_chapters_timecode.setText('')
    self.options_panel_video_chapters_name.setText('')
    show_edit_chapter(self)

def append_chapter(self, name, timecode):
    if self.selected_video_chapter:
        self.actual_project['videos'][self.selected_video][1].remove(self.selected_video_chapter)
        del self.actual_project['videos'][self.selected_video][2][self.selected_video_chapter]
    self.actual_project['videos'][self.selected_video][1].append(name)
    self.actual_project['videos'][self.selected_video][2][name] = timecode

def confirm_edit_chapter(self):
    append_chapter(self, self.options_panel_video_chapters_name.text(), self.options_panel_video_chapters_timecode.text())
    hide_edit_chapter(self)
    update_changes(self)

def check_chapter_name(self):
    None

def hide_edit_chapter(self):
    self.options_panel_video_chapters_list.setGeometry(10,70,self.options_panel_video_panel.width()-20,self.options_panel_video_panel.height()-115)
    self.options_panel_video_chapters_list.setEnabled(True)
    self.options_panel_video_chapters_name_label.setVisible(False)
    self.options_panel_video_chapters_name.setVisible(False)
    self.options_panel_video_chapters_timecode_label.setVisible(False)
    self.options_panel_video_chapters_timecode.setVisible(False)
    self.options_panel_video_chapters_edit_confirm.setVisible(False)
    self.options_panel_video_chapters_edit_cancel.setVisible(False)
    self.options_panel_video_chapters_add.setVisible(True)
    self.options_panel_video_chapters_remove.setVisible(True)
    self.options_panel_video_chapters_edit.setVisible(True)
    self.options_panel_video_chapters_import.setVisible(True)

def remove_chapter(self):
    del self.actual_project['videos'][self.selected_video][2][self.selected_video_chapter]
    self.actual_project['videos'][self.selected_video][1].remove(self.selected_video_chapter)
    self.selected_video_chapter = None
    update_changes(self)

def edit_chapter(self):
    self.options_panel_video_chapters_name.setText(self.selected_video_chapter)
    self.options_panel_video_chapters_timecode.setText(self.actual_project['videos'][self.selected_video][2][self.selected_video_chapter])
    check_chapter_name(self)
    show_edit_chapter(self)

def show_edit_chapter(self):
    self.options_panel_video_chapters_list.setGeometry(10,70,self.options_panel_video_panel.width()-20,self.options_panel_video_panel.height()-135)
    self.options_panel_video_chapters_list.setEnabled(False)
    self.options_panel_video_chapters_name_label.setVisible(True)
    self.options_panel_video_chapters_name.setVisible(True)
    self.options_panel_video_chapters_timecode_label.setVisible(True)
    self.options_panel_video_chapters_timecode.setVisible(True)
    self.options_panel_video_chapters_edit_confirm.setVisible(True)
    self.options_panel_video_chapters_edit_cancel.setVisible(True)
    self.options_panel_video_chapters_add.setVisible(False)
    self.options_panel_video_chapters_remove.setVisible(False)
    self.options_panel_video_chapters_edit.setVisible(False)
    self.options_panel_video_chapters_import.setVisible(False)

def populate_chapters_list(self):
    self.options_panel_video_chapters_list.clear()
    list_of_chapters_in_order = sort_list_of_chapters(self.actual_project['videos'][self.selected_video][2])

    self.actual_project['videos'][self.selected_video][1] = list_of_chapters_in_order

    for chapter in self.actual_project['videos'][self.selected_video][1]:
        icon = QIcon(os.path.join(path_graphics, 'chapter.png'))
        self.options_panel_video_chapters_list.addItem(QListWidgetItem(icon, str(chapter) + '\n' + self.actual_project['videos'][self.selected_video][2][chapter]))

###################################################################################################
###################################################################################### GENERATE DVD
###################################################################################################

app = QApplication(sys.argv)
app.addLibraryPath(app.applicationDirPath() + "/../PlugIns")
app.setStyle("plastique")
app.setApplicationName("Open DVD Producer")
font_database = QFontDatabase()
font_database.addApplicationFont(os.path.join(path_opendvdproducer, 'resources','Ubuntu-RI.ttf'))
font_database.addApplicationFont(os.path.join(path_opendvdproducer, 'resources','Ubuntu-R.ttf'))
font_database.addApplicationFont(os.path.join(path_opendvdproducer, 'resources','Ubuntu-B.ttf'))
font_database.addApplicationFont(os.path.join(path_opendvdproducer, 'resources','Ubuntu-BI.ttf'))
font_database.addApplicationFont(os.path.join(path_opendvdproducer, 'resources','UbuntuMono-R.ttf'))
app.setFont(QFont('Ubuntu', interface_font_size))
app.setDesktopSettingsAware(False)
app.main = main_window()

app.main.show()
clean_changes(app.main)

sys.exit(app.exec_())
