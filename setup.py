#!/usr/bin/env python3

from setuptools import find_packages, setup
import os

desktop_file = '[Desktop Entry]'
desktop_file += '\nEncoding=UTF-8'
desktop_file += '\nName=Open DVD Producer'
desktop_file += '\nExec=python3 /usr/local/share/opendvdproducer/opendvdproducer.py'
desktop_file += '\nIcon=/usr/local/share/opendvdproducer/graphics/opendvdproducer.png'
desktop_file += '\nInfo=Open DVD Producer'
desktop_file += '\nCategories=Application;Multimedia'
desktop_file += '\nComment=A modern, open source software to produce DVD images.'
desktop_file += '\nTerminal=false'
desktop_file += '\nType=Application'
desktop_file += '\nStartupNotify=true'
open('/tmp/' + 'opendvdproducer.desktop', 'w').write(desktop_file)

data_files = []
for filename in os.listdir('graphics'):
    list_of_files = []
    if filename.endswith(('.png', '.mkv')):
        list_of_files.append('graphics/' + filename)
    data_files.append(('share/opendvdproducer/graphics', list_of_files))
for filename in os.listdir('resources'):
    list_of_files = []
    if filename.endswith(('.ttf', '.flac')):
        list_of_files.append('resources/' + filename)
    data_files.append(('share/opendvdproducer/resources', list_of_files))
for filename in os.listdir('modules'):
    list_of_files = []
    if filename.endswith(('.py')):
        list_of_files.append('modules/' + filename)
    data_files.append(('share/opendvdproducer/modules', list_of_files))

data_files.append(('share/opendvdproducer', ['opendvdproducer.py']))
#data_files.append(('share/applications', ['snap/gui/opendvdproducer.desktop']))
data_files.append(('share/applications', ['/tmp/opendvdproducer.desktop']))

setup(name='opendvdproducer',
      version='18.11',
      url='https://opendvdproducer.jonata.org/',
      author='Jonatã Bolzan Loss',
      author_email='jonata@jonata.org',
      description=('A modern, open source cross platform software to produce DVD images.'),
      license='GPL3',
      install_requires=[
                           'PyQt5',
                           'python-vlc'
                        ],
      data_files = data_files,
      packages=find_packages(),
     )
