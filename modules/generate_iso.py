#!/usr/bin/python3
# -*- coding: utf-8 -*-

import os, codecs, tempfile, random, subprocess, shutil

def generate_ddp(original_file, output_path):
    split_area = 4699979776
    sides = 1
    actual_side = 0
    disc_size = 'B' #Disc size "B" = 12cm, "A" = 8cm *
    tp_mode = "PTP"

    if os.path.getsize(original_file) < split_area:
        layers = ['Layer0']
    else:
        layers = ['Layer0', 'Layer1']

    if not os.path.isdir(output_path):
        os.mkdir(output_path)

    actual_layer = 0
    main_size = os.path.getsize(original_file)/2048
    for layer in layers:
        os.mkdir(os.path.join(output_path, layer))

        if len(layers) == 1:
            layer_size = main_size
            layer0_size = main_size
        else:
            layer0_size = split_area/2048
            if actual_layer == 0:
                layer_size = split_area/2048
            else:
                layer_size = main_size - (split_area/2048)

        ddpid = str(
            "DDP 2.00" +
            "             " +
            "        " +
            "        " +
            " " +
            "                                                " +
            " " +
            "DV" +
            str(sides) +
            str(actual_side) +
            str(len(layers)) +
            str(actual_layer) +
            "I" +
            disc_size +
            "0" +
            "0" +
            "  " +
            "                             " +

            "VVVM" +
            "D2" +
            "        " +
            "00000016" +
            "00193024" +
            "        " +
            "DV" +
            "1" +
            "0" +
            "    " +
            "    " +
            "    " +
            " " +
            "  " +
            "  " +
            "            " +
            "017"
            "CONTROL.DAT      " +
            " " +
            "    " +
            "        " +
            "         " +
            "               " +

            "VVVM" +
            "D0" +
            "        " +
            str("%08d" % int(layer_size)) +
            "00196608" +
            "        " +
            "DV" +
            "0" +
            "0" +
            "    " +
            "    " +
            "    " +
            " " +
            "  " +
            "  " +
            "            " +
            "017"
            "MAIN.DAT         " +
            " " +
            "    " +
            "        " +
            "         " +
            "               "
            )

        open(os.path.join(output_path, layer, 'DDPID'), 'w').write(ddpid)

        control_dat = ''
        control_dat += str(bytearray([int('00000000', 2)]))
        control_dat += str(bytearray([int('00000000', 2)]))
        control_dat += str(bytearray([int('00000000', 2)]))
        control_dat += str(bytearray([int('00000000', 2)]))
        control_dat += str(bytearray([int('00000000', 2)]))
        control_dat += str(bytearray([int('00000000', 2)]))
        control_dat += str(bytearray([int('00000001', 2)]))
        control_dat += str(bytearray([int('00000010', 2)]))

        if len(layers) == 2:
            if tp_mode == "OTP":
                control_dat += str(bytearray([int('00110001', 2)]))
            else:
                control_dat += str(bytearray([int('00100001', 2)]))
        else:
            if tp_mode == "OTP":
                control_dat += str(bytearray([int('00010001', 2)]))
            else:
                control_dat += str(bytearray([int('00000001', 2)]))

        control_dat += str(bytearray([int('00010000', 2)]))
        control_dat += str(bytearray([int('00000000', 2)]))
        control_dat += str(bytearray([int('00000011', 2)]))
        control_dat += str(bytearray([int('00000000', 2)]))
        control_dat += str(bytearray([int('00000000', 2)]))
        control_dat += str(bytearray([int('00000000', 2)]))
        control_dat += str(('%%0%dx' % (3 << 1) % int(196608 + main_size - 1)).decode('hex')[-3:])
        control_dat += str(bytearray([int('00000000', 2)]))

        if tp_mode == "OTP" and len(layers) == 2:
            control_dat += str(('%%0%dx' % (3 << 1) % int(196608 + layer0_size - 1)).decode('hex')[-3:])
        else:
            control_dat += str(bytearray([int('00000000', 2)]))
            control_dat += str(bytearray([int('00000000', 2)]))
            control_dat += str(bytearray([int('00000000', 2)]))

        control_dat += str(bytearray([int('00000000', 2)]))

        for i in range(16):
            control_dat += str(bytearray([int('00000000', 2)]))

        for i in range(2015):
            control_dat += str(bytearray([int('00000000', 2)]))

        for i in range(6):
            control_dat += str(bytearray([int('00000000', 2)]))
        for i in range(2048):
            control_dat += str(bytearray([int('00000000', 2)]))

        for i in range(14):
            for j in range(6):
                control_dat += str(bytearray([int('00000000', 2)]))
            for j in range(2048):
                control_dat += str(bytearray([int('00000000', 2)]))

        open(os.path.join(output_path, layer, 'CONTROL.DAT'), 'w').write(control_dat)

        actual_layer += 1

    if len(layers) == 2:
        subprocess.run([split_bin, '-b=' + str(split_area), os.path.join(path_tmp, 'movie.iso'), os.path.join(path_tmp, 'movie.iso_part')], stdin=subprocess.DEVNULL, stdout=subprocess.DEVNULL, stderr=subprocess.DEVNULL)
        shutil.move(os.path.join(path_tmp, 'movie.iso_part.aa'), os.path.join(output_path, 'Layer0', 'MAIN.DAT'))
        shutil.move(os.path.join(path_tmp, 'movie.iso_part.ab'), os.path.join(output_path, 'Layer1', 'MAIN.DAT'))
    else:
        shutil.copy(os.path.join(path_tmp, 'movie.iso'), os.path.join(output_path, 'Layer0', 'MAIN.DAT'))


def run(actual_project, signal, split_bin, ffprobe_bin, ffmpeg_bin, imagemagick_convert_bin, spumux_bin, dvdauthor_bin, mkisofs_bin, md5_bin):
    path_tmp = os.path.join(tempfile.gettempdir(), 'opendvdproducer-' + str(random.randint(1000,9999)))
    os.mkdir(path_tmp)

    audio_codec = 'ac3'
    framerate = '25'
    height = 480
    video_format = 'pal'
    video_resolutions = ['720x480']
    aspect_ratio = '16:9'
    audio_datarate = 384
    generate_md5 = False
    generate_ddp = False
    generate_iso = True

    total_progress = str(len(actual_project['videos']) + len(actual_project['menus']) + 8)

    signal.sig.emit('START,0,' + total_progress)

    if not os.path.isdir(os.path.join(path_tmp, 'dvd')):
        os.mkdir(os.path.join(path_tmp, 'dvd'))

    final_dvd_author_xml = '<dvdauthor dest="' + os.path.join(path_tmp, 'dvd') + '" jumppad="yes">'

    list_of_used_videos = []
    for video in actual_project['videos']:
        list_of_used_videos.append(video)

    intro_video = False
    for video in actual_project['videos']:
        if actual_project['videos'][video][3]:
            intro_video = video
            break

    final_dvd_author_xml += '<vmgm><menus><video format="' + video_format + '" aspect="' + aspect_ratio + '"'
    if aspect_ratio == '16:9':
        final_dvd_author_xml += ' widescreen="nopanscan"'
    final_dvd_author_xml += ' /><audio lang="EN" /><subpicture lang="EN" />'

    final_dvd_author_xml += '<pgc entry="title">'

    if intro_video:
        if actual_project['videos'][intro_video][4]:
            video_path = os.path.join(path_tmp, intro_video + '.mpg')
        else:
            video_path = actual_project['videos'][video][0]
            if actual_project['has_menus']:
                final_dvd_author_xml += '<pre> if ( g0 != 0 ) jump titleset 1 menu; g0 = 1; </pre>'
            else:
                final_dvd_author_xml += '<pre> if ( g0 != 0 ) jump titleset 1 title 1; g0 = 1; </pre>'
        final_dvd_author_xml += '<vob file="' + video_path + '"/>'
        list_of_used_videos.remove(intro_video)

    if actual_project['has_menus']:
        final_dvd_author_xml += '<post>jump titleset 1 menu;</post>'
    else:
        final_dvd_author_xml += '<post>jump title 1;</post>'

    final_dvd_author_xml += '</pgc></menus></vmgm>'
    final_dvd_author_xml += '<titleset>'

    menu_count = 1
    if actual_project['has_menus']:
        final_dvd_author_xml += '<menus>'

        final_dvd_author_xml += '<video format="' + video_format + '" aspect="' + aspect_ratio + '"'
        if aspect_ratio == '16:9':
            final_dvd_author_xml += ' widescreen="nopanscan"'
        final_dvd_author_xml += ' /><audio lang="EN" />'

        if aspect_ratio == '16:9':
            final_dvd_author_xml += '<subpicture lang="EN">'
            final_dvd_author_xml += '<stream id="0" mode="widescreen" /><stream id="1" mode="letterbox" />'
            final_dvd_author_xml += '</subpicture>'

        list_of_used_groups = []
        for video in list_of_used_videos:
            list_of_chapters = []
            for menu in actual_project['menus']:
                for button in actual_project['menus'][menu][1]:
                    if actual_project['menus'][menu][2][button][4]:
                        if ' > ' in actual_project['menus'][menu][2][button][4]:
                            if actual_project['menus'][menu][2][button][4].split(' > ')[0] == video:
                                if '(' in actual_project['menus'][menu][2][button][4].split(' > ')[1]:
                                    list_of_chapters.append(actual_project['menus'][menu][2][button][4].split(' > ')[1].split(' (')[1].split(')')[0])
                                elif not actual_project['menus'][menu][2][button][4] in list_of_used_groups:
                                    list_of_used_groups.append(actual_project['menus'][menu][2][button][4])

        first_menu = True

        signal.sig.emit('PROCESSING MENUS,' + str(menu_count))

        for menu in actual_project['menus']:
            final_spumux_xml_0 = ''
            final_spumux_xml_1 = ''
            final_dvd_author_xml += '<pgc'
            if first_menu:
                final_dvd_author_xml += ' entry="root"'
                first_menu = False
            final_dvd_author_xml += '>'
            final_dvd_author_xml += '<vob file="' + os.path.join(path_tmp, menu + '_final.mpg') + '" />'

            if actual_project['menus'][menu][0].split('.')[-1] == 'png':
                if actual_project['menus'][menu][5] and os.path.isfile(actual_project['menus'][menu][5]):
                    sound_file = actual_project['menus'][menu][5]
                else:
                    sound_file = os.path.join(path_opendvdproducer, 'resources','silence.flac')

                with subprocess.Popen([ffprobe_bin, '-show_format', '-print_format', 'xml', sound_file], stdin=subprocess.DEVNULL, stdout=subprocess.PIPE, stderr=subprocess.STDOUT).stdout.read().decode() as sound_length_xml:
                    if ' duration="' in sound_length_xml:
                        sound_length = float(sound_length_xml.split(' duration="')[1].split('"')[0])
                    else:
                        sound_length = 60.0

                final_command = [ffmpeg_bin]
                final_command += '-y',
                final_command += '-loop','1'
                final_command += '-i', actual_project['menus'][menu][0]
                final_command += '-i', sound_file
                final_command += '-c:v', 'mpeg2video'
                final_command += '-c:a', audio_codec
                final_command += '-f', 'dvd'
                final_command += '-s', video_resolutions[0]
                final_command += '-r', framerate
                final_command += '-pix_fmt', 'yuv420p'
                final_command += '-g', str(actual_project['gop_size'])
                final_command += '-b:v', str(actual_project['menu_bitrate'] * 1000)
                if actual_project['menu_encoding'] == 'CBR':
                    final_command += '-maxrate', str(actual_project['menu_bitrate'] * 1000)
                    final_command += '-minrate', str(actual_project['menu_bitrate'] * 1000)
                elif actual_project['menu_encoding'] == 'VBR':
                    final_command += '-maxrate', str(actual_project['menu_max_bitrate'] * 1000)
                    final_command += '-minrate', '0'
                final_command += '-bufsize', '1835008'
                final_command += '-packetsize', '2048'
                final_command += '-muxrate', '10080000'
                final_command += '-b:a', str(audio_datarate * 1000)
                final_command += '-ar', '48000'
                final_command += '-aspect', aspect_ratio
                final_command += '-t', str(sound_length)
                final_command += os.path.join(path_tmp, menu + '.mpg'),
                subprocess.run(final_command, stdin=subprocess.DEVNULL, stdout=subprocess.DEVNULL, stderr=subprocess.DEVNULL)

            else:
                final_command = [ffmpeg_bin]
                final_command += '-y',
                final_command += '-i', actual_project['menus'][menu][0]
                if actual_project['menu_twopass'] and actual_project['menu_encoding'] == 'VBR':
                    final_command += '-pass', '1'
                final_command += '-c:v', 'mpeg2video'
                final_command += '-c:a', audio_codec
                final_command += '-f', 'dvd'
                final_command += '-s', video_resolutions[0]
                final_command += '-r', framerate
                final_command += '-pix_fmt', 'yuv420p'
                final_command += '-g', str(actual_project['gop_size'])
                final_command += '-b:v', str(actual_project['menu_bitrate'] * 1000)
                if actual_project['menu_encoding'] == 'CBR':
                    final_command += '-maxrate', str(actual_project['menu_bitrate'] * 1000)
                    final_command += '-minrate', str(actual_project['menu_bitrate'] * 1000)
                elif actual_project['menu_encoding'] == 'VBR':
                    final_command += '-maxrate', str(actual_project['menu_max_bitrate'] * 1000)
                    final_command += '-minrate', '0'
                final_command += '-bufsize', '1835008'
                final_command += '-packetsize', '2048'
                final_command += '-muxrate', '10080000'
                final_command += '-b:a', str(audio_datarate * 1000)
                final_command += '-ar', '48000'
                final_command += '-aspect', aspect_ratio
                if actual_project['menu_twopass'] and actual_project['menu_encoding'] == 'VBR':
                    final_command += '-passlogfile', os.path.join(path_tmp, menu + '.log')
                final_command += os.path.join(path_tmp, menu + '.mpg'),
                subprocess.run(final_command, stdin=subprocess.DEVNULL, stdout=subprocess.DEVNULL, stderr=subprocess.DEVNULL)

                if actual_project['menu_twopass'] and actual_project['menu_encoding'] == 'VBR':
                    final_command = [ffmpeg_bin]
                    final_command += '-y',
                    final_command += '-i', actual_project['menus'][menu][0]
                    final_command += '-pass', '2'
                    final_command += '-c:v', 'mpeg2video'
                    final_command += '-c:a', audio_codec
                    final_command += '-f', 'dvd'
                    final_command += '-s', video_resolutions[0]
                    final_command += '-r', framerate
                    final_command += '-pix_fmt', 'yuv420p'
                    final_command += '-g', str(actual_project['gop_size'])
                    final_command += '-b:v', str(actual_project['menu_bitrate'] * 1000)
                    if actual_project['menu_encoding'] == 'CBR':
                        final_command += '-maxrate', str(actual_project['menu_bitrate'] * 1000)
                        final_command += '-minrate', str(actual_project['menu_bitrate'] * 1000)
                    elif actual_project['menu_encoding'] == 'VBR':
                        final_command += '-maxrate', str(actual_project['menu_max_bitrate'] * 1000)
                        final_command += '-minrate', '0'
                    final_command += '-bufsize', '1835008'
                    final_command += '-packetsize', '2048'
                    final_command += '-muxrate', '10080000'
                    final_command += '-b:a', str(audio_datarate * 1000)
                    final_command += '-ar', '48000'
                    final_command += '-aspect', aspect_ratio
                    final_command += '-passlogfile', os.path.join(path_tmp, menu + '.log')
                    final_command += os.path.join(path_tmp, menu + '.mpg'),
                    subprocess.run(final_command, stdin=subprocess.DEVNULL, stdout=subprocess.DEVNULL, stderr=subprocess.DEVNULL)

            if actual_project['menus'][menu][3]:
                menu_color = '#FFFFFF'
                if actual_project['menus'][menu][4]:
                    menu_color = actual_project['menus'][menu][4]
                size = video_resolutions[0] + '!'
                size_ws = video_resolutions[0].split('x')[0] + 'x' + str(height *.75) + '!'

                subprocess.run([imagemagick_convert_bin, actual_project['menus'][menu][3], '-resize', size, '+antialias', '-threshold', str(int(actual_project['menus'][menu][9]*100)) + '%', '-flatten', os.path.join(path_tmp, actual_project['menus'][menu][3].split('/')[-1].split('\\')[-1][:-4] + '_hl_0.png')], stdin=subprocess.DEVNULL, stdout=subprocess.DEVNULL, stderr=subprocess.DEVNULL)
                subprocess.run([imagemagick_convert_bin, os.path.join(path_tmp, actual_project['menus'][menu][3].split('/')[-1].split('\\')[-1][:-4] + '_hl_0.png'), '-threshold', str(int(actual_project['menus'][menu][9]*100)) + '%', '-transparent', 'white', '-type', 'TrueColorMatte', '-fill', menu_color + str('%02x' % int(actual_project['menus'][menu][8]*255)), '-opaque', 'black', os.path.join(path_tmp, actual_project['menus'][menu][3].split('/')[-1].split('\\')[-1][:-4] + '_hl_0.png')], stdin=subprocess.DEVNULL, stdout=subprocess.DEVNULL, stderr=subprocess.DEVNULL)

                subprocess.run([imagemagick_convert_bin, actual_project['menus'][menu][3], '-resize', size_ws, '+antialias', '-threshold', str(int(actual_project['menus'][menu][9]*100)) + '%', '-flatten', os.path.join(path_tmp, actual_project['menus'][menu][3].split('/')[-1].split('\\')[-1][:-4] + '_hl_1.png')], stdin=subprocess.DEVNULL, stdout=subprocess.DEVNULL, stderr=subprocess.DEVNULL)
                subprocess.run([imagemagick_convert_bin, os.path.join(path_tmp, actual_project['menus'][menu][3].split('/')[-1].split('\\')[-1][:-4] + '_hl_1.png'), '-resize', size_ws, '-matte', '-bordercolor', 'none', '-border', '0x' + str( height * .125 ), os.path.join(path_tmp, actual_project['menus'][menu][3].split('/')[-1].split('\\')[-1][:-4] + '_hl_1.png')], stdin=subprocess.DEVNULL, stdout=subprocess.DEVNULL, stderr=subprocess.DEVNULL)
                subprocess.run([imagemagick_convert_bin, os.path.join(path_tmp, actual_project['menus'][menu][3].split('/')[-1].split('\\')[-1][:-4] + '_hl_1.png'), '-threshold', str(int(actual_project['menus'][menu][9]*100)) + '%', '-transparent', 'white', '-type', 'TrueColorMatte', '-fill', menu_color + str('%02x' % int(actual_project['menus'][menu][8]*255)), '-opaque', 'black', os.path.join(path_tmp, actual_project['menus'][menu][3].split('/')[-1].split('\\')[-1][:-4] + '_hl_1.png')], stdin=subprocess.DEVNULL, stdout=subprocess.DEVNULL, stderr=subprocess.DEVNULL)

                final_spumux_xml_0 += '<subpictures><stream><spu force="yes" start="00:00:00.00" highlight="' + os.path.join(path_tmp, actual_project['menus'][menu][3].split('/')[-1].split('\\')[-1][:-4] + '_hl_0.png') + '">'
                final_spumux_xml_1 += '<subpictures><stream><spu force="yes" start="00:00:00.00" highlight="' + os.path.join(path_tmp, actual_project['menus'][menu][3].split('/')[-1].split('\\')[-1][:-4] + '_hl_1.png') + '">'

            for button in actual_project['menus'][menu][1]:
                if actual_project['menus'][menu][2][button][4]:
                    final_dvd_author_xml += '<button name="' + button.replace(' ', '_') + '">jump '
                    jump_to = actual_project['menus'][menu][2][button][4]
                    if jump_to in actual_project['menus']:
                        final_dvd_author_xml += 'menu ' + str([*actual_project['menus']].index(jump_to) + 1) + ';'

                    elif not ' > ' in jump_to and jump_to in list_of_used_videos:
                        final_dvd_author_xml += 'title ' + str(list_of_used_videos.index(jump_to) + 1) + ';'

                    elif ' > ' in jump_to:
                        jump_to_video = jump_to.split(' > ')[0]
                        jump_to_mark = False
                        jumt_to_group = False
                        if '(' in jump_to.split(' > ')[1]:
                            jump_to_mark = jump_to.split(' > ')[1].split(' (')[0]
                        else:
                            jump_to_group = jump_to

                        if jump_to_mark:
                            if jump_to_video in list_of_used_videos and jump_to_mark in actual_project['videos'][jump_to_video][1]:
                                final_dvd_author_xml += 'title ' + str(list_of_used_videos.index(jump_to_video) + 1) + ' chapter ' + str([*actual_project['videos'][jump_to_video][1]].index(jump_to_mark) + 1) + ';'
                        elif jump_to_group:
                            final_dvd_author_xml += 'title ' + str(len(list_of_used_videos) + list_of_used_groups.index(jump_to_group) + 1) + ';'

                    final_dvd_author_xml += '</button>'

                    if aspect_ratio == '16:9':
                        factor_x = 1
                        if video_format == 'pal':
                            factor_y = 1.42222222
                        elif video_format == 'ntsc':
                            factor_y = 1.18518519

                        x = int(int(actual_project['menus'][menu][2][button][0]))
                        y = int(int(actual_project['menus'][menu][2][button][1]))
                        w = int(int(actual_project['menus'][menu][2][button][2]))
                        h = int(int(actual_project['menus'][menu][2][button][3]))

                        final_spumux_xml_0 += '<button name="' + button.replace(' ', '_') + '" x0="' + str(x) + '" y0="' + str(y) + '" x1="' + str( x + w ) + '" y1="' + str( y + h ) + '"'
                        final_spumux_xml_1 += '<button name="' + button.replace(' ', '_') + '" x0="' + str(x) + '" y0="' + str(int( ( y * .75 ) + ( height * .125 ) )) + '" x1="' + str( x + w ) + '" y1="' + str(int( ( y * .75 ) + ( height * .125 ) + ( h * .75 ))) + '"'

                    if aspect_ratio == '4:3':
                        factor_x = 1.125
                        if video_format == 'pal':
                            factor_y = 1.2
                        elif video_format == 'ntsc':
                            factor_y = 1

                        x = int(int(actual_project['menus'][menu][2][button][0]))
                        y = int(int(actual_project['menus'][menu][2][button][1]))
                        w = int(int(actual_project['menus'][menu][2][button][2]))
                        h = int(int(actual_project['menus'][menu][2][button][3]))

                        final_spumux_xml_0 += '<button name="' + button.replace(' ', '_') + '" x0="' + str(int( x )) + '" y0="' + str(int( y )) + '" x1="' + str(int( x + w )) + '" y1="' + str(int( y + h )) + '"'

                    if actual_project['menus'][menu][2][button][5][0]:
                        final_spumux_xml_0 += ' up="' + actual_project['menus'][menu][2][button][5][0].replace(' ', '_') + '"'
                        final_spumux_xml_1 += ' up="' + actual_project['menus'][menu][2][button][5][0].replace(' ', '_') + '"'
                    if actual_project['menus'][menu][2][button][5][1]:
                        final_spumux_xml_0 += ' right="' + actual_project['menus'][menu][2][button][5][1].replace(' ', '_') + '"'
                        final_spumux_xml_1 += ' right="' + actual_project['menus'][menu][2][button][5][1].replace(' ', '_') + '"'
                    if actual_project['menus'][menu][2][button][5][2]:
                        final_spumux_xml_0 += ' down="' + actual_project['menus'][menu][2][button][5][2].replace(' ', '_') + '"'
                        final_spumux_xml_1 += ' down="' + actual_project['menus'][menu][2][button][5][2].replace(' ', '_') + '"'
                    if actual_project['menus'][menu][2][button][5][3]:
                        final_spumux_xml_0 += ' left="' + actual_project['menus'][menu][2][button][5][3].replace(' ', '_') + '"'
                        final_spumux_xml_1 += ' left="' + actual_project['menus'][menu][2][button][5][3].replace(' ', '_') + '"'

                    final_spumux_xml_0 += ' />'
                    final_spumux_xml_1 += ' />'
            final_dvd_author_xml += '<post>'
            if actual_project['menus'][menu][7]:
                jump_to = actual_project['menus'][menu][7]
                if jump_to in actual_project['menus']:
                    final_dvd_author_xml += 'call menu ' + str([*actual_project['menus']].index(jump_to) + 1) + ';'
                elif not ' > ' in jump_to and jump_to in list_of_used_videos:
                    final_dvd_author_xml += 'jump title ' + str(list_of_used_videos.index(jump_to) + 1) + ';'
            else:
                final_dvd_author_xml += 'jump cell 1;'
            final_dvd_author_xml += '</post>'
            final_dvd_author_xml += '</pgc>'
            final_spumux_xml_0 += '</spu></stream></subpictures>'
            final_spumux_xml_1 += '</spu></stream></subpictures>'

            if actual_project['menus'][menu][3]:
                open(os.path.join(path_tmp, actual_project['menus'][menu][3].split('/')[-1].split('\\')[-1][:-4] + '_0.xml'), 'w').write(final_spumux_xml_0)
                open(os.path.join(path_tmp, actual_project['menus'][menu][3].split('/')[-1].split('\\')[-1][:-4] + '_1.xml'), 'w').write(final_spumux_xml_1)
                menu_mpg_file = open(os.path.join(path_tmp, menu + '.mpg'))
                menu_mpg_final_file = open(os.path.join(path_tmp, menu + '_final.mpg'), 'w')
                print(type(menu_mpg_file))
                print(type(menu_mpg_final_file))

                if aspect_ratio == '16:9':
                    s0 = subprocess.Popen([spumux_bin, '-s', '0', os.path.join(path_tmp, actual_project['menus'][menu][3].split('/')[-1].split('\\')[-1][:-4] + '_0.xml')], stdin=menu_mpg_file, stdout=subprocess.PIPE, stderr=subprocess.DEVNULL)
                    print(type(s0))
                    subprocess.Popen([spumux_bin, '-s', '1', os.path.join(path_tmp, actual_project['menus'][menu][3].split('/')[-1].split('\\')[-1][:-4] + '_1.xml')], stdin=s0.stdout, stdout=menu_mpg_final_file, stderr=subprocess.DEVNULL)
                    s0.stdout.close()
                else:
                    subprocess.run([spumux_bin, os.path.join(path_tmp, actual_project['menus'][menu][3].split('/')[-1].split('\\')[-1][:-4] + '_0.xml')], stdin=menu_mpg_file, stdout=menu_mpg_final_file, stderr=subprocess.DEVNULL)
                menu_mpg_final_file.close()

            else:
                shutil.move(os.path.join(path_tmp, menu + '.mpg'), os.path.join(path_tmp, menu + '_final.mpg'))

            menu_count += 1
            signal.sig.emit('PROCESSING MENU ' + menu.upper() + ',' + str(menu_count))

        final_dvd_author_xml += '</menus>'

    final_dvd_author_xml += '<titles>'

    signal.sig.emit('PROCESSING VIDEOS,' + str(menu_count + 1))

    video_count = menu_count + 1
    for video in actual_project['videos']:

        chapters_keyframes = ''
        for chapter in actual_project['videos'][video][1]:
            chapters_keyframes += actual_project['videos'][video][2][chapter] + ','
        if actual_project['videos'][video][4]:
            final_command = [ffmpeg_bin]
            final_command += '-y',
            final_command += '-i', actual_project['videos'][video][0]
            final_length = actual_project['videos'][video][5]
            if actual_project['videos'][video][6]:
                final_command += '-ss', str(actual_project['videos'][video][6])
                final_length -= actual_project['videos'][video][6]
            if actual_project['videos'][video][7]:
                final_length -= (actual_project['videos'][video][5] - actual_project['videos'][video][7])
            if actual_project['videos'][video][6] or actual_project['videos'][video][7]:
                final_command += '-t', str(final_length)
            if actual_project['video_twopass'] and actual_project['video_encoding'] == 'VBR':
                final_command += '-pass', '1'
            final_command += '-c:v', 'mpeg2video'
            final_command += '-c:a', audio_codec
            final_command += '-f', 'dvd'
            final_command += '-s', video_resolutions[actual_project['videos'][video][9]]
            final_command += '-r', framerate
            final_command += '-pix_fmt', 'yuv420p'
            final_command += '-g', str(actual_project['gop_size'])
            if len(actual_project['videos'][video][1]) > 0:
                final_command += '-force_key_frames', chapters_keyframes[:-1]
            final_command += '-b:v', str(actual_project['video_bitrate'] * 1000)
            if actual_project['video_encoding'] == 'CBR':
                final_command += '-maxrate', str(actual_project['video_bitrate'] * 1000)
                final_command += '-minrate', str(actual_project['video_bitrate'] * 1000)
            elif actual_project['video_encoding'] == 'VBR':
                final_command += '-maxrate', str(actual_project['video_max_bitrate'] * 1000)
                final_command += '-minrate', '0'
            final_command += '-bufsize', '1835008'
            final_command += '-packetsize', '2048'
            final_command += '-muxrate', '10080000'
            final_command += '-b:a', str(audio_datarate * 1000)
            final_command += '-ar', '48000'
            final_command += '-aspect', aspect_ratio
            if actual_project['video_twopass'] and actual_project['video_encoding'] == 'VBR':
                final_command += '-passlogfile', os.path.join(path_tmp, video + '.log')
            final_command += os.path.join(path_tmp, video + '.mpg'),
            subprocess.run(final_command, stdin=subprocess.DEVNULL, stdout=subprocess.DEVNULL, stderr=subprocess.DEVNULL)

            if actual_project['video_twopass'] and actual_project['video_encoding'] == 'VBR':
                final_command = [ffmpeg_bin]
                final_command += '-y',
                final_command += '-i', actual_project['videos'][video][0]
                final_length = actual_project['videos'][video][5]
                if actual_project['videos'][video][6]:
                    final_command += '-ss', str(actual_project['videos'][video][6])
                    final_length -= actual_project['videos'][video][6]
                if actual_project['videos'][video][7]:
                    final_length -= (actual_project['videos'][video][5] - actual_project['videos'][video][7])
                if actual_project['videos'][video][6] or actual_project['videos'][video][7]:
                    final_command += '-t', str(final_length)
                final_command += '-pass', '2'
                final_command += '-c:v', 'mpeg2video'
                final_command += '-c:a', audio_codec
                final_command += '-f', 'dvd'
                final_command += '-s', video_resolutions[actual_project['videos'][video][9]]
                final_command += '-r', framerate
                final_command += '-pix_fmt', 'yuv420p'
                final_command += '-g', str(actual_project['gop_size'])
                if len(actual_project['videos'][video][1]) > 0:
                    final_command += '-force_key_frames', chapters_keyframes[:-1]#'chapters'
                final_command += '-b:v', str(actual_project['video_bitrate'] * 1000)
                if actual_project['video_encoding'] == 'CBR':
                    final_command += '-maxrate', str(actual_project['video_bitrate'] * 1000)
                    final_command += '-minrate', str(actual_project['video_bitrate'] * 1000)
                elif actual_project['video_encoding'] == 'VBR':
                    final_command += '-maxrate', str(actual_project['video_max_bitrate'] * 1000)
                    final_command += '-minrate', '0'
                final_command += '-bufsize', '1835008'
                final_command += '-packetsize', '2048'
                final_command += '-muxrate', '10080000'
                final_command += '-b:a', str(audio_datarate * 1000)
                final_command += '-ar', '48000'
                final_command += '-aspect', aspect_ratio
                final_command += '-passlogfile', os.path.join(path_tmp, video + '.log')
                final_command += os.path.join(path_tmp, video + '.mpg'),
                subprocess.run(final_command, stdin=subprocess.DEVNULL, stdout=subprocess.DEVNULL, stderr=subprocess.DEVNULL)

            video_path = os.path.join(path_tmp, video + '.mpg')
        else:
            video_path = actual_project['videos'][video][0]

        if not actual_project['videos'][video][3] and video in list_of_used_videos:
            final_dvd_author_xml += '<pgc>'
            final_dvd_author_xml += '<vob file="' + video_path + '"'

            if len(actual_project['videos'][video][1]) > 0:
                final_dvd_author_xml += ' chapters="'
                final_dvd_author_xml += chapters_keyframes[:-1] + '"'
            final_dvd_author_xml += ' />'
            if actual_project['has_menus']:
                final_dvd_author_xml += '<post>'
                if actual_project['videos'][video][8]:
                    jump_to = actual_project['videos'][video][8]
                    if jump_to in actual_project['menus']:
                        final_dvd_author_xml += 'call menu ' + str([*actual_project['menus']].index(jump_to) + 1) + ';'
                    elif not ' > ' in jump_to and jump_to in list_of_used_videos:
                        final_dvd_author_xml += 'jump title ' + str(list_of_used_videos.index(jump_to) + 1) + ';'
                else:
                    final_dvd_author_xml += 'call menu;'
                final_dvd_author_xml += '</post>'
            final_dvd_author_xml += '</pgc>'

        video_count += 1
        signal.sig.emit('PROCESSING VIDEO ' + video.upper() + ',' + str(video_count))

    signal.sig.emit('PROCESSING GROUPS,' + str(video_count + 1))

    if actual_project['has_menus']:
        for group in list_of_used_groups:
            video = group.split(' > ')[0]

            final_dvd_author_xml += '<pgc>'

            if actual_project['videos'][video][4]:
                video_path = os.path.join(path_tmp, video + '.mpg')
            else:
                video_path = actual_project['videos'][video][0]

            final_dvd_author_xml += '<vob file="' + video_path + '">'

            temp_list_of_chapters = sort_list_of_chapters(actual_project['videos'][video][2])
            temp_dict_of_chapters = actual_project['videos'][video][2]

            if len(temp_list_of_chapters)%2 == 0:
                with subprocess.Popen([ffprobe_bin,  '-show_format', '-print_format', 'xml', actual_project['videos'][video][0]], stdin=subprocess.DEVNULL, stdout=subprocess.PIPE, stderr=subprocess.STDOUT).stdout.read().decode() as length_xml:
                    length = convert_to_timecode(length_xml.split(' duration="')[1].split('"')[0])
                    temp_dict_of_chapters['end'] = length
                    temp_list_of_chapters.append('end')

            for chapter in temp_list_of_chapters:
                if chapter.split(' ')[0] == group.split(' > ')[1]:
                    mark = temp_dict_of_chapters[chapter]
                    next_mark = temp_dict_of_chapters[temp_list_of_chapters[temp_list_of_chapters.index(chapter)+1]]
                    final_dvd_author_xml += '<cell start="' +  mark + '" end="' + next_mark + '" chapter="1"></cell>'

            final_dvd_author_xml += '</vob>'
            final_dvd_author_xml += '<post>call menu;</post>'
            final_dvd_author_xml += '</pgc>'

    final_dvd_author_xml += '</titles></titleset>'

    final_dvd_author_xml += '</dvdauthor>'

    open(os.path.join(path_tmp, 'dvd.xml'), 'w').write(final_dvd_author_xml)

    signal.sig.emit('PROCESSING DVD FOLDERS,' + str(video_count + 2))

    subprocess.run([dvdauthor_bin, '-x', os.path.join(path_tmp, 'dvd.xml')], stdin=subprocess.DEVNULL, stdout=subprocess.DEVNULL, stderr=subprocess.DEVNULL)

    signal.sig.emit('PROCESSING DVD ISO,' + str(video_count + 3))

    subprocess.run([mkisofs_bin, '-v', '-dvd-video', '-udf', '-V', actual_project['name'][:32], '-o',  os.path.join(path_tmp, 'movie.iso'),  os.path.join(path_tmp, 'dvd')], stdin=subprocess.DEVNULL, stdout=subprocess.DEVNULL, stderr=subprocess.DEVNULL)
    if os.path.isfile(os.path.join(path_tmp, 'movie.iso')):
        if generate_md5:
            signal.sig.emit('PROCESSING MD5,' + str(video_count + 4))

            md5 = ''
            with subprocess.Popen([md5_bin, os.path.join(path_tmp, 'movie.iso')], stdin=subprocess.DEVNULL, stdout=subprocess.PIPE, stderr=subprocess.DEVNULL).stdout.read() as md5_output:
                md5 += md5_output

            if md5:
                open(actual_project['file'].replace(actual_project['file'].split('/')[-1].split('\\')[-1], '') + actual_project['name'] + '.md5', 'w').write(md5)

        if generate_ddp:
            signal.sig.emit('PROCESSING DDP,' + str(video_count + 5))
            generate_ddp(os.path.join(path_tmp, 'movie.iso'), actual_project['file'].replace(actual_project['file'].split('/')[-1].split('\\')[-1], '') + 'ddp')

        if generate_iso:
            shutil.move(os.path.join(path_tmp, 'movie.iso'), os.path.join(actual_project['file'].replace(actual_project['file'].split('/')[-1].split('\\')[-1], ''), actual_project['name'] + '.iso'))
    else:
        None

    shutil.rmtree(os.path.join(path_tmp, 'dvd'), ignore_errors=True)
    signal.sig.emit('FINISH')
