#!/usr/bin/python3
# -*- coding: utf-8 -*-

from PyQt5.QtWidgets import QWidget, QLabel, QCheckBox
from PyQt5.QtCore import QPropertyAnimation, QEasingCurve, QRect

import os

def load(self, path_graphics):
    self.finalize_panel = QLabel(parent=self.main_panel)
    self.finalize_panel.setObjectName('finalize_panel_background')
    self.finalize_panel.setStyleSheet('#finalize_panel_background { border-top: 0; border-right: 0; border-bottom: 0; border-left: 19px; border-image: url("' + os.path.join(path_graphics, "finalize_panel_background.png").replace('\\', '/') + '") 0 0 0 19 stretch stretch; }')
    self.finalize_panel.setGeometry(self.main_panel.width(),0,500,100)
    self.finalize_panel.animation = QPropertyAnimation(self.finalize_panel, b'geometry')
    self.finalize_panel.animation.setEasingCurve(QEasingCurve.OutCirc)

    class finalize_panel_generate_button(QWidget):
        def enterEvent(widget, event):
            self.finalize_panel.generate_button_background.setStyleSheet("QLabel { padding-left:20px;  background-image: url(" + os.path.join(path_graphics, "finalize_panel_background_button_over.png").replace('\\', '/') + "); background-position: center left; }")
        def leaveEvent(widget, event):
            self.finalize_panel.generate_button_background.setStyleSheet("QLabel { padding-left:20px;  background-image: url(" + os.path.join(path_graphics, "finalize_panel_background_button_normal.png").replace('\\', '/') + "); background-position: center left; }")
        def mousePressEvent(widget, event):
            dvd_generate(self)

    self.finalize_panel.generate_button = finalize_panel_generate_button(parent=self.finalize_panel)
    self.finalize_panel.generate_button.setGeometry(0,14,235,60)

    self.finalize_panel.generate_button_background = QLabel('<font style="font-size:14px;color:white;" ><b>GENERATE<br>IMAGE</b></font>', parent=self.finalize_panel.generate_button)
    self.finalize_panel.generate_button_background.setGeometry(0,0,self.finalize_panel.generate_button.width(),self.finalize_panel.generate_button.height())
    self.finalize_panel.generate_button_background.setStyleSheet("QLabel { padding-left:20px; background-image: url(" + os.path.join(path_graphics, "finalize_panel_background_button_normal.png").replace('\\', '/') + "); background-position: center left; }")

    self.finalize_panel.generate_button_background_md5 = QLabel(parent=self.finalize_panel.generate_button)
    self.finalize_panel.generate_button_background_md5.setGeometry(0,0,self.finalize_panel.generate_button.width(),self.finalize_panel.generate_button.height())
    self.finalize_panel.generate_button_background_md5.setStyleSheet("QLabel { background-image: url(" + os.path.join(path_graphics, "finalize_panel_background_button_md5.png").replace('\\', '/') + "); background-position: center left; }")
    self.finalize_panel.generate_button_background_md5.setVisible(False)

    self.finalize_panel.generate_button_background_ddp = QLabel(parent=self.finalize_panel.generate_button)
    self.finalize_panel.generate_button_background_ddp.setGeometry(0,0,self.finalize_panel.generate_button.width(),self.finalize_panel.generate_button.height())
    self.finalize_panel.generate_button_background_ddp.setStyleSheet("QLabel { background-image: url(" + os.path.join(path_graphics, "finalize_panel_background_button_ddp.png").replace('\\', '/') + "); background-position: center left; }")
    self.finalize_panel.generate_button_background_ddp.setVisible(False)

    self.finalize_panel.generate_options = QLabel(parent=self.finalize_panel)
    self.finalize_panel.generate_options.setGeometry(240, 14, 250, 60)
    self.finalize_panel.generate_options.setStyleSheet("QLabel { background-image: url(" + os.path.join(path_graphics, "finalize_panel_options_background.png").replace('\\', '/') + "); background-position: center left; }")

    class finalize_panel_generate_options_toggle(QLabel):
        def enterEvent(widget, event):
            if self.finalize_panel.x() == self.main_panel.width() - 260:
                self.finalize_panel.generate_options_toggle.setStyleSheet("QLabel { background-image: url(" + os.path.join(path_graphics, "finalize_panel_background_button_toggle_left_over.png").replace('\\', '/') + "); background-position: center left; }")
            else:
                self.finalize_panel.generate_options_toggle.setStyleSheet("QLabel { background-image: url(" + os.path.join(path_graphics, "finalize_panel_background_button_toggle_right_over.png").replace('\\', '/') + "); background-position: center left; }")
        def leaveEvent(widget, event):
            if self.finalize_panel.x() == self.main_panel.width() - 260:
                self.finalize_panel.generate_options_toggle.setStyleSheet("QLabel { background-image: url(" + os.path.join(path_graphics, "finalize_panel_background_button_toggle_left.png").replace('\\', '/') + "); background-position: center left; }")
            else:
                self.finalize_panel.generate_options_toggle.setStyleSheet("QLabel { background-image: url(" + os.path.join(path_graphics, "finalize_panel_background_button_toggle_right.png").replace('\\', '/') + "); background-position: center left; }")
        def mousePressEvent(widget, event):
            if self.finalize_panel.x() == self.main_panel.width() - 260:
                self.generate_effect(self.finalize_panel.animation, 'geometry', 500, [self.finalize_panel.x(),self.finalize_panel.y(),self.finalize_panel.width(),self.finalize_panel.height()], [self.main_panel.width() - self.finalize_panel.width(),self.finalize_panel.y(),self.finalize_panel.width(),self.finalize_panel.height()])
            else:
                self.generate_effect(self.finalize_panel.animation, 'geometry', 500, [self.finalize_panel.x(),self.finalize_panel.y(),self.finalize_panel.width(),self.finalize_panel.height()], [self.main_panel.width() - 260,self.finalize_panel.y(),self.finalize_panel.width(),self.finalize_panel.height()])
                self.finalize_panel.generate_options_toggle.setStyleSheet("QLabel { background-image: url(" + os.path.join(path_graphics, "finalize_panel_background_button_toggle_right.png").replace('\\', '/') + "); background-position: center left; }")

    self.finalize_panel.generate_options_toggle = finalize_panel_generate_options_toggle(parent=self.finalize_panel.generate_options)
    self.finalize_panel.generate_options_toggle.setGeometry(0,0,20,self.finalize_panel.generate_options.height())
    self.finalize_panel.generate_options_toggle.setStyleSheet("QLabel { background-image: url(" + os.path.join(path_graphics, "finalize_panel_background_button_toggle_right.png").replace('\\', '/') + "); background-position: center left; }")

    self.finalize_panel.generate_button_md5_checkbox = QCheckBox('MD5', parent=self.finalize_panel.generate_options)
    self.finalize_panel.generate_button_md5_checkbox.clicked.connect(lambda:set_generate_dvd_kind(self))
    self.finalize_panel.generate_button_md5_checkbox.setStyleSheet('color:silver')
    self.finalize_panel.generate_button_md5_checkbox.setGeometry(40, 20, 50, 20)

    self.finalize_panel.generate_button_ddp_checkbox = QCheckBox('DDP', parent=self.finalize_panel.generate_options)
    self.finalize_panel.generate_button_ddp_checkbox.clicked.connect(lambda:set_generate_dvd_kind(self))
    self.finalize_panel.generate_button_ddp_checkbox.setStyleSheet('color:silver')
    self.finalize_panel.generate_button_ddp_checkbox.setGeometry(110, 20, 50, 20)

def set_generate_dvd_kind(self):
    if self.finalize_panel.generate_button_ddp_checkbox.isChecked():
        self.finalize_panel.generate_button_background_ddp.setVisible(True)
    else:
        self.finalize_panel.generate_button_background_ddp.setVisible(False)

    if self.finalize_panel.generate_button_md5_checkbox.isChecked():
        self.finalize_panel.generate_button_background_md5.setVisible(True)
    else:
        self.finalize_panel.generate_button_background_md5.setVisible(False)

def dvd_generate(self):
    self.preview_video_widget.setVisible(False)
    if not self.actual_project['file']:
        self.generate_dvd_thread_thread.actual_project_file = os.path.join(path_home, self.actual_project['name'] + '.odvdp')
    else:
        self.generate_dvd_thread_thread.actual_project_file = self.actual_project['file']
        self.generate_dvd_thread_thread.project_name = self.actual_project['name']

    if self.audio_formats[self.actual_project['audio_format']] == 'MP2 48kHz':
        self.generate_dvd_thread_thread.audio_codec = 'mp2'#'pcm_s16le'

    if self.video_formats[self.actual_project['video_format']].split(' ')[0] == 'NTSC':
        self.generate_dvd_thread_thread.framerate = '29.97'

    self.generate_dvd_thread_thread.height = int(self.video_formats[self.actual_project['video_format']].split(' ')[1].split('x')[1])
    self.generate_dvd_thread_thread.video_format = self.video_formats[self.actual_project['video_format']].split(' ')[0].lower()
    self.generate_dvd_thread_thread.video_resolutions = self.resolutions#self.video_formats[self.actual_project['video_format']].split(' ')[1].lower()

    self.generate_dvd_thread_thread.aspect_ratio = self.aspect_ratios[self.actual_project['aspect_ratio']]

    self.generate_dvd_thread_thread.selected_menu_encoding = self.actual_project['menu_encoding']
    self.generate_dvd_thread_thread.selected_video_encoding = self.actual_project['video_encoding']

    self.generate_dvd_thread_thread.selected_menu_twopass = self.actual_project['menu_twopass']
    self.generate_dvd_thread_thread.selected_video_twopass = self.actual_project['video_twopass']

    self.generate_dvd_thread_thread.selected_gop_size = self.actual_project['gop_size']
    self.generate_dvd_thread_thread.selected_menu_bitrate = self.actual_project['menu_bitrate']
    self.generate_dvd_thread_thread.selected_menu_max_bitrate = self.actual_project['menu_max_bitrate']
    self.generate_dvd_thread_thread.selected_video_bitrate = self.actual_project['video_bitrate']
    self.generate_dvd_thread_thread.selected_video_max_bitrate = self.actual_project['video_max_bitrate']

    self.generate_dvd_thread_thread.list_of_menus = self.actual_project['menus']
    self.generate_dvd_thread_thread.list_of_videos = self.actual_project['videos']
    self.generate_dvd_thread_thread.dict_of_menus = self.actual_project['menus']
    self.generate_dvd_thread_thread.dict_of_videos = self.actual_project['videos']

    self.generate_dvd_thread_thread.has_menus = self.actual_project['has_menus']

    self.generate_dvd_thread_thread.audio_datarate = int(self.actual_project['audio_datarate'].split(' ')[0])

    self.generate_dvd_thread_thread.generate_md5 = self.finalize_panel.generate_button_md5_checkbox.isChecked()
    self.generate_dvd_thread_thread.generate_ddp = self.finalize_panel.generate_button_ddp_checkbox.isChecked()
    self.generate_dvd_thread_thread.actual_project = self.actual_project
    self.generate_dvd_thread_thread.start()
