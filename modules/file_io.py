#!/usr/bin/python3
# -*- coding: utf-8 -*-

import os, codecs
from collections import OrderedDict

def check_if_is_float(value):
  try:
    float(value)
    return float(value)
  except ValueError:
    return False

def get_absolute_path(actual_project, path):
    return os.path.normpath(os.path.join(actual_project['file'].replace(actual_project['file'].split('/')[-1].split('\\')[-1], ''), path))

def get_relative_path(actual_project, path):
    return os.path.relpath(path, actual_project['file'].replace(actual_project['file'].split('/')[-1].split('\\')[-1], ''))

def write_project_file(actual_project):
    final_project_file = u'<?xml version="1.0" encoding="UTF-8"?>'
    final_project_file += '<dvd name="' + actual_project['name'] + '"'
    final_project_file += ' aspect_ratio="' + str(actual_project['aspect_ratio'])  + '"'
    final_project_file += ' video_format="' + str(actual_project['video_format'])  + '"'
    final_project_file += ' audio_format="' + str(actual_project['audio_format'])  + '"'
    final_project_file += ' video_encoding="' + str(actual_project['video_encoding'])  + '"'
    final_project_file += ' video_bitrate="' + str(actual_project['video_bitrate'])  + '"'
    final_project_file += ' video_max_bitrate="' + str(actual_project['video_max_bitrate'])  + '"'
    final_project_file += ' menu_encoding="' + str(actual_project['menu_encoding']) + '"'
    final_project_file += ' menu_twopass="' + str(actual_project['menu_twopass']) + '"'
    final_project_file += ' video_encoding="' + str(actual_project['video_encoding']) + '"'
    final_project_file += ' video_twopass="' + str(actual_project['video_twopass']) + '"'
    final_project_file += ' menu_bitrate="' + str(actual_project['menu_bitrate']) + '"'
    final_project_file += ' menu_max_bitrate="' + str(actual_project['menu_max_bitrate']) + '"'
    final_project_file += ' audio_datarate="' + actual_project['audio_datarate'] + '"'
    final_project_file += ' has_menus="' + str(actual_project['has_menus']) + '"'
    final_project_file += ' gop_size="' + str(actual_project['gop_size']) + '">'

    final_project_file += '<menus>'
    for menu in actual_project['menus']:
        final_project_file += '<menu name="' + menu + '" filepath="' + get_relative_path(actual_project, actual_project['menus'][menu][0]) + '"'
        if actual_project['menus'][menu][3]:
            final_project_file += ' overlay="' + get_relative_path(actual_project, actual_project['menus'][menu][3]) + '"'
        if actual_project['menus'][menu][4]:
            final_project_file += ' overlay_color="' + actual_project['menus'][menu][4] + '"'
        if actual_project['menus'][menu][5]:
            final_project_file += ' sound="' + get_relative_path(actual_project, actual_project['menus'][menu][5]) + '"'
        final_project_file += ' main_menu="' + str(actual_project['menus'][menu][6]) + '"'
        final_project_file += ' post="' + str(actual_project['menus'][menu][7]) + '"'
        final_project_file += ' transparency="' + str(actual_project['menus'][menu][8]) + '"'
        final_project_file += ' border="' + str(actual_project['menus'][menu][9]) + '"'
        final_project_file += ' length="' + str(actual_project['menus'][menu][10]) + '"'
        final_project_file += '>'
        for button in actual_project['menus'][menu][1]:
            final_project_file += '<button name="' + button + '" x="' + str(actual_project['menus'][menu][2][button][0]) + '" y="' + str(actual_project['menus'][menu][2][button][1]) + '" width="' + str(actual_project['menus'][menu][2][button][2]) + '" height="' + str(actual_project['menus'][menu][2][button][3]) + '" jump_to="' + str(actual_project['menus'][menu][2][button][4])+ '" direction_top="' + str(actual_project['menus'][menu][2][button][5][0]) + '" direction_right="' + str(actual_project['menus'][menu][2][button][5][1]) + '" direction_bottom="' + str(actual_project['menus'][menu][2][button][5][2]) + '" direction_left="' + str(actual_project['menus'][menu][2][button][5][3]) + '"></button>'
        final_project_file += '</menu>'
    final_project_file += '</menus>'
    final_project_file += '<videos>'
    for video in actual_project['videos']:
        final_project_file += '<video name="' + video + '"'
        final_project_file += ' filepath="' + get_relative_path(actual_project, actual_project['videos'][video][0]) + '"'
        final_project_file += ' reencode="' + str(actual_project['videos'][video][4]) + '"'
        final_project_file += ' length="' + str(actual_project['videos'][video][5]) + '"'
        final_project_file += ' start="' + str(actual_project['videos'][video][6]) + '"'
        final_project_file += ' end="' + str(actual_project['videos'][video][7]) + '"'
        final_project_file += ' is_intro="' + str(actual_project['videos'][video][3]) + '"'
        final_project_file += ' post="' + str(actual_project['videos'][video][8]) + '"'
        final_project_file += ' resolution="' + str(actual_project['videos'][video][9]) + '">'
        for chapter in actual_project['videos'][video][1]:
            final_project_file += '<chapter name="' + str(chapter) + '"'
            final_project_file += ' time="' + actual_project['videos'][video][2][chapter] + '">'
            final_project_file += '</chapter>'
        final_project_file += '</video>'
    final_project_file += '</videos>'
    final_project_file += '</dvd>'

    return final_project_file

def read_project_file(filepath):
    project_file_content = codecs.open(filepath, 'r', 'utf-8').read()
    actual_project = {}

    actual_project['file'] = filepath

    actual_project['menus'] = OrderedDict()

    actual_project['videos'] = OrderedDict()

    actual_project['menu_encoding'] = 'CBR'
    actual_project['video_encoding'] = 'CBR'
    actual_project['menu_twopass'] = False
    actual_project['video_twopass'] = False
    actual_project['has_menus'] = True

    actual_project['name'] = project_file_content.split('<dvd ')[1].split('</dvd>')[0].split('name="')[1].split('"')[0]
    actual_project['aspect_ratio'] = int(project_file_content.split('<dvd ')[1].split('</dvd>')[0].split('aspect_ratio="')[1].split('"')[0])
    actual_project['video_format'] = int(project_file_content.split('<dvd ')[1].split('</dvd>')[0].split('video_format="')[1].split('"')[0])
    actual_project['audio_format'] = int(project_file_content.split('<dvd ')[1].split('</dvd>')[0].split('audio_format="')[1].split('"')[0])
    if 'video_bitrate="' in project_file_content.split('<dvd ')[1].split('</dvd>')[0]:
        actual_project['video_bitrate'] = int(project_file_content.split('<dvd ')[1].split('</dvd>')[0].split('video_bitrate="')[1].split('"')[0])
    if 'video_max_bitrate="' in project_file_content.split('<dvd ')[1].split('</dvd>')[0]:
        actual_project['video_max_bitrate'] = int(project_file_content.split('<dvd ')[1].split('</dvd>')[0].split('video_max_bitrate="')[1].split('"')[0])
    if 'menu_encoding="' in project_file_content.split('<dvd ')[1].split('</dvd>')[0]:
        actual_project['menu_encoding'] = project_file_content.split('<dvd ')[1].split('</dvd>')[0].split('menu_encoding="')[1].split('"')[0]
    if 'menu_twopass="' in project_file_content.split('<dvd ')[1].split('</dvd>')[0]:
        if project_file_content.split('<dvd ')[1].split('</dvd>')[0].split('menu_twopass="')[1].split('"')[0] == 'True':
            actual_project['menu_twopass'] = True
    if 'video_encoding="' in project_file_content.split('<dvd ')[1].split('</dvd>')[0]:
        actual_project['video_encoding'] = project_file_content.split('<dvd ')[1].split('</dvd>')[0].split('video_encoding="')[1].split('"')[0]
    if 'video_twopass="' in project_file_content.split('<dvd ')[1].split('</dvd>')[0]:
        if project_file_content.split('<dvd ')[1].split('</dvd>')[0].split('video_twopass="')[1].split('"')[0] == 'True':
            actual_project['video_twopass'] = True
    if 'menu_bitrate="' in project_file_content.split('<dvd ')[1].split('</dvd>')[0]:
        actual_project['menu_bitrate'] = int(project_file_content.split('<dvd ')[1].split('</dvd>')[0].split('menu_bitrate="')[1].split('"')[0])
    if 'menu_max_bitrate="' in project_file_content.split('<dvd ')[1].split('</dvd>')[0]:
        actual_project['menu_max_bitrate'] = int(project_file_content.split('<dvd ')[1].split('</dvd>')[0].split('menu_max_bitrate="')[1].split('"')[0])
    if 'gop_size="' in project_file_content.split('<dvd ')[1].split('</dvd>')[0]:
        actual_project['gop_size'] = int(project_file_content.split('<dvd ')[1].split('</dvd>')[0].split('gop_size="')[1].split('"')[0])
    if 'audio_datarate="' in project_file_content.split('<dvd ')[1].split('</dvd>')[0]:
        actual_project['audio_datarate'] = project_file_content.split('<dvd ')[1].split('</dvd>')[0].split('audio_datarate="')[1].split('"')[0]
    if 'has_menus="' in project_file_content.split('<dvd ')[1].split('</dvd>')[0]:
        if project_file_content.split('<dvd ')[1].split('</dvd>')[0].split('has_menus="')[1].split('"')[0] == 'False':
            self.has_menus = False

    if '<menu ' in project_file_content.split('<menus>')[1].split('</menus>')[0]:
        for menu_item in project_file_content.split('<menus>')[1].split('</menus>')[0].split('<menu '):
            if '</menu>' in menu_item:
                menu_name = menu_item.split('>')[0].split('name="')[1].split('"')[0]
                menu_list_for_dict = []
                menu_list_for_dict.append(get_absolute_path(actual_project, menu_item.split('>')[0].split('filepath="')[1].split('"')[0]))
                list_of_buttons = []
                dict_of_buttons = {}
                if '<button ' in menu_item.split('</menu>')[0]:
                    for button_item in menu_item.split('</menu>')[0].split('<button '):
                        if '</button>' in button_item:
                            button_name = button_item.split('name="')[1].split('"')[0]
                            list_of_buttons.append(button_name)
                            button_list_for_dict = []
                            button_list_for_dict.append(float(button_item.split('x="')[1].split('"')[0]))
                            button_list_for_dict.append(float(button_item.split('y="')[1].split('"')[0]))
                            button_list_for_dict.append(float(button_item.split('width="')[1].split('"')[0]))
                            button_list_for_dict.append(float(button_item.split('height="')[1].split('"')[0]))
                            if button_item.split('jump_to="')[1].split('"')[0] == 'None':
                                button_list_for_dict.append(None)
                            else:
                                button_list_for_dict.append(button_item.split('jump_to="')[1].split('"')[0])
                            direction_list = []
                            if 'direction_top="' in button_item and not button_item.split('direction_top="')[1].split('"')[0] == 'None':
                                direction_list.append(button_item.split('direction_top="')[1].split('"')[0])
                            else:
                                direction_list.append(None)
                            if 'direction_right="' in button_item and not button_item.split('direction_right="')[1].split('"')[0] == 'None':
                                direction_list.append(button_item.split('direction_right="')[1].split('"')[0])
                            else:
                                direction_list.append(None)
                            if 'direction_bottom="' in button_item and not button_item.split('direction_bottom="')[1].split('"')[0] == 'None':
                                direction_list.append(button_item.split('direction_bottom="')[1].split('"')[0])
                            else:
                                direction_list.append(None)
                            if 'direction_left="' in button_item and not button_item.split('direction_left="')[1].split('"')[0] == 'None':
                                direction_list.append(button_item.split('direction_left="')[1].split('"')[0])
                            else:
                                direction_list.append(None)
                            button_list_for_dict.append(direction_list)
                            dict_of_buttons[button_name] = button_list_for_dict
                menu_list_for_dict.append(list_of_buttons)
                menu_list_for_dict.append(dict_of_buttons)
                if 'overlay="' in menu_item.split('>')[0]:
                    menu_list_for_dict.append(get_absolute_path(actual_project, menu_item.split('>')[0].split('overlay="')[1].split('"')[0]))
                else:
                    menu_list_for_dict.append(None)

                if 'overlay_color="' in menu_item.split('>')[0]:
                    menu_list_for_dict.append(menu_item.split('>')[0].split('overlay_color="')[1].split('"')[0])
                else:
                    menu_list_for_dict.append(None)

                if 'sound="' in menu_item.split('>')[0]:
                    menu_list_for_dict.append(get_absolute_path(actual_project, menu_item.split('>')[0].split('sound="')[1].split('"')[0]))
                else:
                    menu_list_for_dict.append(None)

                if 'main_menu="' in menu_item.split('>')[0] and menu_item.split('>')[0].split('main_menu="')[1].split('"')[0] == 'True':
                    menu_list_for_dict.append(True)
                else:
                    menu_list_for_dict.append(False)

                if 'post="' in menu_item.split('>')[0] and not menu_item.split('>')[0].split('post="')[1].split('"')[0] == 'False':
                    menu_list_for_dict.append(menu_item.split('>')[0].split('post="')[1].split('"')[0])
                else:
                    menu_list_for_dict.append(False)

                if 'transparency="' in menu_item.split('>')[0]:
                    menu_list_for_dict.append(float(menu_item.split('>')[0].split('transparency="')[1].split('"')[0]))
                else:
                    menu_list_for_dict.append(.5)

                if 'border="' in menu_item.split('>')[0]:
                    menu_list_for_dict.append(float(menu_item.split('>')[0].split('border="')[1].split('"')[0]))
                else:
                    menu_list_for_dict.append(.5)

                if 'length="' in menu_item.split('>')[0]:
                    menu_list_for_dict.append(float(menu_item.split('>')[0].split('length="')[1].split('"')[0]))
                else:
                    menu_list_for_dict.append(60.0)

                actual_project['menus'][menu_name] = menu_list_for_dict

    if '<video ' in project_file_content.split('<videos>')[1].split('</videos>')[0]:
        for video_item in project_file_content.split('<videos>')[1].split('</videos>')[0].split('<video'):
            if '</video>' in video_item:
                video_name = video_item.split(' name="')[1].split('"')[0]
                video_path = get_absolute_path(actual_project, video_item.split(' filepath="')[1].split('"')[0])
                list_of_chapters = []
                dict_of_chapters = {}
                is_intro = False
                reencode = False
                start = False
                end = False
                post = None
                resolution = 0
                if 'is_intro=' in video_item:
                    if video_item.split(' is_intro="')[1].split('"')[0] == 'True':
                        is_intro = True
                if 'reencode=' in video_item:
                    if video_item.split(' reencode="')[1].split('"')[0] == 'True':
                        reencode = True
                if 'post=' in video_item:
                    if not video_item.split(' post="')[1].split('"')[0] == 'None':
                        post = video_item.split(' post="')[1].split('"')[0]
                if 'resolution=' in video_item:
                    resolution = int(video_item.split(' resolution="')[1].split('"')[0])
                if 'length=' in video_item:
                    length = float(video_item.split(' length="')[1].split('"')[0])
                else:
                    if os.path.isfile(video_path):
                        length_xml = unicode(subprocess.Popen([ffprobe_bin,'-loglevel', 'error',  '-show_format', '-print_format', 'xml', video_path], startupinfo=startupinfo, stdin=subprocess.PIPE, stdout=subprocess.PIPE, stderr=subprocess.PIPE).stdout.read(), 'utf-8')
                        length = float(length_xml.split(' duration="')[1].split('"')[0])
                    else:
                        length = 60.0
                if 'start=' in video_item and check_if_is_float(video_item.split(' start="')[1].split('"')[0]):
                    start = check_if_is_float(video_item.split(' start="')[1].split('"')[0])
                if 'end=' in video_item  and check_if_is_float(video_item.split(' end="')[1].split('"')[0]):
                    end = check_if_is_float(video_item.split(' end="')[1].split('"')[0])
                if '<chapter' in video_item:
                    for chapter_item in video_item.split('<chapter'):
                        if '</chapter>' in chapter_item:
                            chapter_name = chapter_item.split(' name="')[1].split('"')[0]
                            chapter_time = chapter_item.split(' time="')[1].split('"')[0]
                            list_of_chapters.append(chapter_name)
                            dict_of_chapters[chapter_name] = chapter_time
                actual_project['videos'][video_name] = [video_path, list_of_chapters, dict_of_chapters, is_intro, reencode, length, start, end, post, resolution]
    return actual_project
